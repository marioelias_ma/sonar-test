!#/bin/sh

java \
-Djava.security.egd=file:/dev/./urandom \
-Djavax.net.ssl.trustStore=${trustoreFile} \
-Djavax.net.ssl.trustStorePassword=${trustorePwd} \
-Djavax.net.ssl.keyStore=${keystoreFile} \
-Djavax.net.ssl.keyStorePassword=${keystorePwd} \
-jar \
app.jar
