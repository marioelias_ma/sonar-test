

# INFORMACIÓN GENERAL
 
- **Servicio:**          	    Cliente
- **Producto:**          		Canales  
- **Equipo manteiner:** 	BGx - Business Capabilites 
                                                                        
# IMPORTANTE
 Este programa fue diseñado por Banco General, S.A., y esta protegido   
 por los derechos de propiedad intelectual.  Su uso no autorizado queda 
 expresamente prohibido de acuerdo a la legislacion vigente, asi como   
 obtener una copia (total o parcial) para fines ajenos al banco, sin el 
 consentimiento de la Vice Presidencia Ejecutiva del Banco General, S.A.
                                                                        
# PROPOSITO
Provee informacion de cliente de canales                                               

# NOTA ACLARATORIA
 ESTE PROGRAMA ES UTILIZADO POR << SERVICIOS SOA >>.                    
                                                                        
 CUALQUIER MODIFICACION AL COMPONENTE DEBE VERIFICARSE CON EL CENTRO    
 DE GOBIERNO SOA PARA EVALUAR EL IMPACTO EN EL FUNCIONAMIENTO DE LOS    
 SERVICIOS DE NEGOCIO.                                                  
## Diagramas 
```mermaid
graph TD
A(Canales Electronicos) --> B[Datapower - Dominio de APIs]
B --> C(API - Cliente)
C --> D[Datapower - Dominio de IIB]
D --> E(Bus de Integracion)
E --> F(JBoss - Conectores Genericos)
F --> G((COBIS))
F --> H((ENTRUST))
```