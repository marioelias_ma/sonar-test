package com.bgeneral.ce.bc.cliente.features.ports;

import javax.xml.datatype.DatatypeConfigurationException;

import com.bgeneral.ce.bc.cliente.features.dto.cliente.ClienteCanalResp;
import com.bgeneral.ce.bc.cliente.features.dto.cliente.ClienteCanalRq;
import com.bgeneral.ce.bc.clientecobis.common.dto.BCRequest;
import com.bgeneral.ce.bc.clientecobis.common.dto.BCResponse;



/**
 * @author Troby Wen
 *
 */

public interface IAmClienteAdapter {
	/**
	 * @param request
	 * @return
	 */

	BCResponse<ClienteCanalResp> consultarClienteCanales(BCRequest<ClienteCanalRq> request) throws DatatypeConfigurationException;


}
