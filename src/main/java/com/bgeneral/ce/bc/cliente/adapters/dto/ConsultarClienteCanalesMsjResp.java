package com.bgeneral.ce.bc.cliente.adapters.dto;

import java.io.Serializable;
import lombok.Getter;
import lombok.Setter;

/**
 * @author Troby Wen
 *
 */
@Getter
@Setter

public class ConsultarClienteCanalesMsjResp implements Serializable{

	private static final long serialVersionUID = 1L;
	private CabeceraResp cabeceraResp;
	
}
