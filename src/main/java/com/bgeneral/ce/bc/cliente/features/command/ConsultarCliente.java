package com.bgeneral.ce.bc.cliente.features.command;

import java.io.Serializable;

import com.bgeneral.ce.bc.cliente.features.dto.cliente.ClienteCanalResp;
import com.bgeneral.ce.bc.cliente.features.dto.cliente.ClienteCanalRq;
import com.bgeneral.ce.bc.clientecobis.common.dto.BCRequest;
import com.bgeneral.ce.bc.clientecobis.controller.interfaces.IAmCommand;

import lombok.Getter;

@Getter
public class ConsultarCliente implements IAmCommand<ClienteCanalRq, ClienteCanalResp>, Serializable{
	
	/**
	 * 
	 */
	private static final long serialVersionUID = -1382679069997318140L;
	private final BCRequest<ClienteCanalRq> dto;

	/**
	 * @param request
	 */
	public ConsultarCliente(BCRequest<ClienteCanalRq> request) {
		this.dto = request;
	}

}
