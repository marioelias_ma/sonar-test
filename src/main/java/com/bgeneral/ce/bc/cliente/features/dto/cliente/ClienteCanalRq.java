package com.bgeneral.ce.bc.cliente.features.dto.cliente;

import java.io.Serializable;

import com.fasterxml.jackson.annotation.JsonProperty;

import lombok.Getter;
import lombok.Setter;

/**
 * @author Troby Wen
 *
 */
@Getter
@Setter
public class ClienteCanalRq implements Serializable{

	/**
	 * 
	 */
	private static final long serialVersionUID = 485068340012505184L;
	@JsonProperty("cedula")
    private String cedula;
	
}
