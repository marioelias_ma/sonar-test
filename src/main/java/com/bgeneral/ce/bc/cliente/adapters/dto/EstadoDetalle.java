package com.bgeneral.ce.bc.cliente.adapters.dto;

import java.io.Serializable;

import com.fasterxml.jackson.annotation.JsonProperty;

import lombok.*;

/**
 * @author alugo
 *
 */
@Getter
@Setter


public class EstadoDetalle implements Serializable{
	private static final long serialVersionUID = 1L;
	
	@JsonProperty("codigo")
	private String codigo;
	
	@JsonProperty("descripcion")
	private String descripcion;
}
