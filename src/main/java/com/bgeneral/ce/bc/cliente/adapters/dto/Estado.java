package com.bgeneral.ce.bc.cliente.adapters.dto;

import java.io.Serializable;

import com.fasterxml.jackson.annotation.JsonProperty;

import lombok.*;

/**
 * @author alugo
 *
 */
@Getter
@Setter


public class Estado implements Serializable{
	private static final long serialVersionUID = 1L;
	
	@JsonProperty("codigo")
	private String codigo;
	
	@JsonProperty("descripcion")
	private String descripcion;
	
	@JsonProperty("nombre")
	private String nombre;
	
	@JsonProperty("severidad")
	private String severidad;
	
	@JsonProperty("tipoEstado")
	private String tipoEstado;
	
	@JsonProperty("codigoRegistro")
	private String codigoRegistro;
	
	private EstadosDetalle estadosDetalle;
}
