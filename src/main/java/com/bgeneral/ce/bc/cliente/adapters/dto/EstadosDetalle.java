package com.bgeneral.ce.bc.cliente.adapters.dto;

import java.io.Serializable;
import java.util.List;

import lombok.*;

/**
 * @author alugo
 *
 */
@Getter
@Setter

public class EstadosDetalle implements Serializable{
	private static final long serialVersionUID = 1L;
    private List <EstadoDetalle> estadoDetalle;
}
