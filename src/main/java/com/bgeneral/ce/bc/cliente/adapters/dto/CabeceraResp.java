package com.bgeneral.ce.bc.cliente.adapters.dto;

import java.io.Serializable;

import lombok.Getter;
import lombok.Setter;

/**
 * @author alugo
 *
 */
@Getter
@Setter

public class CabeceraResp implements Serializable{
	private static final long serialVersionUID = 1L;
	private EstadoGeneral estadoGeneral;
	private EstadosPorRegistro estadosPorRegistro;
       
}
