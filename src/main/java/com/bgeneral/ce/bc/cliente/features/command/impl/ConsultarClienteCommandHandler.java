/**
 * @(#)ConectorUtils.java 1.0.0 
 *
 * Copyright 2004 Banco General.
 * SUN PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 * Este archivo fue disenado por Banco General, S.A., y esta protegido
 * por los derechos de propiedad intelectual. Su uso no autorizado queda
 * expresamente prohibido de acuerdo a la legislacion vigente, asi como
 * obtener una copia (total o parcial) para fines ajenos al banco, sin el
 * consentimiento de la Vice Presidencia Ejecutiva del Banco General, S.A.
 *
 ******************************* PROPOSITO ******************************
 * Handler con los metodos de integracion con los adapters
 ***************************** MODIFICACIONES ****************************
 *
 * Fecha           Autor                        Razon
 * 03/10/2018	   Francisco Nieves			Emision Inicial
 *
 **************************************************************************
 * ***** NOTA ACLARATORIA *****
 **************************************************************************
 * ESTE PROGRAMA ES UTILIZADO POR << BUSSINES CAPABILITY >>.
 *
 * CUALQUIER MODIFICACION AL COMPONENTE DEBE VERIFICARSE CON EL CENTRO
 * DE GOBIERNO SOA PARA EVALUAR EL IMPACTO EN EL FUNCIONAMIENTO DE LOS
 * COMPONENTES.
 **************************************************************************
 */
package com.bgeneral.ce.bc.cliente.features.command.impl;

import com.bgeneral.ce.bc.cliente.features.command.ConsultarCliente;
import com.bgeneral.ce.bc.cliente.features.dto.cliente.ClienteCanalResp;
import com.bgeneral.ce.bc.cliente.features.dto.cliente.ClienteCanalRq;
import com.bgeneral.ce.bc.cliente.features.ports.IAmClienteAdapter;
import com.bgeneral.ce.bc.clientecobis.common.dto.BCResponse;
import com.bgeneral.ce.bc.clientecobis.common.dto.ReturnStatus;
import com.bgeneral.ce.bc.clientecobis.common.dto.Status;
import com.bgeneral.ce.bc.clientecobis.controller.interfaces.IAmCommandHandler;
import com.bgeneral.ce.bc.util.UtilBG;

/**
 * Clase de implementacion de metodos de integracion y funciones tecnicas de las operaciones del dominio Cliente
 * @author Francisco Nieves, Troby Weng, Andrea Lugo
 */
public class ConsultarClienteCommandHandler
		implements IAmCommandHandler<ConsultarCliente, ClienteCanalRq, ClienteCanalResp> {

	/**
	 * 
	 */
	private IAmClienteAdapter restAdapter;

	/**
	 * Constructor
	 * @param restAdapter <IAmClienteAdapter>
	 */
	public ConsultarClienteCommandHandler(IAmClienteAdapter restAdapter) {
		this.restAdapter = restAdapter;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see com.bgeneral.ce.bc.cliente.command.handler.IAmCommandHandler#handle(com.
	 * bgeneral.ce.bc.cliente.command.handler.IAmCommand)
	 */
	@Override
	public BCResponse<ClienteCanalResp> handle(ConsultarCliente command){
		BCResponse<ClienteCanalResp> bcResponse = new BCResponse<>();
		try {
			command.getDto().getBody().setCedula(FormatDocumentIDLegacy(command.getDto().getBody().getCedula()));
			bcResponse = restAdapter.consultarClienteCanales(command.getDto());
		} catch (Exception e) {
			bcResponse = new BCResponse<>();
			bcResponse.setHeader(command.getDto().getHeader());
			Status status = new Status();
			ReturnStatus returnStatus = new ReturnStatus();
			returnStatus.setReturnCode("U0002");
			returnStatus.setReturnCodeDesc(e.getMessage());
			status.setReturnStatus(returnStatus);
			bcResponse.setStatus(status);
		}
		return bcResponse;
	}

	/**
	 * Metodo que formatea la estructura de documentos de identidad hacia formato
	 * legacy de cobis
	 * 
	 * @param idClient 
	 *                 <String> cadena de la cedula con un formato similar
	 *                 8V-456-6547
	 * @return <String> con el formato
	 */
	private static String FormatDocumentIDLegacy(String idClient) {
		String[] arrayId = idClient.split("-");

		arrayId[0] = formatInicialProvinciaStatus(arrayId[0]);
		arrayId[1] = String.format("%04d", Integer.parseInt(arrayId[1]));
		arrayId[2] = String.format("%05d", Integer.parseInt(arrayId[2]));
		return String.format("%4s%4s%5s", arrayId[0], arrayId[1], arrayId[2]);
	}

	/**
	 * Metodo que formatea la cabecera de los documentos de identidad con el formato
	 * legacy de cobis
	 * 
	 * @param catProvincia 
	 * 						<String> Primera cadena o array[0] de un split de la cedula 
	 * 						en el formato 8V-456-6547
	 * @return <String> con el formato
	 */
	private static String formatInicialProvinciaStatus(String catProvincia) {

		/*
		 * Si posee menos de 4 caracteres debe de validar su estructura si posee
		 * combinacion de 3 entre numero y caracter en caso contrario se envia como
		 * llega en el split
		 */
		if (catProvincia.length() < 4) {
			/*
			 * se valida si en la cadena posee al menos un caracter
			 */
			if (UtilBG.isNan(catProvincia.toString())) {
				/*
				 * Siendo el caso se valida si el primero es numero o string Si es solo string,*
				 * entoces puede venir 1 o 2 caracteres y si es uno se completa con espacios del
				 * lado derecho
				 */
				if (UtilBG.isNan(Character.toString(catProvincia.charAt(0)))) {
					catProvincia = String.format("00%-2s", catProvincia);
				} else {
					/*
					 * En caso contrario se valida su longitud por si presenta el caso de: 2
					 * numericos un caracter 1 numerico 2 carateres
					 */
					if (catProvincia.length() == 3) {
						/*
						 * Se valida si el segundo es letra o numero Si es caracter entonces se agrega
						 * un 0 al principio ya que siendo de longitud 3, su primer caracter es numerico
						 * y el segundo es String, por deduccion el 3ro es caracter tambien
						 */
						if (UtilBG.isNan(Character.toString(catProvincia.charAt(1)))) {
							catProvincia = String.format("0%2s", catProvincia);
						} else {
							/*
							 * Caso contrario es que los 2 primeros son numeros y el ultimo es caracter,
							 * entonces se completa con espacios por la derecha.
							 */
							catProvincia = String.format("%2s ", catProvincia);
						}
					} else {
						/*
						 * sino tiene longitud de 3, es porque solo llego 1 caracter y 1 numero
						 */
						catProvincia = String.format("0%2s ", catProvincia);
					}
				}
			} else {
				/*
				 * En este caso, de que no tenga caracteres es porque solo valores numerico ya
				 * sea de 1 o 2 digitos
				 */
				catProvincia = String.format("%02d  ", Integer.parseInt(catProvincia));
			}
		}
		return catProvincia;
	}
}
