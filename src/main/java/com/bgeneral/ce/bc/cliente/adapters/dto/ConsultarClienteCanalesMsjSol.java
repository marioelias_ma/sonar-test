package com.bgeneral.ce.bc.cliente.adapters.dto;

import java.io.Serializable;
import lombok.*;

/**
 * @author Troby Wen
 *
 */
@Getter
@Setter

public class ConsultarClienteCanalesMsjSol implements Serializable{
	//todo: header, body, satus
	private static final long serialVersionUID = 1L;
	private CabeceraSol cabeceraSol;
	private CabeceraCanalesSol cabeceraCanalesSol;
	private Perfil perfil;
	private String cedula;
	
}
