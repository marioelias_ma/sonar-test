package com.bgeneral.ce.bc.cliente.adapters.dto;

import java.io.Serializable;
import java.util.List;

import lombok.*;

/**
 * @author alugo
 *
 */
@Getter
@Setter

public class EstadosPorRegistro implements Serializable{
	private static final long serialVersionUID = 1L;
    private List<Estado> estado;
}
