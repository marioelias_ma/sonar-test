package com.bgeneral.ce.bc.cliente.adapters.dto;
import java.io.Serializable;
import java.util.Date;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonProperty;

import lombok.*;

/**
 * @author alugo
 *
 */
@Getter
@Setter

public class CabeceraCanalesSol implements Serializable {
	private static final long serialVersionUID = 1L;
	@JsonProperty("aplicacion")
	private String aplicacion;
	
	@JsonProperty("canal")
	private String canal;
	
	@JsonProperty("fecha")
	//@JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "yyyy-MM-ddTHH:mm:ss.SSSZ")
	private String fecha;
	
	@JsonProperty("nodo")
	private Integer nodo;
	
	@JsonProperty("sesionBg")
	private String sesionBg;
	
	@JsonProperty("direccionIp")
	private String direccionIp;
	
	@JsonProperty("origenSolicitud")
	private String origenSolicitud;
}
