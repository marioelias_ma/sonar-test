package com.bgeneral.ce.bc.cliente.features.dto.cliente;

import java.io.Serializable;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class ClienteCanalResp implements Serializable{
	
	/**
	 * 
	 */
	private static final long serialVersionUID = -5750929662357775668L;
	private Integer numeroCliente;

}
