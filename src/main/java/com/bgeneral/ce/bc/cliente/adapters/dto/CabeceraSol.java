package com.bgeneral.ce.bc.cliente.adapters.dto;
import java.io.Serializable;

import com.fasterxml.jackson.annotation.JsonProperty;

import lombok.*;

/**
 * @author alugo
 *
 */
@Getter
@Setter

public class CabeceraSol implements Serializable {
	private static final long serialVersionUID = 1L;
	@JsonProperty("numeroUnico")
	private Integer numeroUnico;
	
	@JsonProperty("oficina")
	private Integer oficina;
	
	@JsonProperty("rolUsuario")
	private Integer rolUsuario;
	
	@JsonProperty("terminal")
	private String  terminal;
	
	@JsonProperty("usuario")
	private String  usuario;
}