package com.bgeneral.ce.bc.cliente.features;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.bgeneral.ce.bc.cliente.adapters.cliente.ClienteAdapter;
import com.bgeneral.ce.bc.cliente.features.command.ConsultarCliente;
import com.bgeneral.ce.bc.cliente.features.command.impl.ConsultarClienteCommandHandler;
import com.bgeneral.ce.bc.cliente.features.dto.cliente.ClienteCanalResp;
import com.bgeneral.ce.bc.cliente.features.dto.cliente.ClienteCanalRq;
import com.bgeneral.ce.bc.cliente.features.ports.IAmClienteAdapter;
import com.bgeneral.ce.bc.clientecobis.common.decorators.LogDecoratorHandler;
import com.bgeneral.ce.bc.clientecobis.common.dto.BCRequest;
import com.bgeneral.ce.bc.clientecobis.common.dto.BCResponse;
import com.bgeneral.ce.bc.util.AppProperties;



/**
 * @author Alugo
 *
 */
@RestController
@RequestMapping("/api/ce/bc/cliente")
public class ClienteController {
	
	@Autowired
	private AppProperties configuration;
	
	/**
	 * Metodo de prueba, con esta se valida el server se levanto correctamente.
	 * 
	 * @return
	 */
	@RequestMapping(value = "/healthcheck", method = RequestMethod.POST, consumes = "application/json", produces = "application/json")
	public String healthCheck() {
		return "{"
				+ "\"nombre\": \"Cliente\","
				+ "\"status\": \"OK\","
				+ "}";
	}

	/**
	 * @param request
	 * @return
	 * @throws Exception
	 */
	@RequestMapping(value = "/consultarclientecanales", method = RequestMethod.POST, consumes = "application/json", produces = "application/json")
	public BCResponse<ClienteCanalResp> IdentificarCliente(@RequestBody BCRequest<ClienteCanalRq> request)
			throws Exception {
	
		String restEndpoint = new StringBuilder().append(configuration.getRestEndpoint().getHost()).toString();
		IAmClienteAdapter callClienteAdapter  = new ClienteAdapter(restEndpoint); 
		
				
		ConsultarClienteCommandHandler consultarClienteCanalCommandHandler = new ConsultarClienteCommandHandler(callClienteAdapter);

		LogDecoratorHandler<ConsultarCliente, ClienteCanalRq, ClienteCanalResp> handler = new LogDecoratorHandler<>(consultarClienteCanalCommandHandler);
		return handler.handle(new ConsultarCliente(request));
		
	}
}
