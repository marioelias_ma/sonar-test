package com.bgeneral.ce.bc.cliente.adapters.cliente;

import javax.xml.datatype.DatatypeConfigurationException;

import com.bgeneral.ce.bc.cliente.features.dto.cliente.ClienteCanalResp;
import com.bgeneral.ce.bc.cliente.features.dto.cliente.ClienteCanalRq;
import com.bgeneral.ce.bc.cliente.features.ports.IAmClienteAdapter;
import com.bgeneral.ce.bc.clientecobis.common.dto.BCRequest;
import com.bgeneral.ce.bc.clientecobis.common.dto.BCResponse;
import com.google.gson.FieldNamingPolicy;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

import feign.Feign;
import feign.Headers;
import feign.RequestLine;
import feign.gson.GsonDecoder;
import feign.gson.GsonEncoder;

public class ClienteAdapter implements IAmClienteAdapter {

	
	
	interface IAmConsultarClienteCanales{
		@Headers("Content-Type: Application/json")
		@RequestLine("POST /")
		BCResponse<ClienteCanalResp> consultarClienteCanales(BCRequest<ClienteCanalRq> request);
	}
	
	/**
	 * 
	 */
	private String hostUrl;

	/**
	 * @param url
	 * @param timeOut
	 * @throws Exception
	 */
	public ClienteAdapter(String url){
		this.hostUrl = url;
	}
	
	/**
	 * @param request
	 * @return
	 * @throws DatatypeConfigurationException
	 */
	@Override
	public BCResponse<ClienteCanalResp> consultarClienteCanales(BCRequest<ClienteCanalRq> request){
		
		String url = this.hostUrl +"/sen/ce/cliente/consultarclientecanales";
		IAmConsultarClienteCanales builder = Feign.builder().encoder(new GsonEncoder(getSerializer()))
														.decoder(new GsonDecoder(getSerializer()))
															.target(IAmConsultarClienteCanales.class, url);
				
		return builder.consultarClienteCanales(request);
	}
	
	/**
	 * @return Gson
	 */
	private Gson getSerializer() {
		GsonBuilder builder = new GsonBuilder();
		builder.setFieldNamingPolicy(FieldNamingPolicy.IDENTITY);
		return builder.create();
	}

}