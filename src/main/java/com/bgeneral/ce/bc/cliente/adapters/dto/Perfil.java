package com.bgeneral.ce.bc.cliente.adapters.dto;
import java.io.Serializable;

import com.fasterxml.jackson.annotation.JsonProperty;

import lombok.*;

/**
 * @author alugo
 *
 */
@Getter
@Setter

public class Perfil implements Serializable{
	private static final long serialVersionUID = 1L;
	
	@JsonProperty("login")
	private String login;
	
	@JsonProperty("numeroCliente")
	private Integer numeroCliente;
	
	@JsonProperty("numeroBancaVirtual")
	private Integer numeroBancaVirtual;
	
	@JsonProperty("alias")
	private String alias;
	
	@JsonProperty("numeroPerfil")
	private Integer numeroPerfil;
	
	@JsonProperty("numeroClienteUsuario")
	private Integer numeroClienteUsuario;
}



