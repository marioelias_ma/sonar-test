package com.bgeneral.ce.bc;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.context.properties.EnableConfigurationProperties;
import org.springframework.boot.web.servlet.support.SpringBootServletInitializer;
import org.springframework.context.annotation.ComponentScan;

@SpringBootApplication
@EnableConfigurationProperties
@ComponentScan({ "com.bgeneral.ce.bc" })
public class ConsultarClienteCanalesApplication extends SpringBootServletInitializer{

	public static void main(String[] args) {
		SpringApplication.run(ConsultarClienteCanalesApplication.class, args);
	}
}
