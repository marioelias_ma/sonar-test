package com.bgeneral.ce.bc.clientecobis.controller.dto;

import java.io.Serializable;

import com.bgeneral.ce.bc.clientecobis.common.dto.Catalogo;
import com.fasterxml.jackson.annotation.JsonProperty;

import lombok.Getter;
import lombok.Setter;

/**
 * @author jnieves
 *
 */
@Getter
@Setter
public class DireccionClienteCanalesDTO implements Serializable {
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 9029733212067654670L;

	@JsonProperty("descripcion")
	private String descripcion;
	
	@JsonProperty("tipoDireccion")
	private Catalogo tipoDireccion;
	
	@JsonProperty("corregimiento")
	private Catalogo corregimiento;
	
	@JsonProperty("ciudad")
	private Catalogo ciudad;

	@JsonProperty("pais")
	private Catalogo pais;
	
	@JsonProperty("oficina")
	private Catalogo oficina;
	
	@JsonProperty("barrio")
	private Catalogo barrio;

	@JsonProperty("calle")
	private String calle;

	@JsonProperty("codigoCasa")
	private String codigoCasa;
	
	@JsonProperty("esPrincipal")
	private boolean esPrincipal;
}
