package com.bgeneral.ce.bc.clientecobis.common.decorators;

import java.io.Serializable;
import java.util.UUID;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.apache.logging.log4j.ThreadContext;

import com.bgeneral.ce.bc.clientecobis.common.dto.BCResponse;
import com.bgeneral.ce.bc.clientecobis.controller.interfaces.IAmCommand;
import com.bgeneral.ce.bc.clientecobis.controller.interfaces.IAmCommandHandler;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;

/**
 * @author F. Castillo
 *
 * @param <C> TCommand
 * @param <I> TRequest
 * @param <O> TResponse
 */
public class LogDecoratorHandler<C extends IAmCommand<I, O>, I extends Serializable, O extends Serializable> implements IAmCommandHandler<C, I, O> {
    private final IAmCommandHandler<C, I, O> innerHandler;
    private final Logger logger;
    
    /**
     * @param innerHandler
     */
    public LogDecoratorHandler(IAmCommandHandler<C, I, O> innerHandler) {
        logger = LogManager.getLogger("LogDecorator");
        this.innerHandler = innerHandler;
    }
    
    /* (non-Javadoc)
     * @see com.bgeneral.ce.bc.tokennumerico.infrastructure.IAmCommandHandler#handle(com.bgeneral.ce.bc.tokennumerico.infrastructure.IAmCommand)
     */
    @Override
    public BCResponse<O> handle(C command){
    	ObjectMapper objectMapper = new ObjectMapper();
    	BCResponse<O> result = null;
    	try{
        	ThreadContext.put("requestId", UUID.randomUUID().toString());        	    
           	logger.debug("Request: "  + objectMapper.writeValueAsString(command));
           	result = innerHandler.handle(command); // todas la excepciones esperadas de logica de negocio se controlan dentro del comando                
            logger.debug("Response: " + objectMapper.writeValueAsString(result));
        }
        catch(Exception e) {
            try {
				logger.error("Request", objectMapper.writeValueAsString(command));
	            logger.error("Error", e);
			} catch (JsonProcessingException e1) {
				logger.error("Error", e1);
			}
        }
    	return result;
    }
}