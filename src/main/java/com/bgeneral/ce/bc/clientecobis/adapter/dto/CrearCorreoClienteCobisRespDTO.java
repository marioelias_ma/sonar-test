package com.bgeneral.ce.bc.clientecobis.adapter.dto;

import java.io.Serializable;

import com.fasterxml.jackson.annotation.JsonProperty;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class CrearCorreoClienteCobisRespDTO implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = -1855658199372133008L;

	@JsonProperty("codigoCorreo")
	private Integer codigoCorreo;
	
}
