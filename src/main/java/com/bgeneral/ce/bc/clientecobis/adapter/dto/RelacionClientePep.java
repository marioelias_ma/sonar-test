package com.bgeneral.ce.bc.clientecobis.adapter.dto;

import java.io.Serializable;

import com.bgeneral.ce.bc.clientecobis.common.dto.Catalogo;
import com.fasterxml.jackson.annotation.JsonProperty;

import lombok.Getter;
import lombok.Setter;

/**
 * 
 * @author anlugo
 *
 */
@Getter
@Setter
public class RelacionClientePep extends Catalogo implements Serializable{

	private static final long serialVersionUID = 1L;
	
	@JsonProperty("nombre")
	private String nombre;
}
