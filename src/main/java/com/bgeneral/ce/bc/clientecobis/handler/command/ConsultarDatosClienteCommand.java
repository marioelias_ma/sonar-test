package com.bgeneral.ce.bc.clientecobis.handler.command;

import com.bgeneral.ce.bc.clientecobis.common.dto.BCRequest;
import com.bgeneral.ce.bc.clientecobis.controller.dto.CredencialesClienteDTO;
import com.bgeneral.ce.bc.clientecobis.controller.dto.DatosClienteDTO;
import com.bgeneral.ce.bc.clientecobis.controller.interfaces.IAmCommand;

import lombok.Getter;

@Getter
public class ConsultarDatosClienteCommand implements IAmCommand<CredencialesClienteDTO, DatosClienteDTO>{
	
	private BCRequest<CredencialesClienteDTO> dto;
	
	public ConsultarDatosClienteCommand (BCRequest<CredencialesClienteDTO> body) {
		this.dto = body;
	}

}
