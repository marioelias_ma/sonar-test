package com.bgeneral.ce.bc.clientecobis.controller.dto;

import java.io.Serializable;

import com.fasterxml.jackson.annotation.JsonProperty;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class CodigosRegistroClienteDTO implements Serializable{
	/**
	 * 
	 */
	private static final long serialVersionUID = 2398276313597124923L;
	
	@JsonProperty("enteMis")
	private Integer enteMis;
}