package com.bgeneral.ce.bc.clientecobis.handler.dto;

import java.io.Serializable;


import com.fasterxml.jackson.annotation.JsonProperty;

import lombok.Getter;
import lombok.Setter;

/**
 * @author anlugo
 *
 */
@Getter
@Setter
public class ClientePEPDTO implements Serializable{

	
	private static final long serialVersionUID = 1L;

	@JsonProperty("esPEP")
	private boolean esPEP;
	
}
