package com.bgeneral.ce.bc.clientecobis.controller.dto;

import java.io.Serializable;
import java.util.Date;

import com.bgeneral.ce.bc.clientecobis.common.dto.Catalogo;
import com.bgeneral.ce.bc.clientecobis.common.util.transformation.CustomDateSerializer;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class DatosClienteDTO implements Serializable {
	
	private static final long serialVersionUID = -1704868334814043196L;

	@JsonProperty("numeroCliente")
	private Integer numeroCliente;
	
	@JsonProperty("apellidoPaterno")
	private String apellidoPaterno;

	@JsonProperty("apellidoMaterno")
	private String apellidoMaterno;

	@JsonProperty("apellidoCasada")
	private String apellidoCasada;

	@JsonProperty("nombre")
	private String nombre;
	
	@JsonProperty("segundonombre")
	private String segundonombre;
	
	@JsonProperty("nombreCompleto")
	private String nombreCompleto;
	
	@JsonProperty("cedula")
	private String cedula;

	@JsonProperty("pasaporte")
	private String pasaporte;	

	@JsonProperty("segurosocial")
	private String segurosocial;	
	
	@JsonProperty("fechaNacimiento")
	@JsonSerialize(using = CustomDateSerializer.class)
	private Date  fechaNacimiento;
	
	@JsonProperty("edad")
	private Integer edad;
	
	@JsonProperty("sexo")
	private Catalogo sexo;

	@JsonProperty("pais")
	private Integer pais;

	@JsonProperty("nacionalidad")
	private String nacionalidad;
	
	@JsonProperty("pais_2")
	private Integer pais_2;

	@JsonProperty("nacionalidad_2")
	private String nacionalidad_2;

	@JsonProperty("educacion")
	private Catalogo educacion;
	
	@JsonProperty("actividad")
	private Catalogo actividad;

	@JsonProperty("estadoCivil")
	private Catalogo estadoCivil;
	
	@JsonProperty("tipoPersona")
	private Catalogo tipoPersona;

	@JsonProperty("profesion")
	private Catalogo profesion;
		
	@JsonProperty("paisRiesgo")
	private Catalogo paisRiesgo;
	
	@JsonProperty("codigoOficial")
	private Integer codigoOficial;
	
	@JsonProperty("oficial")
	private String oficial;

	@JsonProperty("descripcionOficial")
	private String descripcionOficial;

	@JsonProperty("usuario")
	private Catalogo usuario;
	
	@JsonProperty("fechaRegistro")
	@JsonSerialize(using = CustomDateSerializer.class)
	private Date  fechaRegistro;
	
	@JsonProperty("fechaUltimaModificacion")
	@JsonSerialize(using = CustomDateSerializer.class)
	private Date  fechaUltimaModificacion;
	
	@JsonProperty("fechaIngreso")
	@JsonSerialize(using = CustomDateSerializer.class)
	private Date  fechaIngreso;
	
	@JsonProperty("fechaExp")
	@JsonSerialize(using = CustomDateSerializer.class)
	private Date  fechaExp;	
	
	@JsonProperty("grupo")
	private Catalogo grupo;
	
	@JsonProperty("retencion")
	private String retencion;		

	@JsonProperty("codigoMalaRef")
	private String codigoMalaRef;		

	@JsonProperty("codigoCalificacion")
	private Catalogo codigoCalificacion;	

	@JsonProperty("comentario")
	private String comentario;	
	
	@JsonProperty("grupoRel")
	private String grupoRel;		

	@JsonProperty("estatus")
	private Catalogo estatus;	

	@JsonProperty("fechaVerificacion")
	@JsonSerialize(using = CustomDateSerializer.class)
	private Date  fechaVerificacion;
	
	@JsonProperty("difunto")
	private String difunto;		

	@JsonProperty("versionCsba")
	private Integer versionCsba;		
	
	@JsonProperty("autorizacionAPC")
	private Boolean autorizacionAPC;		

	@JsonProperty("necesitaRevision")
	private Boolean necesitaRevision;	

	@JsonProperty("referenciaInterna")
	private String referenciaInterna;		
	
	@JsonProperty("clasificacion")
	private Catalogo clasificacion;		
	
	@JsonProperty("subClasificacion")
	private Catalogo subClasificacion;	
	
	@JsonProperty("relacion")
	private Catalogo relacion;		
	
	@JsonProperty("fechaInicioRelacion")
	private String fechaInicioRelacion;		

	@JsonProperty("oficialCodigo")
	private Integer oficialCodigo;	

	@JsonProperty("oficialSector")
	private Catalogo oficialSector;		
	
	@JsonProperty("oficialSubSector")
	private Catalogo oficialSubSector;		

	@JsonProperty("oficialTipo")
	private Catalogo oficialTipo;	

	@JsonProperty("usuarioRed")
	private String usuarioRed;
	
	@JsonProperty("clienteBanca")
	private Catalogo clienteBanca;	
	
	@JsonProperty("paisNacimiento")
	private Catalogo paisNacimiento;	

	@JsonProperty("cantidadCuentasPasivas")
	private Integer cantidadCuentasPasivas;	
	
	@JsonProperty("evaluacionCumplimiento")
	private Catalogo evaluacionCumplimiento;	

	@JsonProperty("clienteEsPep")
	private Boolean clienteEsPep;		

	@JsonProperty("clienteTienePantallazo")
	private Boolean clienteTienePantallazo;	
	
}
