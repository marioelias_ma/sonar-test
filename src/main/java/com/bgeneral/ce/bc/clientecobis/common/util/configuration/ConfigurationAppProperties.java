package com.bgeneral.ce.bc.clientecobis.common.util.configuration;

import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.boot.context.properties.EnableConfigurationProperties;
import org.springframework.context.annotation.Configuration;

import lombok.Getter;
import lombok.Setter;

/**
 * @author Linneker
 *
 */
@Getter
@Configuration
@ConfigurationProperties
@EnableConfigurationProperties
public class ConfigurationAppProperties {
	private LoggingConf logs = new LoggingConf();
	private ClientConf soapEndpoint = new ClientConf();
	private ClientConf restEndpoint = new ClientConf();
	private CBConf circuitbreaker = new CBConf ();

	@Getter
	@Setter
	public static class LoggingConf {
		private String level;
		private String location;
	}
	
	@Getter
	@Setter
	public static class ClientConf {
		private String host;
		private int timeout;
	}
	
	@Getter
	@Setter
	public static class CBConf {
		private int thresholdPercentage;
		private int requestVolumeThreshold;
		private int sleepWindowInMilliseconds;
	}
}
