package com.bgeneral.ce.bc.clientecobis.common.dto;

import lombok.*;

import java.io.Serializable;

import com.fasterxml.jackson.annotation.JsonProperty;

/**
 * @author jnieves, brodriguez
 *
 */
@Getter
@Setter
public class Perfil implements Serializable{
	  @JsonProperty("login")
	  private String login;

	  @JsonProperty("numeroCliente")
	  private Integer numeroCliente;

	  @JsonProperty("numeroBancaVirtual")
	  private Integer numeroBancaVirtual;

	  @JsonProperty("alias")
	  private String alias;

	  @JsonProperty("numeroPerfil")
	  private Integer numeroPerfil;

	  @JsonProperty("numeroClienteUsuario")
	  private Integer numeroClienteUsuario;

	  @JsonProperty("vistaAsociada")
	  private Integer vistaAsociada;

}
