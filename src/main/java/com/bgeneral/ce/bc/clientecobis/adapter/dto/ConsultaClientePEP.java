package com.bgeneral.ce.bc.clientecobis.adapter.dto;

import java.io.Serializable;

import com.fasterxml.jackson.annotation.JsonProperty;

import lombok.Getter;
import lombok.Setter;

/***
 * 
 * @author anlugo
 *
 */
@Getter
@Setter
public class ConsultaClientePEP implements Serializable {

	private static final long serialVersionUID = -1704868334814043196L;

	@JsonProperty("numeroRegistros")
	private Integer numeroRegistros;
	
	private ClientePEP clientePEP;
}

