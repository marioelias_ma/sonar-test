package com.bgeneral.ce.bc.clientecobis.handler.dto;

import java.io.Serializable;

import com.bgeneral.ce.bc.clientecobis.common.dto.Catalogo;
import com.fasterxml.jackson.annotation.JsonProperty;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class TelefonoClienteDTO implements Serializable {


	/**
	 * 
	 */
	private static final long serialVersionUID = -4001636281474707101L;

	@JsonProperty("numeroTelefono")
	private String numeroTelefono;

	@JsonProperty("enteMis")
	private Catalogo direccion;

	@JsonProperty("tipoTelefono")
	private String tipoTelefono;

	@JsonProperty("extencion")
	private String extencion;

	@JsonProperty("pais")
	private Catalogo pais;
}
