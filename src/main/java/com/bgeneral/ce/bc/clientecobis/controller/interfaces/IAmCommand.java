package com.bgeneral.ce.bc.clientecobis.controller.interfaces;

import java.io.Serializable;

/**
 * @author jnieves
 *
 * @param <I>
 * @param <O>
 */
public interface IAmCommand<I extends Serializable, O extends Serializable> {

}