package com.bgeneral.ce.bc.clientecobis.common.util.transformation;

import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Date;

import com.fasterxml.jackson.core.JsonGenerator;
import com.fasterxml.jackson.databind.SerializerProvider;
import com.fasterxml.jackson.databind.ser.std.StdSerializer;

public class CustomDateSerializer extends StdSerializer<Date> {
  
    /**
	 * 
	 */
	private static final long serialVersionUID = 6947892791800137373L;
	private SimpleDateFormat formatter 
      = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSSZ");
 
    public CustomDateSerializer() {
        this(null);
    }
 
    public CustomDateSerializer(Class<Date> t) {
        super(t);
    }
     
    @Override
    public void serialize (Date value, JsonGenerator gen, SerializerProvider arg2)
      throws IOException {
        gen.writeString(formatter.format(value));
    }
}