package com.bgeneral.ce.bc.clientecobis.controller.dto;

import java.io.Serializable;

import com.fasterxml.jackson.annotation.JsonProperty;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class TelefonoClienteCanalesDTO implements Serializable{
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 1995857948709548771L;

	@JsonProperty("numeroTelefono")
	private String numeroTelefono;

	@JsonProperty("tipoTelefono")
	private String tipoTelefono;

	@JsonProperty("extencion")
	private String extencion;
}
