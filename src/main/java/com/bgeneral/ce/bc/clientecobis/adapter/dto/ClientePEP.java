package com.bgeneral.ce.bc.clientecobis.adapter.dto;

import java.io.Serializable;
import java.sql.Date;

import com.bgeneral.ce.bc.clientecobis.common.dto.Catalogo;
import com.bgeneral.ce.bc.clientecobis.common.util.transformation.CustomDateSerializer;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;


import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class ClientePEP implements Serializable {

	private static final long serialVersionUID = 1L;

	@JsonProperty("secuencial")
	private Integer secuencial;
	
	@JsonProperty("numeroCliente")
	private Integer numeroCliente;
	
	@JsonProperty("nombreCompleto")
	private String nombreCompleto;
	
	@JsonProperty("fechaModificacion")
	@JsonSerialize(using = CustomDateSerializer.class)
	private Date fechaModificacion;
	
	@JsonProperty("tipoCliente")
	private Catalogo tipoCliente;
	
	@JsonProperty("relacionClientePep")
	private RelacionClientePep relacionClientePep;
	
	@JsonProperty("cargoPep")
	private CargoPep cargoPep;
	
	@JsonProperty("categoriaPep")
	private Catalogo categoriaPep;
	
	@JsonProperty("estadoPep")
	private Catalogo estadoPep;
	
	
	
}
