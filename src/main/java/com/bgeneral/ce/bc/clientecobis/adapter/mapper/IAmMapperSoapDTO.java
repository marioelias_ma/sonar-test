package com.bgeneral.ce.bc.clientecobis.adapter.mapper;

import java.io.Serializable;

import javax.xml.datatype.DatatypeConfigurationException;

import com.bgeneral.ce.bc.clientecobis.common.dto.BCRequest;
import com.bgeneral.ce.bc.clientecobis.common.dto.BCResponse;

/**
 * @author jnieves
 * 
 *         Interfaz (Tranformador) para los mapper entre los DTO de Handler y el
 *         Adapter
 *
 * @param <I> THandlerRequest extends Serializable
 * @param <O> THandlerResponse extends Serializable
 * @param <E> TMsjSolTipo: Clase del Mensaje de Entrada SOAP
 * @param <S> TMsjRespTipo: Clase del Mensaje de Salida SOAP
 */
public interface IAmMapperSoapDTO<I extends Serializable, O extends Serializable, E, S> {

	E handlerRequestTOAdapterRequestDTO(BCRequest<I> request) throws DatatypeConfigurationException;

	BCResponse<O> adapterResponseTOHandlerResponseDTO(S response) throws DatatypeConfigurationException;
}