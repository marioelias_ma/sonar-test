package com.bgeneral.ce.bc.clientecobis.common.dto;

import java.io.Serializable;

import com.fasterxml.jackson.annotation.JsonProperty;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class Catalogo implements Serializable {
	
	/**
	 * 
	 */
	private static final long serialVersionUID = -5789289002264854770L;

	@JsonProperty("codigo")
	private String codigo;

	@JsonProperty("descripcion")
	private String descripcion;
}
