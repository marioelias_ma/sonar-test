package com.bgeneral.ce.bc.clientecobis.common.util;

public class UtilBG {
	
	private UtilBG() {}


	public static boolean isNan(String str) {
		return !(str.matches("((-|\\+)?[0-9]+(\\.[0-9]+)?)+"));
	}
	
	public static String concat(String str1, String str2) {
		StringBuilder builder = new StringBuilder();
		builder.append(str1);
		builder.append(str2);
		return builder.toString();
	}
	

	/**
	 * Metodo que formatea la estructura de documentos de identidad hacia formato
	 * legacy de cobis
	 * 
	 * @param idClient 
	 *                 <String> cadena de la cedula con un formato similar
	 *                 8V-456-6547
	 * @return <String> con el formato
	 */
	public static String FormatDocumentIDLegacy(String idClient) {
		String[] arrayId = idClient.split("-");

		arrayId[0] = formatInicialProvinciaStatus(arrayId[0]);
		arrayId[1] = String.format("%04d", Integer.parseInt(arrayId[1]));
		arrayId[2] = String.format("%05d", Integer.parseInt(arrayId[2]));
		return String.format("%4s%4s%5s", arrayId[0], arrayId[1], arrayId[2]);
	}

	/**
	 * Metodo que formatea la cabecera de los documentos de identidad con el formato
	 * legacy de cobis
	 * 
	 * @param catProvincia 
	 * 						<String> Primera cadena o array[0] de un split de la cedula 
	 * 						en el formato 8V-456-6547
	 * @return <String> con el formato
	 */
	private static String formatInicialProvinciaStatus(String catProvincia) {

		/*
		 * Si posee menos de 4 caracteres debe de validar su estructura si posee
		 * combinacion de 3 entre numero y caracter en caso contrario se envia como
		 * llega en el split
		 */
		if (catProvincia.length() < 4) {
			/*
			 * se valida si en la cadena posee al menos un caracter
			 */
			if (UtilBG.isNan(catProvincia.toString())) {
				/*
				 * Siendo el caso se valida si el primero es numero o string Si es solo string,*
				 * entoces puede venir 1 o 2 caracteres y si es uno se completa con espacios del
				 * lado derecho
				 */
				if (UtilBG.isNan(Character.toString(catProvincia.charAt(0)))) {
					catProvincia = String.format("00%-2s", catProvincia);
				} else {
					/*
					 * En caso contrario se valida su longitud por si presenta el caso de: 2
					 * numericos un caracter 1 numerico 2 carateres
					 */
					if (catProvincia.length() == 3) {
						/*
						 * Se valida si el segundo es letra o numero Si es caracter entonces se agrega
						 * un 0 al principio ya que siendo de longitud 3, su primer caracter es numerico
						 * y el segundo es String, por deduccion el 3ro es caracter tambien
						 */
						if (UtilBG.isNan(Character.toString(catProvincia.charAt(1)))) {
							catProvincia = String.format("0%2s", catProvincia);
						} else {
							/*
							 * Caso contrario es que los 2 primeros son numeros y el ultimo es caracter,
							 * entonces se completa con espacios por la derecha.
							 */
							catProvincia = String.format("%2s ", catProvincia);
						}
					} else {
						/*
						 * sino tiene longitud de 3, es porque solo llego 1 caracter y 1 numero
						 */
						catProvincia = String.format("0%2s ", catProvincia);
					}
				}
			} else {
				/*
				 * En este caso, de que no tenga caracteres es porque solo valores numerico ya
				 * sea de 1 o 2 digitos
				 */
				catProvincia = String.format("%02d  ", Integer.parseInt(catProvincia));
			}
		}
		return catProvincia;
	}

}
