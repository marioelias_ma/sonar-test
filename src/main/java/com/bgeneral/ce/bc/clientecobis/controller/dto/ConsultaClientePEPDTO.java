package com.bgeneral.ce.bc.clientecobis.controller.dto;

import java.io.Serializable;


import com.fasterxml.jackson.annotation.JsonProperty;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class ConsultaClientePEPDTO implements Serializable{

	private static final long serialVersionUID = 1L;
	
	@JsonProperty("esPEP")
	private boolean esPEP;
	
}
