package com.bgeneral.ce.bc.clientecobis.controller.dto;

import java.io.Serializable;

import com.fasterxml.jackson.annotation.JsonProperty;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class CorreoElectronicoCanalesDTO implements Serializable{
	
	/**
	 * 
	 */
	private static final long serialVersionUID = -8674234603064391253L;
	
	@JsonProperty("codigo")
	private String codigo;

	@JsonProperty("direccionEmail")
	private String direccionEmail;
	
	@JsonProperty("tipoMail")
	private String tipoMail;

}
