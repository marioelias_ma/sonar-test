package com.bgeneral.ce.bc.clientecobis.adapter;

import static com.bgeneral.ce.bc.clientecobis.common.util.transformation.RestPolicyUtil.buildGsonInterface;

import org.modelmapper.internal.util.Assert;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Repository;

import com.bgeneral.ce.bc.clientecobis.common.dto.BCRequest;
import com.bgeneral.ce.bc.clientecobis.common.dto.BCResponse;
import com.bgeneral.ce.bc.clientecobis.common.dto.EmptyBodyT;
import com.bgeneral.ce.bc.clientecobis.common.util.configuration.ConfigurationAppProperties;
import com.bgeneral.ce.bc.clientecobis.handler.dto.DatosDireccionClienteCobisHandlerDTO;
import com.bgeneral.ce.bc.clientecobis.handler.interfaces.IAmRestAdapter;
import com.bgeneral.ce.bc.clientecobis.adapter.mapper.IAmMapperJsonDTO;
import com.bgeneral.ce.bc.clientecobis.adapter.dto.DatosDireccionClienteCobisDTO;

import feign.Headers;
import feign.RequestLine;

/**
 * Clase adapter que comunica con el bus para obtener la datos asociado a la
 * direccion de un cliente en Cobis
 * 
 * @author jnieves
 *
 */
@Repository("ConsultarDireccionClienteCobisAdapter")
public class ConsultarDireccionClienteCobisAdapter
		implements IAmRestAdapter<EmptyBodyT, DatosDireccionClienteCobisHandlerDTO> {
	
	@Autowired
	private ConfigurationAppProperties configurationAppProperties;

	@Autowired
	@Qualifier("ConsultarDireccionClienteCobisMapper")
	private IAmMapperJsonDTO<EmptyBodyT, DatosDireccionClienteCobisHandlerDTO, EmptyBodyT, DatosDireccionClienteCobisDTO> mapper;

	/**
	 * @author jnieves
	 *
	 */
	@FunctionalInterface
	interface IConsultarDireccionClienteCobis {
		@Headers("Content-Type: Application/json")
		@RequestLine("POST /")
		BCResponse<DatosDireccionClienteCobisDTO> consultarDireccionClienteCobis(BCRequest<EmptyBodyT> request);
	}
	
	/*
	 * (non-Javadoc)
	 * 
	 * @see com.bgeneral.ce.bc.clientecobis.handler.interfaces.IAmRestAdapter#
	 * callRestService(com.bgeneral.ce.bc.clientecobis.common.dto.BCRequest)
	 */
	@Override
	public BCResponse<DatosDireccionClienteCobisHandlerDTO> callRestService(BCRequest<EmptyBodyT> request) {
		Assert.notNull(request, "BCRequest<EmptyBodyT>.getBody() NULL");

		IConsultarDireccionClienteCobis builder = buildGsonInterface(IConsultarDireccionClienteCobis.class,
				configurationAppProperties.getRestEndpoint(), "/sen/ce/cliente/consultardireccionclientecobis");

		BCResponse<DatosDireccionClienteCobisDTO> response = builder.consultarDireccionClienteCobis(request);
		BCResponse<DatosDireccionClienteCobisHandlerDTO> bcResponse = this.mapper.adapterResponseTOHandlerResponseDTO(response);

		Assert.notNull(bcResponse.getStatus().getReturnStatus(), "BCResponse<DatosDireccionClienteCobisHandlerDTO>.getStatus() NULL");
		return bcResponse;
	}

}
