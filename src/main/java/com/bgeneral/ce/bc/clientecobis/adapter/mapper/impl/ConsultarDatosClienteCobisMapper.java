package com.bgeneral.ce.bc.clientecobis.adapter.mapper.impl;

import org.modelmapper.ModelMapper;
import org.springframework.stereotype.Component;

import com.bgeneral.ce.bc.clientecobis.adapter.dto.CredencialesClienteCobisDTO;
import com.bgeneral.ce.bc.clientecobis.adapter.dto.DatosClienteCobisDTO;
import com.bgeneral.ce.bc.clientecobis.adapter.mapper.IAmMapperJsonDTO;
import com.bgeneral.ce.bc.clientecobis.common.dto.BCRequest;
import com.bgeneral.ce.bc.clientecobis.common.dto.BCResponse;
import com.bgeneral.ce.bc.clientecobis.common.util.catalogs.CodeCatalog;
import com.bgeneral.ce.bc.clientecobis.controller.dto.CredencialesClienteDTO;
import com.bgeneral.ce.bc.clientecobis.handler.dto.DatosClienteHandlerDTO;

/**
 * Clase Mapper para el Request y Response de la clase de
 * @Component("ConsultarDatosClienteCobisMapper")
 * 
 * @author igudino
 */
@Component("ConsultarDatosClienteCobisMapper")
public class ConsultarDatosClienteCobisMapper implements IAmMapperJsonDTO<CredencialesClienteDTO, DatosClienteHandlerDTO, CredencialesClienteCobisDTO, DatosClienteCobisDTO>{
	
	@Override
	public BCRequest<CredencialesClienteCobisDTO> handlerRequestTOAdapterRequestDTO(BCRequest<CredencialesClienteDTO> request) {
		BCRequest<CredencialesClienteCobisDTO> backendRequest = new BCRequest<>();
		backendRequest.setHeader(request.getHeader());
		backendRequest.setBody(new ModelMapper().map(request.getBody(), CredencialesClienteCobisDTO.class));
		return backendRequest;
	}

	@Override
	public BCResponse<DatosClienteHandlerDTO> adapterResponseTOHandlerResponseDTO(
			BCResponse<DatosClienteCobisDTO> response) {
		BCResponse<DatosClienteHandlerDTO> bcResponse = new BCResponse<>();
		bcResponse.setHeader(response.getHeader());
		bcResponse.setStatus(response.getStatus());
		
		if(CodeCatalog.CODE_SUCCESS.codeS().equals(response.getStatus().getReturnStatus().getReturnCode())) {
			DatosClienteHandlerDTO body = new DatosClienteHandlerDTO();
			
			
			body.setActividad(response.getBody().getActividad());
			body.setApellidoCasada(response.getBody().getApellidoCasada());
			body.setNumeroCliente(response.getBody().getNumeroCliente());
			body.setApellidoPaterno(response.getBody().getApellidoPaterno());
			body.setApellidoMaterno(response.getBody().getApellidoMaterno());
			body.setNumeroCliente(response.getBody().getNumeroCliente());
			body.setApellidoPaterno(response.getBody().getApellidoPaterno());
			body.setApellidoMaterno(response.getBody().getApellidoMaterno());
	        body.setApellidoCasada(response.getBody().getApellidoCasada());
			body.setNombre(response.getBody().getNombre());
			body.setSegundonombre(response.getBody().getSegundonombre());
			body.setNombreCompleto(response.getBody().getNombreCompleto());
			body.setCedula(response.getBody().getCedula());
			body.setPasaporte(response.getBody().getPasaporte());	
			body.setSegurosocial(response.getBody().getSegurosocial());	
			body.setFechaNacimiento(response.getBody().getFechaNacimiento());
			body.setEdad(response.getBody().getEdad());
			body.setSexo(response.getBody().getSexo());
			body.setPais(response.getBody().getPais());
			body.setNacionalidad(response.getBody().getNacionalidad());
			body.setPais_2(response.getBody().getPais_2());
			body.setNacionalidad_2(response.getBody().getNacionalidad_2());
			body.setEducacion(response.getBody().getEducacion());
			body.setEstadoCivil(response.getBody().getEstadoCivil());

			body.setTipoPersona(response.getBody().getTipoPersona());
			body.setProfesion(response.getBody().getProfesion());
			body.setPaisRiesgo(response.getBody().getPaisRiesgo());
			body.setCodigoOficial(response.getBody().getCodigoOficial());
			body.setOficial(response.getBody().getOficial());
			body.setDescripcionOficial(response.getBody().getDescripcionOficial());
			body.setUsuario(response.getBody().getUsuario());
			body.setFechaRegistro(response.getBody().getFechaRegistro());
			body.setFechaUltimaModificacion(response.getBody().getFechaUltimaModificacion());
			body.setFechaIngreso(response.getBody().getFechaIngreso());
			body.setFechaExp(response.getBody().getFechaExp());	
			body.setGrupo(response.getBody().getGrupo());
			body.setRetencion(response.getBody().getRetencion());		
			body.setCodigoMalaRef(response.getBody().getCodigoMalaRef());		
			body.setCodigoCalificacion(response.getBody().getCodigoCalificacion());	
			body.setComentario(response.getBody().getComentario());	
			body.setGrupoRel(response.getBody().getGrupoRel());		
			body.setEstatus(response.getBody().getEstatus());	
			body.setFechaVerificacion(response.getBody().getFechaVerificacion());
			body.setDifunto(response.getBody().getDifunto());		
			body.setVersionCsba(response.getBody().getVersionCsba());		
			body.setAutorizacionAPC(response.getBody().getAutorizacionAPC());		
			body.setNecesitaRevision(response.getBody().getNecesitaRevision());	
			body.setReferenciaInterna(response.getBody().getReferenciaInterna());		
			body.setClasificacion(response.getBody().getClasificacion());		
			body.setSubClasificacion(response.getBody().getSubClasificacion());	
			body.setRelacion(response.getBody().getRelacion());		
			body.setFechaInicioRelacion(response.getBody().getFechaInicioRelacion());		
			body.setOficialCodigo(response.getBody().getOficialCodigo());	
			body.setOficialSector(response.getBody().getOficialSector());		
			body.setOficialSubSector(response.getBody().getOficialSubSector());		
			body.setOficialTipo(response.getBody().getOficialTipo());	
			body.setUsuarioRed(response.getBody().getUsuarioRed());
			body.setClienteBanca(response.getBody().getClienteBanca());	
			body.setPaisNacimiento(response.getBody().getPaisNacimiento());	
			body.setCantidadCuentasPasivas(response.getBody().getCantidadCuentasPasivas());	
			body.setEvaluacionCumplimiento(response.getBody().getEvaluacionCumplimiento());	
			
			bcResponse.setBody(body);			
		
		}
	
		return bcResponse;
	}
	
}
