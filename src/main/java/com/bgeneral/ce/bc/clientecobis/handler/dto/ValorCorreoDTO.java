package com.bgeneral.ce.bc.clientecobis.handler.dto;

import java.io.Serializable;

import com.fasterxml.jackson.annotation.JsonProperty;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class ValorCorreoDTO implements Serializable{

	/**
	 * 
	 */
	private static final long serialVersionUID = 656104967576971345L;
	
	@JsonProperty("codigoCorreo")
	private Integer codigoCorreo;
	
}
