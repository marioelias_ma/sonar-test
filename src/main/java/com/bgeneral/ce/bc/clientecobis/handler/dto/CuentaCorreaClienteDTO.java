package com.bgeneral.ce.bc.clientecobis.handler.dto;

import java.io.Serializable;

import com.fasterxml.jackson.annotation.JsonProperty;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class CuentaCorreaClienteDTO implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = -2411632612864682044L;

	@JsonProperty("tipoUso")
	private String tipoUso;

	@JsonProperty("direccion")
	private String direccion;

	@JsonProperty("tipoMail")
	private String tipoMail;

}
