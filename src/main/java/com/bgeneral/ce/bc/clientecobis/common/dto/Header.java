package com.bgeneral.ce.bc.clientecobis.common.dto;

import java.io.Serializable;
import java.util.Date;

import com.bgeneral.ce.bc.clientecobis.common.util.transformation.CustomDateSerializer;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;

import lombok.Getter;
import lombok.Setter;

/**
 * @author jnieves, brodriguez
 *
 */
@Getter
@Setter
public class Header implements Serializable {
	/**
	* 
	*/
	private static final long serialVersionUID = 1L;
	@JsonProperty("numeroUnico")
	private Integer numeroUnico;

	@JsonProperty("oficina")
	private Integer oficina;

	@JsonProperty("terminal")
	private String terminal;

	@JsonProperty("usuario")
	private String usuario;

	@JsonProperty("rolUsuario")
	private Integer rolUsuario;

	@JsonProperty("aplicacion")
	private String aplicacion;

	@JsonProperty("canal")
	private String canal;
	
	@JsonProperty("fecha")
	@JsonSerialize(using = CustomDateSerializer.class)
	private Date fecha;

	@JsonProperty("nodo")
	private Integer nodo;

	@JsonProperty("sesionBg")
	private String sesionBg;

	@JsonProperty("direccionIp")
	private String direccionIp;

	@JsonProperty("origenSolicitud")
	private String origenSolicitud;

	@JsonProperty("numeroReferencia")
	private String numeroReferencia;

	@JsonProperty("horaInicio")
	private String horaInicio;

	@JsonProperty("perfil")
	private Perfil perfil;
}