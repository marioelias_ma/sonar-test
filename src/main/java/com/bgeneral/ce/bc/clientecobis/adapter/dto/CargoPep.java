package com.bgeneral.ce.bc.clientecobis.adapter.dto;

import java.io.Serializable;
import java.util.Date;

import com.bgeneral.ce.bc.clientecobis.common.util.transformation.CustomDateSerializer;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;

import lombok.Getter;
import lombok.Setter;

/**
 * 
 * @author anlugo
 *
 */
@Getter
@Setter
public class CargoPep implements Serializable {

	private static final long serialVersionUID = 1L;

	@JsonProperty("descripcion")
	private String descripcion;
	
	@JsonProperty("institucion")
	private String institucion;
	
	@JsonProperty("mantiene")
	private String mantiene;
	
	@JsonProperty("fechaFinalizacion")
	@JsonSerialize(using = CustomDateSerializer.class)
	private Date fechaFinalizacion;
	
}
