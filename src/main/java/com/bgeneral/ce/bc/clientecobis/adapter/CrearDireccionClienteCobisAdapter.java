package com.bgeneral.ce.bc.clientecobis.adapter;

import static com.bgeneral.ce.bc.clientecobis.common.util.transformation.RestPolicyUtil.buildGsonInterface;

import org.modelmapper.internal.util.Assert;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Repository;

import com.bgeneral.ce.bc.clientecobis.adapter.dto.DireccionClienteCobisDTO;
import com.bgeneral.ce.bc.clientecobis.adapter.dto.ValorDireccionCobisDTO;
import com.bgeneral.ce.bc.clientecobis.adapter.mapper.IAmMapperJsonDTO;
import com.bgeneral.ce.bc.clientecobis.common.dto.BCRequest;
import com.bgeneral.ce.bc.clientecobis.common.dto.BCResponse;
import com.bgeneral.ce.bc.clientecobis.common.util.configuration.ConfigurationAppProperties;
import com.bgeneral.ce.bc.clientecobis.handler.dto.DireccionClienteDTO;
import com.bgeneral.ce.bc.clientecobis.handler.dto.ValorDireccionDTO;
import com.bgeneral.ce.bc.clientecobis.handler.interfaces.IAmRestAdapter;

import feign.Headers;
import feign.RequestLine;

/**
 * Clase adapter que comunica con el bus para guardar los datos completos la
 * direccion del cliente
 * 
 * @author jnieves
 *
 */
@Repository("CrearDireccionClienteCobisAdapter")
public class CrearDireccionClienteCobisAdapter implements IAmRestAdapter<DireccionClienteDTO, ValorDireccionDTO> {

	@Autowired
	private ConfigurationAppProperties configurationAppProperties;

	@Autowired
	@Qualifier("CrearDireccionClienteCobisMapper")
	private IAmMapperJsonDTO<DireccionClienteDTO, ValorDireccionDTO, DireccionClienteCobisDTO, ValorDireccionCobisDTO> mapper;

	/**
	 * @author jnieves
	 *
	 */
	@FunctionalInterface
	interface ICrearDireccionClienteCobis {
		@Headers("Content-Type: Application/json")
		@RequestLine("POST /")
		BCResponse<ValorDireccionCobisDTO> crearDireccionClienteCobis(BCRequest<DireccionClienteCobisDTO> request);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see com.bgeneral.ce.bc.clientecobis.handler.interfaces.IAmRestAdapter#
	 * callRestService(com.bgeneral.ce.bc.clientecobis.common.dto.BCRequest)
	 */
	@Override
	public BCResponse<ValorDireccionDTO> callRestService(BCRequest<DireccionClienteDTO> request) {
		Assert.notNull(request, "BCRequest<CrearCorreoClienteRqDTO>.getBody() NULL");
		Assert.notNull(request.getBody(), "BCRequest<CrearCorreoClienteRqDTO>.getBody() NULL");
		BCRequest<DireccionClienteCobisDTO> bcRequest = this.mapper.handlerRequestTOAdapterRequestDTO(request);

		ICrearDireccionClienteCobis builder = buildGsonInterface(ICrearDireccionClienteCobis.class,
				configurationAppProperties.getRestEndpoint(), "/sen/ce/cliente/creardireccionclientecobis");

		BCResponse<ValorDireccionCobisDTO> response = builder.crearDireccionClienteCobis(bcRequest);
		BCResponse<ValorDireccionDTO> bcResponse = this.mapper.adapterResponseTOHandlerResponseDTO(response);

		Assert.notNull(bcResponse.getStatus().getReturnStatus(), "BCResponse<ValorDireccionDTO>.getStatus() NULL");
		return bcResponse;
	}

}
