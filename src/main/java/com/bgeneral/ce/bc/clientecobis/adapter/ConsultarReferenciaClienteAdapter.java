package com.bgeneral.ce.bc.clientecobis.adapter;

import static com.bgeneral.ce.bc.clientecobis.common.util.transformation.RestPolicyUtil.buildGsonInterface;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Repository;
import org.springframework.util.Assert;

import com.bgeneral.ce.bc.clientecobis.adapter.dto.ReferenciaClienteAdapterDTO;
import com.bgeneral.ce.bc.clientecobis.adapter.mapper.IAmMapperJsonDTO;
import com.bgeneral.ce.bc.clientecobis.common.dto.BCRequest;
import com.bgeneral.ce.bc.clientecobis.common.dto.BCResponse;
import com.bgeneral.ce.bc.clientecobis.common.dto.EmptyBodyT;
import com.bgeneral.ce.bc.clientecobis.common.util.configuration.ConfigurationAppProperties;
import com.bgeneral.ce.bc.clientecobis.handler.dto.ReferenciaClienteDTO;
import com.bgeneral.ce.bc.clientecobis.handler.interfaces.IAmRestAdapter;

import feign.Headers;
import feign.RequestLine;

/**
 * Clase adapter que comunica con el bus para consultarlos datos de referencia
 * de un cliente
 * 
 * @author jnieves
 */
@Repository("ConsultarReferenciaClienteAdapter")
public class ConsultarReferenciaClienteAdapter
		implements IAmRestAdapter<EmptyBodyT, ReferenciaClienteDTO> {

	@Autowired
	private ConfigurationAppProperties configurationAppProperties;

	@Autowired
	@Qualifier("ConsultarReferenciaClienteMapper")
	private IAmMapperJsonDTO<EmptyBodyT, ReferenciaClienteDTO, EmptyBodyT, ReferenciaClienteAdapterDTO> mapper;

	/**
	 * Interfaz Funcional que implementa un metodo de conexion al servicio Rest
	 * Legacy se debe de instanciar la clase con Feign.builder() con el metodo
	 * buildGsonInterface
	 */
	@FunctionalInterface
	interface IAmConsultarReferenciaClienteAdapter {
		@Headers("Content-Type: Application/json")
		@RequestLine("POST /")
		BCResponse<ReferenciaClienteAdapterDTO> consultarReferenciaCliente(BCRequest<EmptyBodyT> request);
	}

	/* (non-Javadoc)
	 * @see com.bgeneral.ce.bc.clientecobis.handler.interfaces.IAmRestAdapter#callRestService(com.bgeneral.ce.bc.clientecobis.common.dto.BCRequest)
	 */
	@Override
	public BCResponse<ReferenciaClienteDTO> callRestService(BCRequest<EmptyBodyT> request) {
		/* Se verifica que el request no este nulo */
		Assert.notNull(request.getHeader().getPerfil(), "BCRequest<EmptyBodyT>.getHeader().getPerfil() NULL");

		/* Se construye request */
		IAmConsultarReferenciaClienteAdapter builder = buildGsonInterface(IAmConsultarReferenciaClienteAdapter.class,
				configurationAppProperties.getRestEndpoint(), "/sen/ce/cliente/consultarreferenciacliente");

		BCResponse<ReferenciaClienteAdapterDTO> response = builder.consultarReferenciaCliente(request);
		BCResponse<ReferenciaClienteDTO> bcResponse = this.mapper
				.adapterResponseTOHandlerResponseDTO(response);

		/* Se verifica que el response no este nulo */
		Assert.notNull(bcResponse.getStatus(), "BCResponse<ConsultarReferenciaClienteHandlerDTO>.getStatus() NULL");

		return bcResponse;
	}

}
