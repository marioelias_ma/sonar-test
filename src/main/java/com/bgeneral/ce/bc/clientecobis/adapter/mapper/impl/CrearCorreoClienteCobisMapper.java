package com.bgeneral.ce.bc.clientecobis.adapter.mapper.impl;

import org.modelmapper.ModelMapper;
import org.springframework.stereotype.Component;

import com.bgeneral.ce.bc.clientecobis.adapter.dto.CrearCorreoClienteCobisRespDTO;
import com.bgeneral.ce.bc.clientecobis.adapter.dto.CrearCorreoClienteCobisRqDTO;
import com.bgeneral.ce.bc.clientecobis.adapter.mapper.IAmMapperJsonDTO;
import com.bgeneral.ce.bc.clientecobis.common.dto.BCRequest;
import com.bgeneral.ce.bc.clientecobis.common.dto.BCResponse;
import com.bgeneral.ce.bc.clientecobis.common.util.catalogs.CodeCatalog;
import com.bgeneral.ce.bc.clientecobis.handler.dto.CuentaCorreaClienteDTO;
import com.bgeneral.ce.bc.clientecobis.handler.dto.ValorCorreoDTO;

/**
 * @author A. Lugo
 * 
 * Clase Mapper para el adapter CrearCorreoclienteCobisAdapter
 *
 */

@Component("CrearCorreoclienteCobisMapper")
public class CrearCorreoClienteCobisMapper 
implements IAmMapperJsonDTO <CuentaCorreaClienteDTO, ValorCorreoDTO,CrearCorreoClienteCobisRqDTO,CrearCorreoClienteCobisRespDTO>{

	@Override
	public BCRequest<CrearCorreoClienteCobisRqDTO> handlerRequestTOAdapterRequestDTO(
		   BCRequest<CuentaCorreaClienteDTO> request) {
		BCRequest<CrearCorreoClienteCobisRqDTO> bcRequest = new BCRequest<>();
		bcRequest.setHeader(request.getHeader());
        bcRequest.setBody(new ModelMapper().map(request.getBody(), CrearCorreoClienteCobisRqDTO.class));
		
		return bcRequest;
	}

	@Override
	public BCResponse<ValorCorreoDTO> adapterResponseTOHandlerResponseDTO(
			BCResponse<CrearCorreoClienteCobisRespDTO> response) {
		BCResponse<ValorCorreoDTO> bcResponse = new BCResponse<>();
		bcResponse.setHeader(response.getHeader());
		
		if(CodeCatalog.CODE_SUCCESS.codeS().equals(response.getStatus().getReturnStatus().getReturnCode())) {
			ValorCorreoDTO correoclienteCobisRespDTO = new ValorCorreoDTO();
			correoclienteCobisRespDTO.setCodigoCorreo(response.getBody().getCodigoCorreo());
			bcResponse.setBody(correoclienteCobisRespDTO);
		}
		bcResponse.setStatus(response.getStatus());
		return bcResponse;
	}

}
