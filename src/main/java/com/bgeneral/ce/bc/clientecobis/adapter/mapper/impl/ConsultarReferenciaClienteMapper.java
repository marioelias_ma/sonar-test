package com.bgeneral.ce.bc.clientecobis.adapter.mapper.impl;

import org.springframework.stereotype.Component;

import com.bgeneral.ce.bc.clientecobis.adapter.dto.ReferenciaClienteAdapterDTO;
import com.bgeneral.ce.bc.clientecobis.adapter.mapper.IAmMapperJsonDTO;
import com.bgeneral.ce.bc.clientecobis.common.dto.BCRequest;
import com.bgeneral.ce.bc.clientecobis.common.dto.BCResponse;
import com.bgeneral.ce.bc.clientecobis.common.dto.EmptyBodyT;
import com.bgeneral.ce.bc.clientecobis.common.util.catalogs.CodeCatalog;
import com.bgeneral.ce.bc.clientecobis.handler.dto.ReferenciaClienteDTO;

/**
 * Clase Mapper para el Request y Response del @Component("ConsultarRerenciaClienteMapper
 * 
 * @author jolivero
 */
@Component("ConsultarReferenciaClienteMapper")
public class ConsultarReferenciaClienteMapper implements IAmMapperJsonDTO<EmptyBodyT, ReferenciaClienteDTO, EmptyBodyT, ReferenciaClienteAdapterDTO> {

	@Override
	public BCRequest<EmptyBodyT> handlerRequestTOAdapterRequestDTO(BCRequest<EmptyBodyT> request) {
		return null;
	}

	@Override
	public BCResponse<ReferenciaClienteDTO> adapterResponseTOHandlerResponseDTO(
			BCResponse<ReferenciaClienteAdapterDTO> response) {
		BCResponse<ReferenciaClienteDTO> bcResponse = new BCResponse<>();
		bcResponse.setHeader(response.getHeader());
		bcResponse.setStatus(response.getStatus());
		
		if(CodeCatalog.CODE_SUCCESS.codeS().equals(response.getStatus().getReturnStatus().getReturnCode())) {
			ReferenciaClienteDTO body = new ReferenciaClienteDTO();
			/* Se valida que tenga al menos registro*/
			body.setTieneReferencia((response.getBody().getNumeroRegistros()>0));
			bcResponse.setBody(body);
		}
		
		return bcResponse;
	}
	
	
	
	

}
