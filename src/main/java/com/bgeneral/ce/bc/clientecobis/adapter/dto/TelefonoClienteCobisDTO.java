package com.bgeneral.ce.bc.clientecobis.adapter.dto;

import java.io.Serializable;

import com.bgeneral.ce.bc.clientecobis.common.dto.Catalogo;
import com.fasterxml.jackson.annotation.JsonProperty;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class TelefonoClienteCobisDTO implements Serializable {
	
	/**
	 * 
	 */
	private static final long serialVersionUID = -7672253621703949677L;

	@JsonProperty("numeroTelefono")
	private String numeroTelefono;
	
	@JsonProperty("direccion")
	private Catalogo direccion;
	
	@JsonProperty("tipoTelefono")
    private String tipoTelefono;
	
	@JsonProperty("extencion")
    private String extencion;
	
	@JsonProperty("pais")
    private Catalogo pais;
}
