package com.bgeneral.ce.bc.clientecobis.adapter.dto;

import java.io.Serializable;
import java.util.List;

import com.fasterxml.jackson.annotation.JsonProperty;

import lombok.Getter;
import lombok.Setter;

/**
 * 
 * @author jolivero
 *
 */
@Getter
@Setter
public class ReferenciaClienteAdapterDTO implements Serializable{

	/**
	 * 
	 */
	private static final long serialVersionUID = -8992590422999093013L;
	
	@JsonProperty("numeroRegistros")
	private Integer numeroRegistros;
	
	@JsonProperty("cliente")
	private List<DatosReferenciaCliente> cliente;

	
}
