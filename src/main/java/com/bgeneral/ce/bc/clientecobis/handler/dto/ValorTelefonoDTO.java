package com.bgeneral.ce.bc.clientecobis.handler.dto;

import java.io.Serializable;

import com.fasterxml.jackson.annotation.JsonProperty;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class ValorTelefonoDTO implements Serializable {
	
	/**
	 * 
	 */
	private static final long serialVersionUID = -3749131736458924852L;
	
	@JsonProperty("codigoTelefono")
	private String codigoTelefono;
}
