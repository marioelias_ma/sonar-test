package com.bgeneral.ce.bc.clientecobis.common.dto;

import java.io.Serializable;

import org.springframework.stereotype.Component;

import com.fasterxml.jackson.annotation.JsonProperty;

import lombok.*;

/**
 * @author jnieves, brodriguez
 *
 * @param <T>
 */
@Getter
@Setter
@Component
public class BCResponse<T extends Serializable> implements Serializable {
	/**
	 * s
	 */
	private static final long serialVersionUID = 1L;

	@JsonProperty("header")
	private Header header;

	@JsonProperty("status")
	private Status status;

	@JsonProperty("body")
	private T body;
}
