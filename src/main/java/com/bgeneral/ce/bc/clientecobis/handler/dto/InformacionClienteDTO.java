package com.bgeneral.ce.bc.clientecobis.handler.dto;

import java.io.Serializable;
import java.util.Date;

import com.bgeneral.ce.bc.clientecobis.common.dto.Catalogo;
import com.bgeneral.ce.bc.clientecobis.common.util.transformation.CustomDateSerializer;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class InformacionClienteDTO implements Serializable{
	/**
	 * 
	 */
	private static final long serialVersionUID = 2405172041950251807L;
	
	@JsonProperty("primerNombre")
	private String primerNombre;
	
	@JsonProperty("primerApellido")
	private String primerApellido;
	
	@JsonProperty("cedula")
	private String cedula;
	
	@JsonProperty("fechaNacimiento")
	@JsonSerialize(using = CustomDateSerializer.class)
	private Date fechaNacimiento;
	
	@JsonProperty("sexo")
	private Catalogo sexo;
	
	@JsonProperty("estadoCivil")
	private Catalogo estadoCivil;
	
	@JsonProperty("paisNacionalidad")
	private Catalogo paisNacionalidad;
	
	@JsonProperty("paisNacimiento")
	private Catalogo paisNacimiento;
	
	@JsonProperty("paisRiesgo")
	private Catalogo paisRiesgo;
}
