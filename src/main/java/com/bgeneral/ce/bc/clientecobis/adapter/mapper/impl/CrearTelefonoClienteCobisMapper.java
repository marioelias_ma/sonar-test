package com.bgeneral.ce.bc.clientecobis.adapter.mapper.impl;

import org.modelmapper.ModelMapper;
import org.springframework.stereotype.Component;

import com.bgeneral.ce.bc.clientecobis.adapter.dto.TelefonoClienteCobisDTO;
import com.bgeneral.ce.bc.clientecobis.adapter.dto.ValorTelefonoCobisDTO;
import com.bgeneral.ce.bc.clientecobis.adapter.mapper.IAmMapperJsonDTO;
import com.bgeneral.ce.bc.clientecobis.common.dto.BCRequest;
import com.bgeneral.ce.bc.clientecobis.common.dto.BCResponse;
import com.bgeneral.ce.bc.clientecobis.common.util.catalogs.CodeCatalog;
import com.bgeneral.ce.bc.clientecobis.handler.dto.TelefonoClienteDTO;
import com.bgeneral.ce.bc.clientecobis.handler.dto.ValorDireccionDTO;
import com.bgeneral.ce.bc.clientecobis.handler.dto.ValorTelefonoDTO;

/**
 * Clase Mapper para el Request y Response de la clase de
 * CrearTelefonoClienteCobisAdapter
 * 
 * @author jnieves
 *
 */
@Component("CrearTelefonoClienteCobisMapper")
public class CrearTelefonoClienteCobisMapper implements
		IAmMapperJsonDTO<TelefonoClienteDTO, ValorTelefonoDTO, TelefonoClienteCobisDTO, ValorTelefonoCobisDTO> {

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * com.bgeneral.ce.bc.clientecobis.adapter.mapper.interfaces.IAmMapperJsonDTO#
	 * handlerRequestTOAdapterRequestDTO(com.bgeneral.ce.bc.clientecobis.common.dto.
	 * BCRequest)
	 */
	@Override
	public BCRequest<TelefonoClienteCobisDTO> handlerRequestTOAdapterRequestDTO(
			BCRequest<TelefonoClienteDTO> request) {
		BCRequest<TelefonoClienteCobisDTO> bcRequest = new BCRequest<>();
		bcRequest.setBody(new ModelMapper().map(request.getBody(), TelefonoClienteCobisDTO.class));

		bcRequest.setHeader(request.getHeader());
		return bcRequest;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * com.bgeneral.ce.bc.clientecobis.adapter.mapper.interfaces.IAmMapperJsonDTO#
	 * adapterResponseTOHandlerResponseDTO(com.bgeneral.ce.bc.clientecobis.common.
	 * dto.BCResponse)
	 */
	@Override
	public BCResponse<ValorTelefonoDTO> adapterResponseTOHandlerResponseDTO(
			BCResponse<ValorTelefonoCobisDTO> response) {
		BCResponse<ValorTelefonoDTO> bcResponse = new BCResponse<>();
		
		if(CodeCatalog.CODE_SUCCESS.codeS().equals(response.getStatus().getReturnStatus().getReturnCode())) {
			ValorTelefonoDTO body = new ValorTelefonoDTO();
			body.setCodigoTelefono(response.getBody().getCodigoTelefono());
			bcResponse.setBody(body);
		}
		
		bcResponse.setHeader(response.getHeader());
		bcResponse.setStatus(response.getStatus());
		return bcResponse;
	}

}
