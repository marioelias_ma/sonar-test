package com.bgeneral.ce.bc.clientecobis.handler.interfaces;

import com.bgeneral.ce.bc.clientecobis.controller.interfaces.IAmCommand;

public interface IAmFactory{
	void callAdapter(IAmCommand<?, ?> command);
}
