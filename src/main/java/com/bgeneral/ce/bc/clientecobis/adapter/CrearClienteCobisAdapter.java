package com.bgeneral.ce.bc.clientecobis.adapter;

import static com.bgeneral.ce.bc.clientecobis.common.util.transformation.RestPolicyUtil.buildGsonInterface;

import org.modelmapper.internal.util.Assert;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Repository;

import com.bgeneral.ce.bc.clientecobis.adapter.dto.ClienteCobisDTO;
import com.bgeneral.ce.bc.clientecobis.adapter.dto.ValorClienteCobisDTO;
import com.bgeneral.ce.bc.clientecobis.adapter.mapper.IAmMapperJsonDTO;
import com.bgeneral.ce.bc.clientecobis.common.dto.BCRequest;
import com.bgeneral.ce.bc.clientecobis.common.dto.BCResponse;
import com.bgeneral.ce.bc.clientecobis.common.util.configuration.ConfigurationAppProperties;
import com.bgeneral.ce.bc.clientecobis.handler.dto.EnteClienteDTO;
import com.bgeneral.ce.bc.clientecobis.handler.dto.InformacionClienteDTO;
import com.bgeneral.ce.bc.clientecobis.handler.interfaces.IAmRestAdapter;

import feign.Headers;
import feign.RequestLine;

/**
 * Clase Adapter la cual crea el cliente en cobis.
 * 
 * @author F. Castillo
 * 
 */
@Repository("CrearClienteCobisAdapter")
public class CrearClienteCobisAdapter implements IAmRestAdapter<InformacionClienteDTO, EnteClienteDTO> {
	
	@Autowired
	private ConfigurationAppProperties configurationAppProperties;

	@Autowired
	@Qualifier("CrearClienteCobisMapper")
	private IAmMapperJsonDTO<InformacionClienteDTO, EnteClienteDTO, ClienteCobisDTO, ValorClienteCobisDTO> mapper;
	
	/**
	 * @author jnieves
	 * Interfaz Funcional que implementa un metodo de conexion al servicio Rest Legacy
	 * Se debe de instanciar la clase con Feign.builder() con el metodo buildGsonInterface
	 */
	@FunctionalInterface
	interface ICrearClienteCobis {
		@Headers("Content-Type: Application/json")
		@RequestLine("POST /")
		BCResponse<ValorClienteCobisDTO> crearClienteCobis(
				BCRequest<ClienteCobisDTO> request);
	}
	@Override
	public BCResponse<EnteClienteDTO> callRestService(BCRequest<InformacionClienteDTO> request) {
		Assert.notNull(request.getBody(), "BCRequest<CrearClienteCobisRqDTO>.getBody() NULL");
		BCRequest<ClienteCobisDTO> bcRequest = this.mapper.handlerRequestTOAdapterRequestDTO(request);
		
		ICrearClienteCobis builder = buildGsonInterface(ICrearClienteCobis.class, 
				configurationAppProperties.getRestEndpoint(), "/sen/ce/cliente/crearclientecobis");
		
		BCResponse<ValorClienteCobisDTO> response = builder.crearClienteCobis(bcRequest);
		BCResponse<EnteClienteDTO> bcResponse = this.mapper.adapterResponseTOHandlerResponseDTO(response);
		
		Assert.notNull(bcResponse.getStatus(), "BCResponse<ClienteCobisRespDTO>.getStatus() NULL");
		
		// TODO Auto-generated method stub
		return bcResponse;
	}

}
