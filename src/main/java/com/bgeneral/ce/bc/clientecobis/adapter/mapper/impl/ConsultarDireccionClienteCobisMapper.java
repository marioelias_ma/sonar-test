package com.bgeneral.ce.bc.clientecobis.adapter.mapper.impl;

import org.springframework.stereotype.Component;

import com.bgeneral.ce.bc.clientecobis.adapter.dto.DatosDireccionClienteCobisDTO;
import com.bgeneral.ce.bc.clientecobis.adapter.mapper.IAmMapperJsonDTO;
import com.bgeneral.ce.bc.clientecobis.common.dto.BCRequest;
import com.bgeneral.ce.bc.clientecobis.common.dto.BCResponse;
import com.bgeneral.ce.bc.clientecobis.common.dto.EmptyBodyT;
import com.bgeneral.ce.bc.clientecobis.common.util.catalogs.CodeCatalog;
import com.bgeneral.ce.bc.clientecobis.handler.dto.DatosDireccionClienteCobisHandlerDTO;

/**
 * Clase Mapper para el Request y Response de la clase de
 * ConsultarDireccionClienteCobisAdapter
 * 
 * @author jnieves
 */
@Component("ConsultarDireccionClienteCobisMapper")
public class ConsultarDireccionClienteCobisMapper implements IAmMapperJsonDTO<EmptyBodyT, DatosDireccionClienteCobisHandlerDTO, EmptyBodyT, DatosDireccionClienteCobisDTO> {

	@Override
	public BCRequest<EmptyBodyT> handlerRequestTOAdapterRequestDTO(BCRequest<EmptyBodyT> request) {
		return null;
	}

	@Override
	public BCResponse<DatosDireccionClienteCobisHandlerDTO> adapterResponseTOHandlerResponseDTO(
			BCResponse<DatosDireccionClienteCobisDTO> response) {
		
		BCResponse<DatosDireccionClienteCobisHandlerDTO> bcResponse = new BCResponse<>();
		bcResponse.setHeader(response.getHeader());
		bcResponse.setStatus(response.getStatus());
		
		if(CodeCatalog.CODE_SUCCESS.codeS().equals(response.getStatus().getReturnStatus().getReturnCode())) {
			DatosDireccionClienteCobisHandlerDTO body = new DatosDireccionClienteCobisHandlerDTO();
			
			body.setBarrio(response.getBody().getBarrio());
			body.setCalle(response.getBody().getCalle());
			body.setCiudad(response.getBody().getCiudad());
			body.setCorregimiento(response.getBody().getCorregimiento());
			body.setDireccion(response.getBody().getDireccion());
			body.setEdificioCasa(response.getBody().getEdificioCasa());
			body.setEsPrincipal(response.getBody().getEsPrincipal());
			body.setEsVerificado(response.getBody().getEsVerificado());
			body.setEsVigente(response.getBody().getEsVigente());
			body.setFechaModificacion(response.getBody().getFechaModificacion());
			body.setFechaRegistro(response.getBody().getFechaRegistro());
			body.setFechaVerificacion(response.getBody().getFechaVerificacion());
			body.setFuncionario(response.getBody().getFuncionario());
			body.setOficina(response.getBody().getOficina());
			body.setPais(response.getBody().getPais());
			body.setSector(response.getBody().getSector());
			body.setTipoDireccion(response.getBody().getTipoDireccion());
			body.setZona(response.getBody().getZona());
			
			bcResponse.setBody(body);
		}
		
		return bcResponse;
	}

}
