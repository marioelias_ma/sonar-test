package com.bgeneral.ce.bc.clientecobis.adapter;

import static com.bgeneral.ce.bc.clientecobis.common.util.transformation.RestPolicyUtil.buildGsonInterface;

import org.modelmapper.internal.util.Assert;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Repository;

import com.bgeneral.ce.bc.clientecobis.adapter.dto.CrearCorreoClienteCobisRespDTO;
import com.bgeneral.ce.bc.clientecobis.adapter.dto.CrearCorreoClienteCobisRqDTO;
import com.bgeneral.ce.bc.clientecobis.adapter.mapper.IAmMapperJsonDTO;
import com.bgeneral.ce.bc.clientecobis.common.dto.BCRequest;
import com.bgeneral.ce.bc.clientecobis.common.dto.BCResponse;
import com.bgeneral.ce.bc.clientecobis.common.util.configuration.ConfigurationAppProperties;
import com.bgeneral.ce.bc.clientecobis.handler.dto.CuentaCorreaClienteDTO;
import com.bgeneral.ce.bc.clientecobis.handler.dto.ValorCorreoDTO;
import com.bgeneral.ce.bc.clientecobis.handler.interfaces.IAmRestAdapter;

import feign.Headers;
import feign.RequestLine;

/**
 * Clase Adapter que crea el correo cobis del cliente
 * 
 * @author A. Lugo
 */
@Repository("CrearCorreoClienteCobisAdapter")
public class CrearCorreoClienteCobisAdapter implements IAmRestAdapter<CuentaCorreaClienteDTO,ValorCorreoDTO> {

	@Autowired
	private ConfigurationAppProperties configurationAppProperties;

	@Autowired
	@Qualifier("CrearCorreoclienteCobisMapper")
	private IAmMapperJsonDTO<CuentaCorreaClienteDTO, ValorCorreoDTO,CrearCorreoClienteCobisRqDTO,CrearCorreoClienteCobisRespDTO> mapper;
	
	@FunctionalInterface
	interface ICrearCorrreoClienteCobis {
		@Headers("Content-Type: Application/json")
		@RequestLine("POST /")
		BCResponse<CrearCorreoClienteCobisRespDTO> crearCorreoClienteCobis(
				BCRequest<CrearCorreoClienteCobisRqDTO> request);
	}
	
	@Override
	public BCResponse<ValorCorreoDTO> callRestService(BCRequest<CuentaCorreaClienteDTO> request) {
		Assert.notNull(request, "BCRequest<CrearCorreoClienteRqDTO>.getBody() NULL");
		Assert.notNull(request.getBody(), "BCRequest<CrearCorreoClienteRqDTO>.getBody() NULL");
		BCRequest<CrearCorreoClienteCobisRqDTO> bcRequest = this.mapper.handlerRequestTOAdapterRequestDTO(request);
		
		ICrearCorrreoClienteCobis builder = buildGsonInterface(ICrearCorrreoClienteCobis.class,
				configurationAppProperties.getRestEndpoint(),"/sen/ce/cliente/crearcorreoclientecobis");
		
		BCResponse<CrearCorreoClienteCobisRespDTO> response = builder.crearCorreoClienteCobis(bcRequest);
		BCResponse<ValorCorreoDTO> bcResponse = this.mapper.adapterResponseTOHandlerResponseDTO(response);
		
		Assert.notNull(bcResponse.getStatus(), "BCResponse<CrearCorreoClienteCobisRespDTO>.getStatus() NULL");
		return bcResponse;
	}

}
