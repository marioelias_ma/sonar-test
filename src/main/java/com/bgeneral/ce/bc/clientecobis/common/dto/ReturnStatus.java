package com.bgeneral.ce.bc.clientecobis.common.dto;

import java.io.Serializable;

import com.fasterxml.jackson.annotation.JsonProperty;

import lombok.*;

/**
 * @author jnieves, brodriguez
 *
 */
@Getter
@Setter
public class ReturnStatus implements Serializable{
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	@JsonProperty("returnCode")
	private String returnCode;

	@JsonProperty("returnCodeDesc")
	private String returnCodeDesc;
}
