package com.bgeneral.ce.bc.clientecobis.handler.command;

import com.bgeneral.ce.bc.clientecobis.common.dto.BCRequest;
import com.bgeneral.ce.bc.clientecobis.common.dto.EmptyBodyT;
import com.bgeneral.ce.bc.clientecobis.controller.dto.DatosDireccionClienteCobisDTO;
import com.bgeneral.ce.bc.clientecobis.controller.interfaces.IAmCommand;

import lombok.Getter;

@Getter
public class ConsultarDireccionClienteCommand implements IAmCommand<EmptyBodyT, DatosDireccionClienteCobisDTO>{
	
	private BCRequest<EmptyBodyT> dto;

	public ConsultarDireccionClienteCommand(BCRequest<EmptyBodyT> request) {
		this.dto = request;
	}

}
