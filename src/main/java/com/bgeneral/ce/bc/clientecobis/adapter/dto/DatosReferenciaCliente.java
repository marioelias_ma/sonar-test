package com.bgeneral.ce.bc.clientecobis.adapter.dto;

import java.io.Serializable;

import com.bgeneral.ce.bc.clientecobis.common.dto.Catalogo;
import com.fasterxml.jackson.annotation.JsonProperty;

import lombok.Getter;
import lombok.Setter;

/**
 * 
 * @author jolivero
 *
 */

@Getter
@Setter
public class DatosReferenciaCliente implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 7869922407358955327L;

	@JsonProperty("numeroCliente")
	private Integer numeroCliente;
	
	@JsonProperty("nombreCompleto")
	private String nombreCompleto;
	
	@JsonProperty("secuencia")
	private Integer secuencia;
	
	@JsonProperty("referencia")
	private Integer referencia;
	
	@JsonProperty("tipoReferencia")
	private Catalogo tipoReferencia;
	
	@JsonProperty("motivo")
	private Catalogo motivo;
	
	@JsonProperty("observacion")
	private String observacion;
	
	@JsonProperty("calificacion")
	private Catalogo calificacion;
	
	@JsonProperty("cuenta")
	private String cuenta;
	
	@JsonProperty("fechaInicio")
	private String fechaInicio;
	
	@JsonProperty("fechaVencimiento")
	private String fechaVencimiento;
	
	@JsonProperty("fechaCierre")
	private String fechaCierre;
	
	@JsonProperty("banco")
	private Catalogo banco;
	
	@JsonProperty("fechaRegistro")
	private String fechaRegistro;

	@JsonProperty("fechaModificacion")
	private String fechaModificacion;
	
	@JsonProperty("usuarioLogin")
	private String usuarioNombre;

}
