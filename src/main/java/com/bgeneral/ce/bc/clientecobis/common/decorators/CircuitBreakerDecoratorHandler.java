package com.bgeneral.ce.bc.clientecobis.common.decorators;

import java.io.Serializable;

import com.bgeneral.ce.bc.clientecobis.common.dto.BCResponse;
import com.bgeneral.ce.bc.clientecobis.controller.interfaces.IAmCommand;
import com.bgeneral.ce.bc.clientecobis.controller.interfaces.IAmCommandHandler;
import com.netflix.hystrix.HystrixCommand;
import com.netflix.hystrix.HystrixCommand.Setter;
import com.netflix.hystrix.HystrixCommandGroupKey;


/**
 * @author jnieves
 *
 * @param <C> TCommand
 * @param <I> TRequest
 * @param <O> TResponse
 */
public class CircuitBreakerDecoratorHandler<C extends IAmCommand<I, O>, I extends Serializable, O extends Serializable>
		implements IAmCommandHandler<C, I, O> {
	private final IAmCommandHandler<C, I, O> innerHandler;

	/**
	 * @param innerHandler
	 */
	public CircuitBreakerDecoratorHandler(IAmCommandHandler<C, I, O> innerHandler) {
		this.innerHandler = innerHandler;
	}

	// TODO: definir de donde sacar las configuraciones del CB
	/* (non-Javadoc)
	 * @see com.bgeneral.ce.bc.tokennumerico.infrastructure.IAmCommandHandler#handle(com.bgeneral.ce.bc.tokennumerico.infrastructure.IAmCommand)
	 */
    @Override
	public BCResponse<O> handle(C command) {
		String key = innerHandler.getClass().getName() + "-" + command.getClass().getName();
		Setter setter = Setter.withGroupKey(HystrixCommandGroupKey.Factory.asKey(key));
		return new CircuitBreakerHandler(setter, innerHandler, command).execute();
	}

	/**
	 * @author jnieves, brodriguez
	 *
	 */
	private class CircuitBreakerHandler extends HystrixCommand<BCResponse<O>> {
		private final IAmCommandHandler<C, I, O> innerHandler;
		private final C command;

		/**
		 * @param setter
		 * @param innerHandler
		 * @param command
		 */
		protected CircuitBreakerHandler(Setter setter, IAmCommandHandler<C, I, O> innerHandler,
				C command) {
			super(setter);
			this.innerHandler = innerHandler; // todas la excepciones esperadas de logica de negocio se controlan dentro
												// del comando
			this.command = command;
		}

		/*
		 * (non-Javadoc)
		 * 
		 * @see com.netflix.hystrix.HystrixCommand#run()
		 */
		@Override
		protected BCResponse<O> run(){
			return this.innerHandler.handle(this.command);
		}

	}

}
