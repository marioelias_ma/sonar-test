package com.bgeneral.ce.bc.clientecobis.handler.command.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Service;

import com.bgeneral.ce.bc.clientecobis.common.dto.BCRequest;
import com.bgeneral.ce.bc.clientecobis.common.dto.BCResponse;
import com.bgeneral.ce.bc.clientecobis.common.dto.EmptyBodyT;
import com.bgeneral.ce.bc.clientecobis.common.dto.Header;
import com.bgeneral.ce.bc.clientecobis.common.dto.ReturnStatus;
import com.bgeneral.ce.bc.clientecobis.common.dto.Status;
import com.bgeneral.ce.bc.clientecobis.common.util.catalogs.CodeCatalog;
import com.bgeneral.ce.bc.clientecobis.controller.dto.CredencialesClienteDTO;
import com.bgeneral.ce.bc.clientecobis.controller.dto.DatosClienteDTO;
import com.bgeneral.ce.bc.clientecobis.controller.interfaces.IAmCommandHandler;
import com.bgeneral.ce.bc.clientecobis.handler.command.ConsultarDatosClienteCommand;
import com.bgeneral.ce.bc.clientecobis.handler.dto.ClientePEPDTO;
import com.bgeneral.ce.bc.clientecobis.handler.dto.DatosClienteHandlerDTO;
import com.bgeneral.ce.bc.clientecobis.handler.dto.ReferenciaClienteDTO;
import com.bgeneral.ce.bc.clientecobis.handler.interfaces.IAmRestAdapter;

/**
 * Clase Command Hamdler que contiene la comunicacion con los flujos de negocios
 * 
 * @author Ingrid, jnieves
 */
@Service("ConsultarDatosClienteCommandHandler")
public class ConsultarDatosClienteCommandHandler
		implements IAmCommandHandler<ConsultarDatosClienteCommand, CredencialesClienteDTO, DatosClienteDTO> {

	@Autowired
	@Qualifier("ConsultarDatosClienteCobisAdapter")
	private IAmRestAdapter<CredencialesClienteDTO, DatosClienteHandlerDTO> consultarDatosClienteCobisAdapter;

	@Autowired
	@Qualifier("ConsultarClientePEPAdapter")
	private IAmRestAdapter<EmptyBodyT, ClientePEPDTO> consultarClientePEPAdapter;

	@Autowired
	@Qualifier("ConsultarReferenciaClienteAdapter")
	private IAmRestAdapter<EmptyBodyT, ReferenciaClienteDTO> consultarReferenciaClienteAdapter;

	/*
	 * (non-Javadoc)
	 * 
	 * @see com.bgeneral.ce.bc.clientecobis.controller.interfaces.IAmCommandHandler#
	 * handle(com.bgeneral.ce.bc.clientecobis.controller.interfaces.IAmCommand)
	 */
	@Override
	public BCResponse<DatosClienteDTO> handle(ConsultarDatosClienteCommand command) {
		BCRequest<CredencialesClienteDTO> request = command.getDto();
		BCResponse<DatosClienteDTO> bcResponse = new BCResponse<>();
		bcResponse.setHeader(command.getDto().getHeader());
		bcResponse.setStatus(new Status());
		try {
			BCResponse<DatosClienteHandlerDTO> response = consultarDatosClienteCobisAdapter.callRestService(request);
			bcResponse.setStatus(response.getStatus());

			if (CodeCatalog.CODE_SUCCESS.codeS().equals(response.getStatus().getReturnStatus().getReturnCode())) {
				DatosClienteDTO body = getDatosClienteDTO(response);
				bcResponse.setBody(body);
				bcResponse.getBody().setClienteEsPep(callAdapterConsultarClientePEP(command.getDto().getHeader()));
				bcResponse.getBody()
						.setClienteTienePantallazo(callAdapterConsultarReferenciaCliente(command.getDto().getHeader()));
			}

		} catch (Exception e) {
			bcResponse.getStatus().setReturnStatus(this.returnStatus(CodeCatalog.CODE_ERROR.codeS(), e.getMessage()));
		}

		return bcResponse;
	}

	/**
	 * @param header
	 * @return
	 */
	private boolean callAdapterConsultarClientePEP(Header header) {
		BCRequest<EmptyBodyT> requestClientePEP = new BCRequest<>();
		requestClientePEP.setHeader(header);
		ClientePEPDTO clientePEPDTO = consultarClientePEPAdapter.callRestService(requestClientePEP).getBody();
		if(clientePEPDTO != null) {
			return clientePEPDTO.isEsPEP();
		}
		return false;
	}

	/**
	 * @param header
	 * @return
	 */
	private boolean callAdapterConsultarReferenciaCliente(Header header) {
		BCRequest<EmptyBodyT> requestClientePEP = new BCRequest<>();
		requestClientePEP.setHeader(header);
		ReferenciaClienteDTO clientePEPDTO = consultarReferenciaClienteAdapter.callRestService(requestClientePEP).getBody();
		if(clientePEPDTO != null) {
			return clientePEPDTO.isTieneReferencia();
		}
		return false;
	}

	/**
	 * @param response
	 * @return
	 */
	private DatosClienteDTO getDatosClienteDTO(BCResponse<DatosClienteHandlerDTO> response) {
		DatosClienteDTO body = new DatosClienteDTO();
		body.setActividad(response.getBody().getActividad());
		body.setApellidoCasada(response.getBody().getApellidoCasada());
		body.setNumeroCliente(response.getBody().getNumeroCliente());
		body.setApellidoPaterno(response.getBody().getApellidoPaterno());
		body.setApellidoMaterno(response.getBody().getApellidoMaterno());
		body.setNumeroCliente(response.getBody().getNumeroCliente());
		body.setApellidoPaterno(response.getBody().getApellidoPaterno());
		body.setApellidoMaterno(response.getBody().getApellidoMaterno());
		body.setApellidoCasada(response.getBody().getApellidoCasada());
		body.setNombre(response.getBody().getNombre());
		body.setSegundonombre(response.getBody().getSegundonombre());
		body.setNombreCompleto(response.getBody().getNombreCompleto());
		body.setCedula(response.getBody().getCedula());
		body.setPasaporte(response.getBody().getPasaporte());
		body.setSegurosocial(response.getBody().getSegurosocial());
		body.setFechaNacimiento(response.getBody().getFechaNacimiento());
		body.setEdad(response.getBody().getEdad());
		body.setSexo(response.getBody().getSexo());
		body.setPais(response.getBody().getPais());
		body.setNacionalidad(response.getBody().getNacionalidad());
		body.setPais_2(response.getBody().getPais_2());
		body.setNacionalidad_2(response.getBody().getNacionalidad_2());
		body.setEducacion(response.getBody().getEducacion());
		body.setEstadoCivil(response.getBody().getEstadoCivil());
		body.setTipoPersona(response.getBody().getTipoPersona());
		body.setProfesion(response.getBody().getProfesion());
		body.setPaisRiesgo(response.getBody().getPaisRiesgo());
		body.setCodigoOficial(response.getBody().getCodigoOficial());
		body.setOficial(response.getBody().getOficial());
		body.setDescripcionOficial(response.getBody().getDescripcionOficial());
		body.setUsuario(response.getBody().getUsuario());
		body.setFechaRegistro(response.getBody().getFechaRegistro());
		body.setFechaUltimaModificacion(response.getBody().getFechaUltimaModificacion());
		body.setFechaIngreso(response.getBody().getFechaIngreso());
		body.setFechaExp(response.getBody().getFechaExp());
		body.setGrupo(response.getBody().getGrupo());
		body.setRetencion(response.getBody().getRetencion());
		body.setCodigoMalaRef(response.getBody().getCodigoMalaRef());
		body.setCodigoCalificacion(response.getBody().getCodigoCalificacion());
		body.setComentario(response.getBody().getComentario());
		body.setGrupoRel(response.getBody().getGrupoRel());
		body.setEstatus(response.getBody().getEstatus());
		body.setFechaVerificacion(response.getBody().getFechaVerificacion());
		body.setDifunto(response.getBody().getDifunto());
		body.setVersionCsba(response.getBody().getVersionCsba());
		body.setAutorizacionAPC(response.getBody().getAutorizacionAPC());
		body.setNecesitaRevision(response.getBody().getNecesitaRevision());
		body.setReferenciaInterna(response.getBody().getReferenciaInterna());
		body.setClasificacion(response.getBody().getClasificacion());
		body.setSubClasificacion(response.getBody().getSubClasificacion());
		body.setRelacion(response.getBody().getRelacion());
		body.setFechaInicioRelacion(response.getBody().getFechaInicioRelacion());
		body.setOficialCodigo(response.getBody().getOficialCodigo());
		body.setOficialSector(response.getBody().getOficialSector());
		body.setOficialSubSector(response.getBody().getOficialSubSector());
		body.setOficialTipo(response.getBody().getOficialTipo());
		body.setUsuarioRed(response.getBody().getUsuarioRed());
		body.setClienteBanca(response.getBody().getClienteBanca());
		body.setPaisNacimiento(response.getBody().getPaisNacimiento());
		body.setCantidadCuentasPasivas(response.getBody().getCantidadCuentasPasivas());
		body.setEvaluacionCumplimiento(response.getBody().getEvaluacionCumplimiento());
		body.setClienteEsPep(response.getBody().getClienteEsPep());
		body.setClienteTienePantallazo(response.getBody().getClienteTienePantallazo());
		return body;
	}

	/**
	 * @param e
	 * @return
	 */
	private ReturnStatus returnStatus(String returnCode, String returnCodeDesc) {
		ReturnStatus returnStatus = new ReturnStatus();
		returnStatus.setReturnCode(returnCode);
		returnStatus.setReturnCodeDesc(returnCodeDesc);
		return returnStatus;
	}
}
