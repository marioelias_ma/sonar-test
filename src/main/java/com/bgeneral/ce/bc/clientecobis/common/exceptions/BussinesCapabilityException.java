package com.bgeneral.ce.bc.clientecobis.common.exceptions;


public class BussinesCapabilityException extends RuntimeException {
	
	private final String errorCodeBussines;

	public BussinesCapabilityException(String errorCodeBussines, String errorMessage) {
		super(errorMessage);
		this.errorCodeBussines = errorCodeBussines;
	}

	public BussinesCapabilityException(String errorMessageBussines, Throwable cause) {
		super(errorMessageBussines, cause);
		errorCodeBussines = cause.toString();
	}

	public BussinesCapabilityException(String errorCodeBussines, String errorMessage, Throwable cause) {
		super(errorMessage, cause);
		this.errorCodeBussines = errorCodeBussines;
	}

	@Override
	public String toString() {
		return super.toString();
	}

	@Override
	public String getMessage() {
		return super.getMessage() + " for ErrorCode :" + errorCodeBussines;
	}
	
	public String getErrorCodeBussines() {
		return errorCodeBussines;
	}
}
