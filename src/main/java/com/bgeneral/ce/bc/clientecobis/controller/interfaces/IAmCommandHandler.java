package com.bgeneral.ce.bc.clientecobis.controller.interfaces;

import java.io.Serializable;

import com.bgeneral.ce.bc.clientecobis.common.dto.BCResponse;

/**
 * @author jnieves
 * 
 * Interfaz de implementacion de logica de negocio y orquestacion
 *
 * @param <C> <TCommand> Clase que implemeta la Interfaz IAmCommand
 * @param <I> <TResquest> Clase DTO de entrada para el Handler
 * @param <O> <TResponse> Clase DTO de respuesta del Handler
 */
public interface IAmCommandHandler<C extends IAmCommand<I, O>, I extends Serializable, O extends Serializable> {

	/**
	 * Metodo que contiene la logica a implementar la comunicacion con los adapter,
	 * la logica del negocio y manipulacion de los mensajes DTO
	 * 
	 * @param IAmCommand
	 * @return BCResponse<O>
	 */
	BCResponse<O> handle(C command);
}