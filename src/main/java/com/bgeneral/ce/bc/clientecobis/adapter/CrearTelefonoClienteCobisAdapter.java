package com.bgeneral.ce.bc.clientecobis.adapter;

import static com.bgeneral.ce.bc.clientecobis.common.util.transformation.RestPolicyUtil.buildGsonInterface;

import org.modelmapper.internal.util.Assert;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Repository;

import com.bgeneral.ce.bc.clientecobis.adapter.dto.TelefonoClienteCobisDTO;
import com.bgeneral.ce.bc.clientecobis.adapter.dto.ValorTelefonoCobisDTO;
import com.bgeneral.ce.bc.clientecobis.adapter.mapper.IAmMapperJsonDTO;
import com.bgeneral.ce.bc.clientecobis.common.dto.BCRequest;
import com.bgeneral.ce.bc.clientecobis.common.dto.BCResponse;
import com.bgeneral.ce.bc.clientecobis.common.util.configuration.ConfigurationAppProperties;
import com.bgeneral.ce.bc.clientecobis.handler.dto.TelefonoClienteDTO;
import com.bgeneral.ce.bc.clientecobis.handler.dto.ValorTelefonoDTO;
import com.bgeneral.ce.bc.clientecobis.handler.interfaces.IAmRestAdapter;

import feign.Headers;
import feign.RequestLine;

/**
 * Clase adapter que se comunica con el bus para guardar el telefono del cliente
 * 
 * @author jnieves
 *
 */
@Repository("CrearTelefonoClienteCobisAdapter")
public class CrearTelefonoClienteCobisAdapter implements IAmRestAdapter<TelefonoClienteDTO, ValorTelefonoDTO> {

	@Autowired
	private ConfigurationAppProperties configurationAppProperties;

	@Autowired
	@Qualifier("CrearTelefonoClienteCobisMapper")
	private IAmMapperJsonDTO<TelefonoClienteDTO, ValorTelefonoDTO, TelefonoClienteCobisDTO, ValorTelefonoCobisDTO> mapper;

	/**
	 * @author jnieves
	 *
	 */
	@FunctionalInterface
	interface ICrearTelefonoClienteCobis {
		@Headers("Content-Type: Application/json")
		@RequestLine("POST /")
		BCResponse<ValorTelefonoCobisDTO> crearTelefonoClienteCobis(BCRequest<TelefonoClienteCobisDTO> request);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see com.bgeneral.ce.bc.clientecobis.handler.interfaces.IAmRestAdapter#
	 * callRestService(com.bgeneral.ce.bc.clientecobis.common.dto.BCRequest)
	 */
	@Override
	public BCResponse<ValorTelefonoDTO> callRestService(BCRequest<TelefonoClienteDTO> request) {
		Assert.notNull(request, "BCRequest<TelefonoClienteDTO>.getBody() NULL");
		Assert.notNull(request.getBody(), "BCRequest<TelefonoClienteDTO>.getBody() NULL");
		BCRequest<TelefonoClienteCobisDTO> bcRequest = this.mapper.handlerRequestTOAdapterRequestDTO(request);

		ICrearTelefonoClienteCobis builder = buildGsonInterface(ICrearTelefonoClienteCobis.class,
				configurationAppProperties.getRestEndpoint(), "/sen/ce/cliente/creartelefonoclientecobis");

		BCResponse<ValorTelefonoCobisDTO> response = builder.crearTelefonoClienteCobis(bcRequest);
		BCResponse<ValorTelefonoDTO> bcResponse = this.mapper.adapterResponseTOHandlerResponseDTO(response);

		Assert.notNull(bcResponse.getStatus().getReturnStatus(), "BCResponse<ValorTelefonoDTO>.getStatus() NULL");
		return bcResponse;
	}

}
