package com.bgeneral.ce.bc.clientecobis.adapter.dto;

import java.io.Serializable;

import com.fasterxml.jackson.annotation.JsonProperty;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class CrearCorreoClienteCobisRqDTO implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = -146738235499364314L;

	@JsonProperty("tipoUso")
	private String tipoUso;

	@JsonProperty("direccion")
	private String direccion;
	
	@JsonProperty("tipoMail")
	private String tipoMail;
}
