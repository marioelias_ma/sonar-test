package com.bgeneral.ce.bc.clientecobis.controller.dto;

import java.io.Serializable;
import java.util.Date;

import com.bgeneral.ce.bc.clientecobis.common.dto.Catalogo;
import com.bgeneral.ce.bc.clientecobis.common.util.transformation.CustomDateSerializer;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class DatosDireccionClienteCobisDTO implements Serializable{
	
	private static final long serialVersionUID = -4370977286238606685L;

	@JsonProperty("direccion")
	private Catalogo direccion;
	
	@JsonProperty("tipoDireccion")
	private String tipoDireccion;
		
	@JsonProperty("corregimiento")
	private Catalogo corregimiento;
	
	@JsonProperty("ciudad")
	private Catalogo ciudad;
	
	@JsonProperty("oficina")
	private Catalogo oficina;

	@JsonProperty("pais")
	private Catalogo pais;

	@JsonProperty("barrio")
	private Catalogo barrio;

	@JsonProperty("sector")
	private Catalogo sector;

	@JsonProperty("zona")
	private Catalogo zona;
	
	@JsonProperty("calle")
	private String calle;

	@JsonProperty("edificioCasa")
	private String edificioCasa;
	
	@JsonProperty("fechaRegistro")
	@JsonSerialize(using = CustomDateSerializer.class)
	private Date fechaRegistro;
	
	@JsonProperty("fechaModificacion")
	@JsonSerialize(using = CustomDateSerializer.class)
	private Date fechaModificacion;
	
	@JsonProperty("fechaVerificacion")
	@JsonSerialize(using = CustomDateSerializer.class)
	private Date fechaVerificacion;

	@JsonProperty("esVigente")
	private Boolean esVigente;

	@JsonProperty("esVerificado")
	private Boolean esVerificado;

	@JsonProperty("esPrincipal")
	private Boolean esPrincipal;
		
	@JsonProperty("funcionario")
	private String funcionario;
}
