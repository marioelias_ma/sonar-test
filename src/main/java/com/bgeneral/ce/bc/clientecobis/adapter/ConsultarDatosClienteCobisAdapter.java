package com.bgeneral.ce.bc.clientecobis.adapter;

import static com.bgeneral.ce.bc.clientecobis.common.util.transformation.RestPolicyUtil.buildGsonInterface;

import org.modelmapper.internal.util.Assert;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Repository;

import com.bgeneral.ce.bc.clientecobis.adapter.dto.CredencialesClienteCobisDTO;
import com.bgeneral.ce.bc.clientecobis.adapter.dto.DatosClienteCobisDTO;
import com.bgeneral.ce.bc.clientecobis.adapter.mapper.IAmMapperJsonDTO;
import com.bgeneral.ce.bc.clientecobis.common.dto.BCRequest;
import com.bgeneral.ce.bc.clientecobis.common.dto.BCResponse;
import com.bgeneral.ce.bc.clientecobis.common.util.configuration.ConfigurationAppProperties;
import com.bgeneral.ce.bc.clientecobis.controller.dto.CredencialesClienteDTO;
import com.bgeneral.ce.bc.clientecobis.handler.dto.DatosClienteHandlerDTO;
import com.bgeneral.ce.bc.clientecobis.handler.interfaces.IAmRestAdapter;

import feign.Headers;
import feign.RequestLine;

/**
 * Clase Adapter que implementa la llamada rest a la operacion ConsultarDatosCliente 
 * @author igudino
 */
@Repository("ConsultarDatosClienteCobisAdapter")
public class ConsultarDatosClienteCobisAdapter implements IAmRestAdapter<CredencialesClienteDTO, DatosClienteHandlerDTO> {
	
	@Autowired
	private ConfigurationAppProperties configurationAppProperties;
	
	@Autowired
	@Qualifier("ConsultarDatosClienteCobisMapper")
	private IAmMapperJsonDTO<CredencialesClienteDTO, DatosClienteHandlerDTO, CredencialesClienteCobisDTO, DatosClienteCobisDTO> mapper;
	
	/**
	 * @author igudino
	 * 
	 */
	@FunctionalInterface
	interface IConsultarConfiguracionDatosClienteCobisDTO {
		/**
		 * Metodo rest para la llamada a la operacion ConsultarDatosclienteCobis en el BUS
		 * @param request
		 * @return
		 */
		@Headers("Content-Type: Application/json")
		@RequestLine("POST /")
		BCResponse<DatosClienteCobisDTO> consultarDatosClienteCobis(
				BCRequest<CredencialesClienteCobisDTO> request);
	}
	
	
	@Override
	public BCResponse<DatosClienteHandlerDTO> callRestService(BCRequest<CredencialesClienteDTO> request) {
		Assert.notNull(request, "BCRequest<DatosClienteCobisDTO>.getBody() NULL");
		
		IConsultarConfiguracionDatosClienteCobisDTO builder =  buildGsonInterface(IConsultarConfiguracionDatosClienteCobisDTO.class,
				configurationAppProperties.getRestEndpoint(), "/sen/ce/cliente/consultardatosclientecobis");
		
		BCResponse<DatosClienteCobisDTO> response = builder.consultarDatosClienteCobis(mapper.handlerRequestTOAdapterRequestDTO(request));
		BCResponse<DatosClienteHandlerDTO> bcResponse = mapper.adapterResponseTOHandlerResponseDTO(response);
		Assert.notNull(bcResponse.getStatus(), "BCResponse<DatosClienteCobisDTO>.getStatus() NULL");
		return bcResponse;
	}

}
