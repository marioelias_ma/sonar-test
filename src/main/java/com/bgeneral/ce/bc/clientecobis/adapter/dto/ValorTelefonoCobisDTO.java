package com.bgeneral.ce.bc.clientecobis.adapter.dto;

import java.io.Serializable;

import com.fasterxml.jackson.annotation.JsonProperty;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class ValorTelefonoCobisDTO implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 2703102227902381978L;
	
	@JsonProperty("codigoTelefono")
	private String codigoTelefono;
}
