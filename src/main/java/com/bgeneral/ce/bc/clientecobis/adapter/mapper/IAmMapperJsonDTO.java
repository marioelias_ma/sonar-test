package com.bgeneral.ce.bc.clientecobis.adapter.mapper;

import java.io.Serializable;

import com.bgeneral.ce.bc.clientecobis.common.dto.BCRequest;
import com.bgeneral.ce.bc.clientecobis.common.dto.BCResponse;

/**
 * @author jnieves
 * 
 * Interfaz (Tranformador) para los mapper entre los DTO de Handler y el Adapter
 *
 * @param <I> THandlerRequest extends Serializable
 * @param <O> THandlerResponse extends Serializable
 * @param <E> TAdapterRequest extends Serializable
 * @param <S> TAdapterResponse extends Serializable
 */
public interface IAmMapperJsonDTO <I extends Serializable, O extends Serializable,
								E extends Serializable, S extends Serializable>{
	
	BCRequest<E> handlerRequestTOAdapterRequestDTO(BCRequest<I> request);
	
	BCResponse<O> adapterResponseTOHandlerResponseDTO(BCResponse<S> response);
}