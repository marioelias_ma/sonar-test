package com.bgeneral.ce.bc.clientecobis.handler.command;

import com.bgeneral.ce.bc.clientecobis.common.dto.BCRequest;
import com.bgeneral.ce.bc.clientecobis.controller.dto.CodigosRegistroClienteDTO;
import com.bgeneral.ce.bc.clientecobis.controller.dto.DatosClientesCanalesDTO;
import com.bgeneral.ce.bc.clientecobis.controller.interfaces.IAmCommand;

import lombok.Getter;

/**
 * @author F. Castillo
 *
 */
@Getter
public class CrearClienteCommand implements IAmCommand<DatosClientesCanalesDTO, CodigosRegistroClienteDTO> {

	/**
	 * 
	 */
	private  BCRequest<DatosClientesCanalesDTO> dto;

	/**
	 * @param request
	 */
	public CrearClienteCommand(BCRequest<DatosClientesCanalesDTO> request) {
		this.dto = request;
	}
	
}
