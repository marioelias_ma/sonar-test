package com.bgeneral.ce.bc.clientecobis.common.util.transformation;

import java.io.IOException;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import com.bgeneral.ce.bc.clientecobis.common.util.catalogs.CodeCatalog;
import com.bgeneral.ce.bc.clientecobis.common.util.configuration.ConfigurationAppProperties;
import com.google.gson.FieldNamingPolicy;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

import feign.Feign;
import feign.Request;
import feign.Response;
import feign.gson.GsonDecoder;
import feign.gson.GsonEncoder;

public class RestPolicyUtil {

	private RestPolicyUtil() {}

	public static <T> T buildGsonInterface(final Class<T> apiType, ConfigurationAppProperties.ClientConf clientConf,
			String pathResources) {
		/* Se obtiene el endpoint del archivo de properties */
		StringBuilder builder = new StringBuilder();
		builder.append(clientConf.getHost());
		builder.append(pathResources);
				
		return Feign.builder().encoder(new GsonEncoder(buildGsonSerializer()))
				.decoder(new GsonDecoder(buildGsonSerializer()))
				.logger(new BgFeignLogger(apiType))
				.logLevel(feign.Logger.Level.FULL)
				.target(apiType, builder.toString());
	}

	public static Gson buildGsonSerializer() {
		GsonBuilder builder = new GsonBuilder();
		builder.setFieldNamingPolicy(FieldNamingPolicy.IDENTITY)
				.setDateFormat(CodeCatalog.S_DEFAULT_DATETIME_FORMAT_OUTPUT.codeS()).setLenient();
		return builder.create();
	}
	
	private static class BgFeignLogger extends feign.Logger {
		private final Logger logger;

		private BgFeignLogger(Logger logger) {
			this.logger = logger;
		}

		public BgFeignLogger(Class<?> clazz) {
			this(LogManager.getLogger(clazz));
		}

		@Override
		protected void log(String configKey, String format, Object... args) {
			if (logger.isDebugEnabled()) {
				logger.debug("Adapter " + String.format(methodTag(configKey) + format, args));
			}
		}

		@Override
		protected void logRequest(String configKey, Level logLevel, Request request) {
			if (logger.isDebugEnabled()) {
				super.logRequest(configKey, logLevel, request);
			}
		}

		@Override
		protected Response logAndRebufferResponse(String configKey, Level logLevel, Response response, long elapsedTime)
				throws IOException {
			if (logger.isDebugEnabled()) {
				return super.logAndRebufferResponse(configKey, logLevel, response, elapsedTime);
			}
			return response;
		}

	}
}
