package com.bgeneral.ce.bc.clientecobis.adapter.mapper.impl;

import org.springframework.stereotype.Component;

import com.bgeneral.ce.bc.clientecobis.adapter.dto.ConsultaClientePEP;
import com.bgeneral.ce.bc.clientecobis.adapter.mapper.IAmMapperJsonDTO;
import com.bgeneral.ce.bc.clientecobis.common.dto.BCRequest;
import com.bgeneral.ce.bc.clientecobis.common.dto.BCResponse;
import com.bgeneral.ce.bc.clientecobis.common.dto.EmptyBodyT;
import com.bgeneral.ce.bc.clientecobis.common.util.catalogs.CodeCatalog;
import com.bgeneral.ce.bc.clientecobis.handler.dto.ClientePEPDTO;

/**
 * Clase Mapper para el Request y Response de la clase de
 * ConsultarClientePEPAdapter
 * 
 * @author A Lugo
 */

@Component("ConsultarClientePEPMapper")
public class ConsultarClientePEPMapper implements IAmMapperJsonDTO<EmptyBodyT, ClientePEPDTO, EmptyBodyT, ConsultaClientePEP>{

	@Override
	public BCRequest<EmptyBodyT> handlerRequestTOAdapterRequestDTO(BCRequest<EmptyBodyT> request) {
		return null;
	}
	
	
	@Override
	public BCResponse<ClientePEPDTO> adapterResponseTOHandlerResponseDTO(
			BCResponse<ConsultaClientePEP> response) {

		BCResponse<ClientePEPDTO> bcResponse = new BCResponse<>();
		bcResponse.setHeader(response.getHeader());
		bcResponse.setStatus(response.getStatus());

		
		if(CodeCatalog.CODE_SUCCESS.codeS().equals(response.getStatus().getReturnStatus().getReturnCode())) {
			ClientePEPDTO body = new ClientePEPDTO();
			body.setEsPEP((response.getBody().getNumeroRegistros()>0));
			bcResponse.setBody(body);
		}
		return bcResponse;
	}

}
