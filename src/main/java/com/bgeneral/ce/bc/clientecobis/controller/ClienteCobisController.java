package com.bgeneral.ce.bc.clientecobis.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.cloud.context.config.annotation.RefreshScope;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.bgeneral.ce.bc.clientecobis.common.decorators.LogDecoratorHandler;
import com.bgeneral.ce.bc.clientecobis.common.dto.BCRequest;
import com.bgeneral.ce.bc.clientecobis.common.dto.BCResponse;
import com.bgeneral.ce.bc.clientecobis.common.dto.EmptyBodyT;
import com.bgeneral.ce.bc.clientecobis.controller.dto.CodigosRegistroClienteDTO;
import com.bgeneral.ce.bc.clientecobis.controller.dto.CredencialesClienteDTO;
import com.bgeneral.ce.bc.clientecobis.controller.dto.DatosClienteDTO;
import com.bgeneral.ce.bc.clientecobis.controller.dto.DatosClientesCanalesDTO;
import com.bgeneral.ce.bc.clientecobis.controller.dto.DatosDireccionClienteCobisDTO;
import com.bgeneral.ce.bc.clientecobis.controller.interfaces.IAmCommandHandler;
import com.bgeneral.ce.bc.clientecobis.handler.command.ConsultarDatosClienteCommand;
import com.bgeneral.ce.bc.clientecobis.handler.command.ConsultarDireccionClienteCommand;
import com.bgeneral.ce.bc.clientecobis.handler.command.CrearClienteCommand;

/**
 * @author F. Castillo, I. Gudino, J. Nieves(Soaint)
 * 
 *
 */
@RefreshScope
@RestController
@RequestMapping("/api/ce/bc/cliente/")
public class ClienteCobisController {

	@Autowired
	@Qualifier("CrearClienteCommandHandler")
	private IAmCommandHandler<CrearClienteCommand, DatosClientesCanalesDTO, CodigosRegistroClienteDTO> crearClienteCommandHandler;
	
	@Autowired
	@Qualifier("ConsultarDireccionClienteCommandHandler")
	private IAmCommandHandler<ConsultarDireccionClienteCommand, EmptyBodyT, DatosDireccionClienteCobisDTO> consultarDireccionClienteCommandHandler;
	
    @Autowired
	@Qualifier("ConsultarDatosClienteCommandHandler")
	private IAmCommandHandler<ConsultarDatosClienteCommand, CredencialesClienteDTO, DatosClienteDTO> consultarDatosClienteCommandHandler;	
		
	@PostMapping(path = "/crearcliente", consumes = "application/json", produces = "application/json")
	public ResponseEntity<BCResponse<CodigosRegistroClienteDTO>> crearClienteCanales(@RequestBody BCRequest<DatosClientesCanalesDTO> request){
		CrearClienteCommand command = new CrearClienteCommand(request);
		IAmCommandHandler<CrearClienteCommand, DatosClientesCanalesDTO, CodigosRegistroClienteDTO> logDecoratorHandler = new LogDecoratorHandler<>(crearClienteCommandHandler);
		return new ResponseEntity<>(logDecoratorHandler.handle(command), HttpStatus.OK);
	}

	@PostMapping(path = "/consultardireccion", consumes = "application/json", produces = "application/json")
	public ResponseEntity<BCResponse<DatosDireccionClienteCobisDTO>> consultarDireccionCliente(@RequestBody BCRequest<EmptyBodyT> request){
		ConsultarDireccionClienteCommand command = new ConsultarDireccionClienteCommand(request);
		IAmCommandHandler<ConsultarDireccionClienteCommand, EmptyBodyT, DatosDireccionClienteCobisDTO> logDecoratorHandler = new LogDecoratorHandler<>(consultarDireccionClienteCommandHandler);
		return new ResponseEntity<>(logDecoratorHandler.handle(command), HttpStatus.OK);
	}
	
	@PostMapping(path = "/consultar", consumes = "application/json", produces = "application/json")
	public ResponseEntity<BCResponse<DatosClienteDTO>> consultarDatosClienteCobis(@RequestBody BCRequest<CredencialesClienteDTO> request){
		ConsultarDatosClienteCommand command = new ConsultarDatosClienteCommand(request);
		IAmCommandHandler<ConsultarDatosClienteCommand, CredencialesClienteDTO, DatosClienteDTO> logDecoratorHandler = new LogDecoratorHandler<>(consultarDatosClienteCommandHandler);
		return new ResponseEntity<>(logDecoratorHandler.handle(command), HttpStatus.OK);
	}
}

