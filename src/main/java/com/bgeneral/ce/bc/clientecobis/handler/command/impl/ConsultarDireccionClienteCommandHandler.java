package com.bgeneral.ce.bc.clientecobis.handler.command.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Service;

import com.bgeneral.ce.bc.clientecobis.common.dto.Status;
import com.bgeneral.ce.bc.clientecobis.common.dto.BCRequest;
import com.bgeneral.ce.bc.clientecobis.common.dto.BCResponse;
import com.bgeneral.ce.bc.clientecobis.common.dto.EmptyBodyT;
import com.bgeneral.ce.bc.clientecobis.common.dto.ReturnStatus;
import com.bgeneral.ce.bc.clientecobis.common.util.catalogs.CodeCatalog;
import com.bgeneral.ce.bc.clientecobis.common.exceptions.BussinesCapabilityException;
import com.bgeneral.ce.bc.clientecobis.controller.interfaces.IAmCommandHandler;
import com.bgeneral.ce.bc.clientecobis.controller.dto.DatosDireccionClienteCobisDTO;
import com.bgeneral.ce.bc.clientecobis.handler.interfaces.IAmRestAdapter;
import com.bgeneral.ce.bc.clientecobis.handler.command.ConsultarDireccionClienteCommand;
import com.bgeneral.ce.bc.clientecobis.handler.dto.DatosDireccionClienteCobisHandlerDTO;

/**
 * @author F. Nieves
 *
 */
@Service("ConsultarDireccionClienteCommandHandler")
public class ConsultarDireccionClienteCommandHandler implements IAmCommandHandler<ConsultarDireccionClienteCommand, EmptyBodyT, DatosDireccionClienteCobisDTO> {
	

	@Autowired
	@Qualifier("ConsultarDireccionClienteCobisAdapter")
	private IAmRestAdapter<EmptyBodyT, DatosDireccionClienteCobisHandlerDTO> consultarDireccionClienteCobisAdapter;

	@Override
	public BCResponse<DatosDireccionClienteCobisDTO> handle(ConsultarDireccionClienteCommand command) {
		
		BCRequest<EmptyBodyT> request = command.getDto();
		BCResponse<DatosDireccionClienteCobisDTO> response = new BCResponse<>();
		response.setStatus(new Status());
		
		try {
			
			BCResponse<DatosDireccionClienteCobisHandlerDTO> bcResponse = consultarDireccionClienteCobisAdapter.callRestService(request);
			response.setHeader(command.getDto().getHeader());
			response.getStatus().setReturnStatus(bcResponse.getStatus().getReturnStatus());
			
			if (response.getStatus().getReturnStatus() != null && 
					CodeCatalog.CODE_SUCCESS.codeS().equals(response.getStatus().getReturnStatus().getReturnCode())) {
				response.setBody(loadBodyResponse(bcResponse));
			}
			
		} catch (BussinesCapabilityException e) {
			response.getStatus().setReturnStatus(this.loadReturnStatus(e.getErrorCodeBussines(), e.getMessage()));
		} catch (Exception e) {
			response.getStatus().setReturnStatus(this.loadReturnStatus(CodeCatalog.CODE_ERROR.codeS(), e.getMessage()));
		}
		
		return response;
	}

	/**
	 * @param bcResponse
	 * @param response
	 * @return
	 */
	private DatosDireccionClienteCobisDTO loadBodyResponse(BCResponse<DatosDireccionClienteCobisHandlerDTO> bcResponse) {
		DatosDireccionClienteCobisDTO body = new DatosDireccionClienteCobisDTO();
		body.setBarrio(bcResponse.getBody().getBarrio());
		body.setCalle(bcResponse.getBody().getCalle());
		body.setCiudad(bcResponse.getBody().getCiudad());
		body.setCorregimiento(bcResponse.getBody().getCorregimiento());
		body.setDireccion(bcResponse.getBody().getDireccion());
		body.setEdificioCasa(bcResponse.getBody().getEdificioCasa());
		body.setEsPrincipal(bcResponse.getBody().getEsPrincipal());
		body.setEsVerificado(bcResponse.getBody().getEsVerificado());
		body.setEsVigente(bcResponse.getBody().getEsVigente());
		body.setFechaModificacion(bcResponse.getBody().getFechaModificacion());
		body.setFechaRegistro(bcResponse.getBody().getFechaRegistro());
		body.setFechaVerificacion(bcResponse.getBody().getFechaVerificacion());
		body.setFuncionario(bcResponse.getBody().getFuncionario());
		body.setOficina(bcResponse.getBody().getOficina());
		body.setPais(bcResponse.getBody().getPais());
		body.setSector(bcResponse.getBody().getSector());
		body.setTipoDireccion(bcResponse.getBody().getTipoDireccion());
		body.setZona(bcResponse.getBody().getZona());
		return body;
	}
	
	/**
	 * @param e
	 * @return
	 */
	private ReturnStatus loadReturnStatus(String returnCode, String returnCodeDesc) {
		ReturnStatus returnStatus = new ReturnStatus();
		returnStatus.setReturnCode(returnCode);
		returnStatus.setReturnCodeDesc(returnCodeDesc);
		return returnStatus;
	}

}
