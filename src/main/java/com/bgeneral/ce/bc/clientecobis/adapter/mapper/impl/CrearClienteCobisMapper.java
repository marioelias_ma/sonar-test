package com.bgeneral.ce.bc.clientecobis.adapter.mapper.impl;

import org.modelmapper.ModelMapper;
import org.springframework.stereotype.Component;

import com.bgeneral.ce.bc.clientecobis.adapter.dto.ClienteCobisDTO;
import com.bgeneral.ce.bc.clientecobis.adapter.dto.ValorClienteCobisDTO;
import com.bgeneral.ce.bc.clientecobis.adapter.mapper.IAmMapperJsonDTO;
import com.bgeneral.ce.bc.clientecobis.common.dto.BCRequest;
import com.bgeneral.ce.bc.clientecobis.common.dto.BCResponse;
import com.bgeneral.ce.bc.clientecobis.common.util.catalogs.CodeCatalog;
import com.bgeneral.ce.bc.clientecobis.handler.dto.EnteClienteDTO;
import com.bgeneral.ce.bc.clientecobis.handler.dto.InformacionClienteDTO;

/**
 * @author F. Castillo
 * 
 * Clase Mapper para el adapter CrearClienteCobisAdapter
 *
 */
@Component("CrearClienteCobisMapper")
public class CrearClienteCobisMapper implements IAmMapperJsonDTO<InformacionClienteDTO, EnteClienteDTO, ClienteCobisDTO, ValorClienteCobisDTO> {

	@Override
	public BCRequest<ClienteCobisDTO> handlerRequestTOAdapterRequestDTO(BCRequest<InformacionClienteDTO> request) {
		BCRequest<ClienteCobisDTO> bcRequest = new BCRequest<>();
		bcRequest.setHeader(request.getHeader());
		bcRequest.setBody(new ModelMapper().map(request.getBody(), ClienteCobisDTO.class));
		
		return bcRequest;
	}

	@Override
	public BCResponse<EnteClienteDTO> adapterResponseTOHandlerResponseDTO(
			BCResponse<ValorClienteCobisDTO> response) {
		BCResponse<EnteClienteDTO> bcResponse = new BCResponse<>();
		bcResponse.setHeader(response.getHeader());
		
		if(CodeCatalog.CODE_SUCCESS.codeS().equals(response.getStatus().getReturnStatus().getReturnCode())) {
			EnteClienteDTO clienteCobisRespDTO = new EnteClienteDTO();
			clienteCobisRespDTO.setEnteMis(response.getBody().getEnteMis());
			bcResponse.setBody(clienteCobisRespDTO);
		}
		bcResponse.setStatus(response.getStatus());
		return bcResponse;
	}

}
