package com.bgeneral.ce.bc.clientecobis.adapter;

import static com.bgeneral.ce.bc.clientecobis.common.util.transformation.RestPolicyUtil.buildGsonInterface;

import org.modelmapper.internal.util.Assert;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Repository;

import com.bgeneral.ce.bc.clientecobis.adapter.dto.ConsultaClientePEP;
import com.bgeneral.ce.bc.clientecobis.adapter.mapper.IAmMapperJsonDTO;
import com.bgeneral.ce.bc.clientecobis.common.dto.BCRequest;
import com.bgeneral.ce.bc.clientecobis.common.dto.BCResponse;
import com.bgeneral.ce.bc.clientecobis.common.dto.EmptyBodyT;
import com.bgeneral.ce.bc.clientecobis.common.util.configuration.ConfigurationAppProperties;
import com.bgeneral.ce.bc.clientecobis.handler.dto.ClientePEPDTO;
import com.bgeneral.ce.bc.clientecobis.handler.interfaces.IAmRestAdapter;

import feign.Headers;
import feign.RequestLine;


/**
 * Clase adapter que comunica con el bus para consultar si el cliente
 * es PEP(Persona Expuesta Publicamente) 
 * 
 * @author A Lugo
 *
 */
@Repository("ConsultarClientePEPAdapter")
public class ConsultarClientePEPAdapter  implements IAmRestAdapter<EmptyBodyT, ClientePEPDTO> {
	
	@Autowired
	private ConfigurationAppProperties configurationAppProperties;

	@Autowired
	@Qualifier("ConsultarClientePEPMapper")
	private IAmMapperJsonDTO<EmptyBodyT, ClientePEPDTO, EmptyBodyT, ConsultaClientePEP> mapper;
	
	@FunctionalInterface
	interface IConsultarClientePEP {
		@Headers("Content-Type: Application/json")
		@RequestLine("POST /")
		BCResponse<ConsultaClientePEP> consultarClientePEP(BCRequest<EmptyBodyT> request);
	}
	
	@Override
	public BCResponse<ClientePEPDTO> callRestService(BCRequest<EmptyBodyT> request) {
		Assert.notNull(request, "BCRequest<EmptyBodyT>.getBody() NULL");

		IConsultarClientePEP builder = buildGsonInterface(IConsultarClientePEP .class,
				configurationAppProperties.getRestEndpoint(), "/sen/ce/cliente/consultarclientepep");
		
		BCResponse<ConsultaClientePEP> response = builder.consultarClientePEP(request);
		BCResponse<ClientePEPDTO> bcResponse = this.mapper.adapterResponseTOHandlerResponseDTO(response);

		Assert.notNull(bcResponse.getStatus().getReturnStatus(), "BCResponse<ConsultaClientePEPHandlerDTO>.getStatus() NULL");
		return bcResponse;
	}

}
