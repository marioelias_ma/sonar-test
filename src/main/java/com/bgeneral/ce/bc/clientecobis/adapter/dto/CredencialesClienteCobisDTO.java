package com.bgeneral.ce.bc.clientecobis.adapter.dto;

import java.io.Serializable;

import com.fasterxml.jackson.annotation.JsonProperty;

import lombok.Getter;
import lombok.Setter;
@Getter
@Setter
public class CredencialesClienteCobisDTO implements Serializable{
	/**
	 * 
	 */
	private static final long serialVersionUID = -8674234603064391253L;
	
	@JsonProperty("numeroRegistros")
	private Integer numeroRegistros;

	@JsonProperty("formato")
	private Integer formato;
	
	@JsonProperty("cedula")
	private String cedula;
	
	@JsonProperty("onboarding")
	private boolean onboarding;
}
