package com.bgeneral.ce.bc.clientecobis.handler.dto;

import java.io.Serializable;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class ValorDireccionDTO  implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	@JsonProperty("codigoDireccion")
	private String codigoDireccion;
}
