package com.bgeneral.ce.bc.clientecobis.common.util.catalogs;

public enum CodeCatalog {
	CODE_SUCCESS("U0000"),
	CODE_ERROR("U0002"),
	S_ERROR("ERROR"),
	S_AUTORIZADOR_CONTRASENA("CONTRASENA"),
	S_ESTADO_VALIDACION_A("A"),
	S_ESTADO_VALIDACION_SUCCES("E"),
	S_OK("OK"),
	S_ACTIONMESSAGE_CREATE_USER("INGRESOINICIAL"),
    S_CATEGORIA_REG("REG"),
    S_DEFAULT_DATETIME_FORMAT_INPUT("yyyy-MM-dd'T'HH:mm:ss.SSSZZ"),
    S_DEFAULT_DATETIME_FORMAT_OUTPUT("yyyy-MM-dd'T'HH:mm:ss"),
	S_MSG_CODE_SUCCESS("Se ha realizado la ejecuci&#243;n del servicio correctamente."),
	S_MSG_CODE_ERROR("Se ha realizado la ejecuci&#243;n del servicio con posibles errores."),

	I_TRANSACTION_CODE_CREATE_USER(18761),
	I_SYNCHRONY_TYPA_A(1),
    I_TRANSACTION_CODE_24038(24038),
    I_ERROR_CODE_0(0),
    I_COD_PROD_BEL(40);

	private String codeS;
    private Integer codeI;

    CodeCatalog(String codeS) {
        this.codeS = codeS;
    }
    CodeCatalog(Integer codeI) {
        this.codeI = codeI;
    }
	
	public String codeS() {
        return codeS;
    }
    public Integer codeI() {
        return codeI;
    }
}
