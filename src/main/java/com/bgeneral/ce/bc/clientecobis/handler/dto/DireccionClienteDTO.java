package com.bgeneral.ce.bc.clientecobis.handler.dto;

import java.io.Serializable;

import com.bgeneral.ce.bc.clientecobis.common.dto.Catalogo;
import com.fasterxml.jackson.annotation.JsonProperty;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class DireccionClienteDTO  implements Serializable {
	
	@JsonProperty("descripcion")
	private String descripcion;
	
	@JsonProperty("tipoDireccion")
	private Catalogo tipoDireccion;
	
	@JsonProperty("corregimiento")
	private Catalogo corregimiento;
	
	@JsonProperty("ciudad")
	private Catalogo ciudad;
	
	@JsonProperty("oficina")
	private Catalogo oficina;
	
	@JsonProperty("pais")
	private Catalogo pais;
	
	@JsonProperty("barrio")
	private Catalogo barrio;

	@JsonProperty("calle")
	private String calle;

	@JsonProperty("codigoCasa")
	private String codigoCasa;
	
	@JsonProperty("esPrincipal")
	private boolean esPrincipal;

}
