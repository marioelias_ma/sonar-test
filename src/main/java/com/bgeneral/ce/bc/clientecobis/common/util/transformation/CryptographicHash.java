package com.bgeneral.ce.bc.clientecobis.common.util.transformation;

import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;

public class CryptographicHash {
	
	public static String cifrarPin(String codigo, String login) {
		MessageDigest md;
		try {
			md = MessageDigest.getInstance("SHA-256");
			md.update(new StringBuilder().append(codigo).append(login).toString().getBytes());
			byte[] toChapter1Digest = md.digest();
			return String.format("%064x", new java.math.BigInteger(1, toChapter1Digest));
		} catch (NoSuchAlgorithmException e) {
			return null;
		}

	}
}
