package com.bgeneral.ce.bc.clientecobis.common.util.transformation;

import javax.xml.ws.BindingProvider;

import org.apache.cxf.jaxws.JaxWsProxyFactoryBean;

import com.bgeneral.ce.bc.clientecobis.common.util.configuration.ConfigurationAppProperties;

public class SoapPolicyUtil{

	private SoapPolicyUtil() {}

	public static <T> T buildSoapInterface(final Class<T> apiType, ConfigurationAppProperties.ClientConf clientConf, String pathResources) {
		JaxWsProxyFactoryBean proxy = new JaxWsProxyFactoryBean();
		proxy.setServiceClass(apiType);

		StringBuilder builder = new StringBuilder();
		builder.append(clientConf.getHost());
		builder.append(pathResources);
		proxy.setAddress(builder.toString());
		T service = (T) proxy.create();

		BindingProvider bindingProvider = (BindingProvider) service;
		bindingProvider.getRequestContext().put("javax.xml.ws.client.receiveTimeOut",
				clientConf.getTimeout());
		bindingProvider.getRequestContext().put("javax.xml.ws.client.connectionTimeout",
				clientConf.getTimeout());
		return service;
	}

}
