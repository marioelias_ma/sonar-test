package com.bgeneral.ce.bc.clientecobis.common.dto;

import java.io.Serializable;

import com.fasterxml.jackson.annotation.JsonProperty;

import lombok.Getter;
import lombok.Setter;

/**
 * @author jnieves, brodriguez
 *
 * @param <T>
 */
@Getter
@Setter
public class BCRequest<T extends Serializable> implements Serializable{
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	@JsonProperty("header")
	private Header header;

	@JsonProperty("body")
	private T body;
}
