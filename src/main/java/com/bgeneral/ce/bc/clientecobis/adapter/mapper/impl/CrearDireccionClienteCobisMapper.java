package com.bgeneral.ce.bc.clientecobis.adapter.mapper.impl;

import org.modelmapper.ModelMapper;
import org.springframework.stereotype.Component;

import com.bgeneral.ce.bc.clientecobis.adapter.dto.DireccionClienteCobisDTO;
import com.bgeneral.ce.bc.clientecobis.adapter.dto.ValorDireccionCobisDTO;
import com.bgeneral.ce.bc.clientecobis.adapter.mapper.IAmMapperJsonDTO;
import com.bgeneral.ce.bc.clientecobis.common.dto.BCRequest;
import com.bgeneral.ce.bc.clientecobis.common.dto.BCResponse;
import com.bgeneral.ce.bc.clientecobis.common.util.catalogs.CodeCatalog;
import com.bgeneral.ce.bc.clientecobis.handler.dto.DireccionClienteDTO;
import com.bgeneral.ce.bc.clientecobis.handler.dto.ValorDireccionDTO;

/**
 * Clase Mapper para el Request y Response de la clase de
 * CrearDireccionClienteCobisAdapter
 * 
 * @author jnieves
 */
@Component("CrearDireccionClienteCobisMapper")
public class CrearDireccionClienteCobisMapper implements
		IAmMapperJsonDTO<DireccionClienteDTO, ValorDireccionDTO, DireccionClienteCobisDTO, ValorDireccionCobisDTO> {

	/* (non-Javadoc)
	 * @see com.bgeneral.ce.bc.clientecobis.adapter.mapper.interfaces.IAmMapperJsonDTO#handlerRequestTOAdapterRequestDTO(com.bgeneral.ce.bc.clientecobis.common.dto.BCRequest)
	 */
	@Override
	public BCRequest<DireccionClienteCobisDTO> handlerRequestTOAdapterRequestDTO(
			BCRequest<DireccionClienteDTO> request) {
		
		BCRequest<DireccionClienteCobisDTO> bcRequest = new BCRequest<>();
		bcRequest.setBody(new ModelMapper().map(request.getBody(), DireccionClienteCobisDTO.class));
		bcRequest.setHeader(request.getHeader());
		
		return bcRequest;
	}

	/* (non-Javadoc)
	 * @see com.bgeneral.ce.bc.clientecobis.adapter.mapper.interfaces.IAmMapperJsonDTO#adapterResponseTOHandlerResponseDTO(com.bgeneral.ce.bc.clientecobis.common.dto.BCResponse)
	 */
	@Override
	public BCResponse<ValorDireccionDTO> adapterResponseTOHandlerResponseDTO(
			BCResponse<ValorDireccionCobisDTO> response) {
		
		BCResponse<ValorDireccionDTO> bcResponse = new BCResponse<>();
		bcResponse.setHeader(response.getHeader());
		bcResponse.setStatus(response.getStatus());
		
		if(CodeCatalog.CODE_SUCCESS.codeS().equals(response.getStatus().getReturnStatus().getReturnCode())) {
			ValorDireccionDTO body = new ValorDireccionDTO();
			body.setCodigoDireccion(response.getBody().getCodigoDireccion());
			bcResponse.setBody(body);
		}
		
		return bcResponse;
	}

}
