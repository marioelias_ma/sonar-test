package com.bgeneral.ce.bc.clientecobis.handler.command.impl;

import static com.bgeneral.ce.bc.clientecobis.common.util.UtilBG.FormatDocumentIDLegacy;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Service;

import com.bgeneral.ce.bc.clientecobis.common.dto.BCRequest;
import com.bgeneral.ce.bc.clientecobis.common.dto.BCResponse;
import com.bgeneral.ce.bc.clientecobis.common.dto.Catalogo;
import com.bgeneral.ce.bc.clientecobis.common.dto.ReturnStatus;
import com.bgeneral.ce.bc.clientecobis.common.dto.Status;
import com.bgeneral.ce.bc.clientecobis.common.exceptions.BussinesCapabilityException;
import com.bgeneral.ce.bc.clientecobis.common.util.catalogs.CodeCatalog;
import com.bgeneral.ce.bc.clientecobis.controller.dto.CodigosRegistroClienteDTO;
import com.bgeneral.ce.bc.clientecobis.controller.dto.DatosClientesCanalesDTO;
import com.bgeneral.ce.bc.clientecobis.controller.interfaces.IAmCommandHandler;
import com.bgeneral.ce.bc.clientecobis.handler.command.CrearClienteCommand;
import com.bgeneral.ce.bc.clientecobis.handler.dto.CuentaCorreaClienteDTO;
import com.bgeneral.ce.bc.clientecobis.handler.dto.DireccionClienteDTO;
import com.bgeneral.ce.bc.clientecobis.handler.dto.EnteClienteDTO;
import com.bgeneral.ce.bc.clientecobis.handler.dto.InformacionClienteDTO;
import com.bgeneral.ce.bc.clientecobis.handler.dto.TelefonoClienteDTO;
import com.bgeneral.ce.bc.clientecobis.handler.dto.ValorCorreoDTO;
import com.bgeneral.ce.bc.clientecobis.handler.dto.ValorDireccionDTO;
import com.bgeneral.ce.bc.clientecobis.handler.dto.ValorTelefonoDTO;
import com.bgeneral.ce.bc.clientecobis.handler.interfaces.IAmRestAdapter;

/**
 * @author F. Castillo, A. Lugo, F. Nieves
 *
 */
@Service("CrearClienteCommandHandler")
public class CrearClienteCommandHandler
		implements IAmCommandHandler<CrearClienteCommand, DatosClientesCanalesDTO, CodigosRegistroClienteDTO> {

	@Autowired
	@Qualifier("CrearClienteCobisAdapter")
	private IAmRestAdapter<InformacionClienteDTO, EnteClienteDTO> crearClienteCobisAdapter;

	@Autowired
	@Qualifier("CrearCorreoClienteCobisAdapter")
	private IAmRestAdapter<CuentaCorreaClienteDTO, ValorCorreoDTO> crearCorreoClienteCobisAdapter;

	@Autowired
	@Qualifier("CrearDireccionClienteCobisAdapter")
	private IAmRestAdapter<DireccionClienteDTO, ValorDireccionDTO> crearDireccionClienteCobisAdapter;

	@Autowired
	@Qualifier("CrearTelefonoClienteCobisAdapter")
	private IAmRestAdapter<TelefonoClienteDTO, ValorTelefonoDTO> crearTelefonoClienteCobisAdapter;

	/*
	 * (non-Javadoc)
	 * 
	 * @see com.bgeneral.ce.bc.clientecobis.controller.interfaces.IAmCommandHandler#
	 * handle(com.bgeneral.ce.bc.clientecobis.controller.interfaces.IAmCommand)
	 */
	@Override
	public BCResponse<CodigosRegistroClienteDTO> handle(CrearClienteCommand command) {
		BCResponse<CodigosRegistroClienteDTO> bcResponse = new BCResponse<>();
		bcResponse.setStatus(new Status());

		try {
			BCResponse<EnteClienteDTO> enteCliente = this.crearClienteCobis(command.getDto());

			bcResponse.getStatus().setReturnStatus(enteCliente.getStatus().getReturnStatus());

			if (CodeCatalog.CODE_SUCCESS.codeS().equals(enteCliente.getStatus().getReturnStatus().getReturnCode())) {
				command.getDto().getHeader().getPerfil()
						.setNumeroCliente(Integer.valueOf(enteCliente.getBody().getEnteMis()));
				bcResponse.setBody(new CodigosRegistroClienteDTO());
				bcResponse.getBody().setEnteMis(Integer.valueOf(enteCliente.getBody().getEnteMis()));
				this.crearCorreoClienteCobis(command.getDto());
				this.crearDireccionClienteCobis(command.getDto());
			}

		} catch (BussinesCapabilityException e) {
			bcResponse.getStatus().setReturnStatus(this.returnStatus(e.getErrorCodeBussines(), e.getMessage()));
		} catch (Exception e) {
			bcResponse.getStatus().setReturnStatus(this.returnStatus(CodeCatalog.CODE_ERROR.codeS(), e.getMessage()));
		}

		bcResponse.setHeader(command.getDto().getHeader());
		return bcResponse;
	}

	/**
	 * @param e
	 * @return
	 */
	private ReturnStatus returnStatus(String returnCode, String returnCodeDesc) {
		ReturnStatus returnStatus = new ReturnStatus();
		returnStatus.setReturnCode(returnCode);
		returnStatus.setReturnCodeDesc(returnCodeDesc);
		return returnStatus;
	}

	/**
	 * @param bcRequest
	 * @return BCResponse<EmptyBodyT>
	 * @throws ClassNotFoundException
	 * @throws IllegalAccessException
	 * @throws InstantiationException
	 */
	private BCResponse<EnteClienteDTO> crearClienteCobis(BCRequest<DatosClientesCanalesDTO> requestCommand) {
		BCRequest<InformacionClienteDTO> requestCrearCliente = new BCRequest<>();

		InformacionClienteDTO crearClienteCobisRqDTO = new InformacionClienteDTO();
		crearClienteCobisRqDTO.setPrimerNombre(requestCommand.getBody().getPrimerNombre());
		crearClienteCobisRqDTO.setPrimerApellido(requestCommand.getBody().getPrimerApellido());
		crearClienteCobisRqDTO.setCedula(FormatDocumentIDLegacy(requestCommand.getBody().getCedula()));
		crearClienteCobisRqDTO.setFechaNacimiento(requestCommand.getBody().getFechaNacimiento());
		crearClienteCobisRqDTO.setSexo(requestCommand.getBody().getSexo());
		crearClienteCobisRqDTO.setEstadoCivil(requestCommand.getBody().getEstadoCivil());
		crearClienteCobisRqDTO.setPaisNacimiento(requestCommand.getBody().getPaisNacimiento());
		crearClienteCobisRqDTO.setPaisNacionalidad(requestCommand.getBody().getPaisNacionalidad());
		crearClienteCobisRqDTO.setPaisRiesgo(requestCommand.getBody().getPaisRiesgo());

		requestCrearCliente.setBody(crearClienteCobisRqDTO);
		requestCrearCliente.setHeader(requestCommand.getHeader());

		return crearClienteCobisAdapter.callRestService(requestCrearCliente);
	}

	/**
	 * @param command
	 * @return
	 */
	private void crearCorreoClienteCobis(BCRequest<DatosClientesCanalesDTO> requestCommand) {
		if (requestCommand.getBody().getCorreoElectronico() != null) {
			BCRequest<CuentaCorreaClienteDTO> requestCrearCorreoCliente = new BCRequest<>();
			CuentaCorreaClienteDTO crearCorreo = new CuentaCorreaClienteDTO();
			crearCorreo.setDireccion(requestCommand.getBody().getCorreoElectronico().getDireccionEmail());
			crearCorreo.setTipoMail("M");
			crearCorreo.setTipoUso(requestCommand.getBody().getCorreoElectronico().getTipoMail());

			requestCrearCorreoCliente.setBody(crearCorreo);
			requestCrearCorreoCliente.setHeader(requestCommand.getHeader());

			crearCorreoClienteCobisAdapter.callRestService(requestCrearCorreoCliente);
		}
	}

	/**
	 * @param requestCommand
	 * @return
	 */
	private void crearDireccionClienteCobis(BCRequest<DatosClientesCanalesDTO> requestCommand) {

		if (requestCommand.getBody().getDireccion() != null) {
			BCRequest<DireccionClienteDTO> bcRequest = new BCRequest<>();
			DireccionClienteDTO clienteDTO = new DireccionClienteDTO();
			clienteDTO.setBarrio(requestCommand.getBody().getDireccion().getBarrio());
			clienteDTO.setCalle(requestCommand.getBody().getDireccion().getCalle());
			clienteDTO.setCiudad(requestCommand.getBody().getDireccion().getCiudad());
			clienteDTO.setCodigoCasa(requestCommand.getBody().getDireccion().getCodigoCasa());
			clienteDTO.setCorregimiento(requestCommand.getBody().getDireccion().getCorregimiento());
			clienteDTO.setDescripcion(requestCommand.getBody().getDireccion().getDescripcion());
			clienteDTO.setEsPrincipal(requestCommand.getBody().getDireccion().isEsPrincipal());
			clienteDTO.setOficina(requestCommand.getBody().getDireccion().getOficina());
			clienteDTO.setPais(requestCommand.getBody().getDireccion().getPais());
			clienteDTO.setTipoDireccion(requestCommand.getBody().getDireccion().getTipoDireccion());

			bcRequest.setBody(clienteDTO);
			bcRequest.setHeader(requestCommand.getHeader());

			BCResponse<ValorDireccionDTO> bcResponse = crearDireccionClienteCobisAdapter.callRestService(bcRequest);
			
			if (requestCommand.getBody().getDireccion() != null && bcResponse.getBody() != null &&
					CodeCatalog.CODE_SUCCESS.codeS().equals(bcResponse.getStatus().getReturnStatus().getReturnCode())) {
				this.crearTelefonoClienteCobis(requestCommand, bcResponse.getBody().getCodigoDireccion());
			}
		}
	}

	/**
	 * @param requestCommand
	 * @param codigoDireccion
	 */
	private void crearTelefonoClienteCobis(BCRequest<DatosClientesCanalesDTO> requestCommand, String codigoDireccion) {

		BCRequest<TelefonoClienteDTO> bcRequest = new BCRequest<>();

		TelefonoClienteDTO body = new TelefonoClienteDTO();
		body.setDireccion(new Catalogo());
		body.getDireccion().setCodigo(codigoDireccion);
		body.setExtencion(requestCommand.getBody().getTelefono().getExtencion());
		body.setNumeroTelefono(requestCommand.getBody().getTelefono().getNumeroTelefono());
		body.setPais(requestCommand.getBody().getDireccion().getPais());
		body.setTipoTelefono(requestCommand.getBody().getTelefono().getTipoTelefono());
		bcRequest.setBody(body);

		bcRequest.setHeader(requestCommand.getHeader());

		crearTelefonoClienteCobisAdapter.callRestService(bcRequest);
	}

}
