package com.bgeneral.ce.bc.clientecobis.handler.interfaces;

import java.io.Serializable;

import com.bgeneral.ce.bc.clientecobis.common.dto.BCRequest;
import com.bgeneral.ce.bc.clientecobis.common.dto.BCResponse;

public interface IAmSoapAdapter <TRequest extends Serializable, TResponse extends Serializable> {

	BCResponse<TResponse> callSoapService(BCRequest<TRequest> request);

}
