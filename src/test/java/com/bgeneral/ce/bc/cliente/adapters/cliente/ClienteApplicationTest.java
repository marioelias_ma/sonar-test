package com.bgeneral.ce.bc.cliente.adapters.cliente;

import static org.hamcrest.CoreMatchers.instanceOf;

import java.util.Date;
import java.util.GregorianCalendar;

import javax.xml.datatype.DatatypeConfigurationException;
import javax.xml.datatype.DatatypeFactory;
import javax.xml.datatype.XMLGregorianCalendar;

import org.junit.Assert;
import org.junit.Ignore;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import com.bgeneral.ce.bc.ConsultarClienteCanalesApplication;
import com.bgeneral.ce.bc.cliente.features.dto.cliente.ClienteCanalResp;
import com.bgeneral.ce.bc.cliente.features.dto.cliente.ClienteCanalRq;
import com.bgeneral.ce.bc.clientecobis.common.dto.BCRequest;
import com.bgeneral.ce.bc.clientecobis.common.dto.BCResponse;
import com.bgeneral.ce.bc.clientecobis.common.dto.Header;
import com.bgeneral.ce.bc.clientecobis.common.dto.Perfil;

/**
 * @author jnieves, F. Castillo, alugo
 *
 */

//Utilizar un yml de pruebas
@RunWith(SpringRunner.class)
@SpringBootTest(classes = ConsultarClienteCanalesApplication.class)
@Ignore
public class ClienteApplicationTest {

	@Test
	public void shouldConnectToSoapClientCrearUsuarioCobis() throws Exception {
		ClienteAdapter sut = new ClienteAdapter("http://10.252.103.11:6999");
		
		BCRequest<ClienteCanalRq> model = new BCRequest<>();
		
		ClienteCanalRq consultarClienteCanalesBody = new ClienteCanalRq();
		consultarClienteCanalesBody.setCedula("8-888-8888");
		
		model.setHeader(getHeader());
		model.setBody(consultarClienteCanalesBody);
		
		BCResponse<ClienteCanalResp> resp = sut.consultarClienteCanales(model);
		Assert.assertNotNull(resp);
		System.out.println("Respuesta del servicio: " + resp.getStatus().getReturnStatus().getReturnCode());
		Assert.assertThat(resp.getStatus().getReturnStatus().getReturnCode(), instanceOf(String.class));
	}
	
	public Header getHeader() throws DatatypeConfigurationException {
		Header headerRest = new Header();
		// Cabecera
		headerRest.setNumeroUnico(1);
		headerRest.setOficina(1);
		headerRest.setTerminal("1");
		headerRest.setUsuario("brodrigu");
		headerRest.setRolUsuario(3);
		// CabeceraCanales
		headerRest.setAplicacion("1");
		headerRest.setCanal("1");
		GregorianCalendar gregory = new GregorianCalendar();
		gregory.setTime(new Date());
		XMLGregorianCalendar date2 = DatatypeFactory.newInstance().newXMLGregorianCalendar(gregory);		
	
		headerRest.setFecha(date2.toGregorianCalendar().getTime());
		headerRest.setNodo(1);
		headerRest.setSesionBg("1234");
		headerRest.setDireccionIp("123.123.123.123");
		headerRest.setOrigenSolicitud("BEL");
		headerRest.setNumeroReferencia("1234");
		headerRest.setHoraInicio(date2.toString());
		// Perfil
		Perfil perfil = new Perfil();
		perfil.setLogin("1114567");
		perfil.setNumeroCliente(1114558);
		perfil.setNumeroBancaVirtual(423698);
		perfil.setAlias("personal05");
		perfil.setNumeroPerfil(3);

		headerRest.setPerfil(perfil);
		return headerRest;
	}

}
