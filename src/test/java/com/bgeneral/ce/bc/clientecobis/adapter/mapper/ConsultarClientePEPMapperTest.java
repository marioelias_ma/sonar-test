package com.bgeneral.ce.bc.clientecobis.adapter.mapper;

import static org.hamcrest.CoreMatchers.equalTo;
import static org.hamcrest.CoreMatchers.instanceOf;
import static org.junit.Assert.assertThat;

import java.util.Date;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.junit.MockitoJUnitRunner;

import com.bgeneral.ce.bc.clientecobis.adapter.dto.ConsultaClientePEP;
import com.bgeneral.ce.bc.clientecobis.adapter.dto.DatosDireccionClienteCobisDTO;
import com.bgeneral.ce.bc.clientecobis.adapter.mapper.impl.ConsultarClientePEPMapper;
import com.bgeneral.ce.bc.clientecobis.common.dto.BCResponse;
import com.bgeneral.ce.bc.clientecobis.common.dto.Header;
import com.bgeneral.ce.bc.clientecobis.common.dto.Perfil;
import com.bgeneral.ce.bc.clientecobis.common.dto.ReturnStatus;
import com.bgeneral.ce.bc.clientecobis.common.dto.Status;
import com.bgeneral.ce.bc.clientecobis.common.util.catalogs.CodeCatalog;
import com.bgeneral.ce.bc.clientecobis.handler.dto.ClientePEPDTO;
import com.bgeneral.ce.bc.clientecobis.handler.dto.DatosDireccionClienteCobisHandlerDTO;

/**
 * 
 * @author anlugo
 *
 */

@RunWith(MockitoJUnitRunner.class)
public class ConsultarClientePEPMapperTest {
	
	// ConsultaClientePEPHandlerDTO, ConsultaClientePEP
		@InjectMocks
		private ConsultarClientePEPMapper sut;
		
		private BCResponse<ConsultaClientePEP> validLoadResponse() {
			BCResponse<ConsultaClientePEP> bcResponse = new BCResponse<>();
			bcResponse.setHeader(this.getHeader());
			bcResponse.setStatus(this.getStatus());
			bcResponse.setBody(this.getBody());
			return bcResponse;
		}	
		
		/*
		 * Testing Mapper to Response
		 * 
		 */
		@Test
		public void buildHandlerRequestTOAdapterResponse() {
			BCResponse<ConsultaClientePEP> bcResponse = validLoadResponse();
			sut.adapterResponseTOHandlerResponseDTO(bcResponse);
		}
	

		@Test(expected = NullPointerException.class)
		public void validateStatusResponseNull() {
			BCResponse<ConsultaClientePEP> bcResponse = validLoadResponse();
			bcResponse.getStatus().setReturnStatus(null);
			sut.adapterResponseTOHandlerResponseDTO(bcResponse);
		}
		
		@Test
		public void validateValueInHeaderResponse() {
			BCResponse<ConsultaClientePEP> bcResponse = validLoadResponse();
			BCResponse<ClientePEPDTO> response = sut.adapterResponseTOHandlerResponseDTO(bcResponse);

			assertThat(response.getHeader().getCanal(), instanceOf(String.class));
			assertThat(response.getHeader().getCanal(), equalTo("1"));
		}
		
		@Test
		public void validateValueInBodyResponse() {
			BCResponse<ConsultaClientePEP> bcResponse = validLoadResponse();
			BCResponse<ClientePEPDTO> response = sut.adapterResponseTOHandlerResponseDTO(bcResponse);

			assertThat(response.getBody().isEsPEP(), instanceOf(Boolean.class));
			assertThat(response.getBody().isEsPEP(), equalTo(true));
		}
		
		@Test
		public void validateValueInNullBodyResponse() {
			BCResponse<ConsultaClientePEP> bcResponse = validLoadResponse();
			bcResponse.getStatus().getReturnStatus().setReturnCode("U0002");
			bcResponse.getStatus().getReturnStatus().setReturnCodeDesc("FALLO");
			BCResponse<ClientePEPDTO> response = sut.adapterResponseTOHandlerResponseDTO(bcResponse);

			assertThat(response.getBody(), equalTo(null));
		}
		
		private ConsultaClientePEP getBody() {
			ConsultaClientePEP body = new ConsultaClientePEP();
			body.setNumeroRegistros(1);		
			return body;
		}

		
		private Header getHeader() {
			Header headerRest = new Header();
			// Cabecera
			headerRest.setNumeroUnico(1);
			headerRest.setOficina(1);
			headerRest.setTerminal("1");
			headerRest.setUsuario("bccanales");
			headerRest.setRolUsuario(3);
			// CabeceraCanales
			headerRest.setAplicacion("1");
			headerRest.setCanal("1");
			headerRest.setFecha(new Date());
			headerRest.setNodo(1);
			headerRest.setSesionBg("1234");
			headerRest.setDireccionIp("");
			headerRest.setOrigenSolicitud("bel");
			headerRest.setNumeroReferencia("1234");
			headerRest.setHoraInicio("12:00:00");

			// Perfil
			Perfil perfil = new Perfil();
			perfil.setLogin("");
			perfil.setNumeroCliente(12);
			perfil.setNumeroBancaVirtual(0);
			perfil.setAlias("prueba");
			perfil.setNumeroPerfil(3);

			headerRest.setPerfil(perfil);
			return headerRest;
		}

		private Status getStatus() {
			Status status = new Status();
			ReturnStatus returnStatus = new ReturnStatus();
			returnStatus.setReturnCode(CodeCatalog.CODE_SUCCESS.codeS());
			returnStatus.setReturnCodeDesc(CodeCatalog.S_MSG_CODE_SUCCESS.codeS());
			status.setReturnStatus(returnStatus);
			return status;
		}

}
