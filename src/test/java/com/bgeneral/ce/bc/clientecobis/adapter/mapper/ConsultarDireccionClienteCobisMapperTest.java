package com.bgeneral.ce.bc.clientecobis.adapter.mapper;

import static org.hamcrest.CoreMatchers.equalTo;
import static org.hamcrest.CoreMatchers.instanceOf;
import static org.junit.Assert.assertThat;

import java.util.Date;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.junit.MockitoJUnitRunner;

import com.bgeneral.ce.bc.clientecobis.adapter.dto.DatosDireccionClienteCobisDTO;
import com.bgeneral.ce.bc.clientecobis.adapter.mapper.impl.ConsultarDireccionClienteCobisMapper;
import com.bgeneral.ce.bc.clientecobis.common.dto.BCResponse;
import com.bgeneral.ce.bc.clientecobis.common.dto.Catalogo;
import com.bgeneral.ce.bc.clientecobis.common.dto.Header;
import com.bgeneral.ce.bc.clientecobis.common.dto.Perfil;
import com.bgeneral.ce.bc.clientecobis.common.dto.ReturnStatus;
import com.bgeneral.ce.bc.clientecobis.common.dto.Status;
import com.bgeneral.ce.bc.clientecobis.common.util.catalogs.CodeCatalog;
import com.bgeneral.ce.bc.clientecobis.handler.dto.DatosDireccionClienteCobisHandlerDTO;

@RunWith(MockitoJUnitRunner.class)
public class ConsultarDireccionClienteCobisMapperTest {

	// DatosDireccionClienteCobisHandlerDTO, DatosDireccionClienteCobisDTO
	@InjectMocks
	private ConsultarDireccionClienteCobisMapper sut;

	private BCResponse<DatosDireccionClienteCobisDTO> validLoadResponse() {
		BCResponse<DatosDireccionClienteCobisDTO> bcResponse = new BCResponse<>();
		bcResponse.setHeader(this.getHeader());
		bcResponse.setStatus(this.getStatus());
		bcResponse.setBody(this.getBody());
		return bcResponse;
	}

	/*
	 * Testing Mapper to Response
	 * 
	 */
	@Test
	public void buildHandlerRequestTOAdapterResponse() {
		BCResponse<DatosDireccionClienteCobisDTO> bcResponse = validLoadResponse();
		sut.adapterResponseTOHandlerResponseDTO(bcResponse);
	}

	@Test
	public void validateValueResponseDataType() {
		BCResponse<DatosDireccionClienteCobisDTO> bcResponse = validLoadResponse();
		assertThat(sut.adapterResponseTOHandlerResponseDTO(bcResponse).getBody(),
				instanceOf(DatosDireccionClienteCobisHandlerDTO.class));
	}

	@Test(expected = NullPointerException.class)
	public void validateStatusResponseNull() {
		BCResponse<DatosDireccionClienteCobisDTO> bcResponse = validLoadResponse();
		bcResponse.getStatus().setReturnStatus(null);
		sut.adapterResponseTOHandlerResponseDTO(bcResponse);
	}

	@Test
	public void validateValueBodyResponseNull() {
		BCResponse<DatosDireccionClienteCobisDTO> bcResponse = validLoadResponse();
		bcResponse.getStatus().getReturnStatus().setReturnCode("U0002");
		bcResponse.getStatus().getReturnStatus().setReturnCodeDesc("FALLO");
		BCResponse<DatosDireccionClienteCobisHandlerDTO> response = sut.adapterResponseTOHandlerResponseDTO(bcResponse);

		assertThat(response.getBody(), equalTo(null));
	}

	@Test
	public void validateValueInHeaderResponse() {
		BCResponse<DatosDireccionClienteCobisDTO> bcResponse = validLoadResponse();
		BCResponse<DatosDireccionClienteCobisHandlerDTO> response = sut.adapterResponseTOHandlerResponseDTO(bcResponse);

		assertThat(response.getHeader().getCanal(), instanceOf(String.class));
		assertThat(response.getHeader().getCanal(), equalTo("1"));
	}

	@Test
	public void validateValueInBodyResponse() {
		BCResponse<DatosDireccionClienteCobisDTO> bcResponse = validLoadResponse();
		BCResponse<DatosDireccionClienteCobisHandlerDTO> response = sut.adapterResponseTOHandlerResponseDTO(bcResponse);

		assertThat(response.getBody().getCalle(), instanceOf(String.class));
		assertThat(response.getBody().getCalle(), equalTo("CALLE"));
	}

	private DatosDireccionClienteCobisDTO getBody() {
		DatosDireccionClienteCobisDTO body = new DatosDireccionClienteCobisDTO();

		body.setCalle("CALLE");
		body.setEdificioCasa("EDIFICIO CASA");
		body.setFuncionario("FUNCIONARIO");
		body.setTipoDireccion("TIPO DIRECCION");

		body.setEsPrincipal(true);
		body.setEsVerificado(false);
		body.setEsVigente(false);

		body.setFechaModificacion(new Date());
		body.setFechaRegistro(new Date());
		body.setFechaVerificacion(new Date());

		Catalogo catalogo = new Catalogo();

		catalogo.setCodigo("BARRIO_COD");
		catalogo.setDescripcion("Barrio Descripcion");
		body.setBarrio(catalogo);

		catalogo.setCodigo("CIUDAD_COD");
		catalogo.setDescripcion("Ciudad Descripcion");
		body.setCiudad(catalogo);

		catalogo.setCodigo("CORREGIMIENTO_COD");
		catalogo.setDescripcion("Corregimiento Descripcion");
		body.setCorregimiento(catalogo);

		catalogo.setCodigo("DIRECCION_COD");
		catalogo.setDescripcion("Direccion Descripcion");
		body.setDireccion(catalogo);

		catalogo.setCodigo("OFICINA_COD");
		catalogo.setDescripcion("Oficina Descripcion");
		body.setOficina(catalogo);

		catalogo.setCodigo("PAIS_COD");
		catalogo.setDescripcion("Pais Descripcion");
		body.setPais(catalogo);

		catalogo.setCodigo("SECTOR_COD");
		catalogo.setDescripcion("Sector Descripcion");
		body.setSector(catalogo);

		catalogo.setCodigo("ZONA_COD");
		catalogo.setDescripcion("Zona Descripcion");
		body.setZona(catalogo);

		return body;
	}

	private Header getHeader() {
		Header headerRest = new Header();
		// Cabecera
		headerRest.setNumeroUnico(1);
		headerRest.setOficina(1);
		headerRest.setTerminal("1");
		headerRest.setUsuario("bccanales");
		headerRest.setRolUsuario(3);
		// CabeceraCanales
		headerRest.setAplicacion("1");
		headerRest.setCanal("1");
		headerRest.setFecha(new Date());
		headerRest.setNodo(1);
		headerRest.setSesionBg("1234");
		headerRest.setDireccionIp("");
		headerRest.setOrigenSolicitud("bel");
		headerRest.setNumeroReferencia("1234");
		headerRest.setHoraInicio("12:00:00");

		// Perfil
		Perfil perfil = new Perfil();
		perfil.setLogin("");
		perfil.setNumeroCliente(12);
		perfil.setNumeroBancaVirtual(0);
		perfil.setAlias("prueba");
		perfil.setNumeroPerfil(3);

		headerRest.setPerfil(perfil);
		return headerRest;
	}

	private Status getStatus() {
		Status status = new Status();
		ReturnStatus returnStatus = new ReturnStatus();
		returnStatus.setReturnCode(CodeCatalog.CODE_SUCCESS.codeS());
		returnStatus.setReturnCodeDesc(CodeCatalog.S_MSG_CODE_SUCCESS.codeS());
		status.setReturnStatus(returnStatus);
		return status;
	}

}
