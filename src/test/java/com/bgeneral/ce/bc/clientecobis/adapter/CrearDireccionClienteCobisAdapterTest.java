package com.bgeneral.ce.bc.clientecobis.adapter;

import static org.hamcrest.CoreMatchers.instanceOf;
import static org.mockito.Mockito.when;

import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.MockitoJUnitRunner;
import org.springframework.boot.test.context.SpringBootTest;

import com.bgeneral.ce.bc.clientecobis.adapter.dto.DireccionClienteCobisDTO;
import com.bgeneral.ce.bc.clientecobis.adapter.dto.ValorDireccionCobisDTO;
import com.bgeneral.ce.bc.clientecobis.adapter.mapper.IAmMapperJsonDTO;
import com.bgeneral.ce.bc.clientecobis.common.dto.BCRequest;
import com.bgeneral.ce.bc.clientecobis.common.dto.BCResponse;
import com.bgeneral.ce.bc.clientecobis.common.dto.Catalogo;
import com.bgeneral.ce.bc.clientecobis.common.dto.ReturnStatus;
import com.bgeneral.ce.bc.clientecobis.handler.dto.DireccionClienteDTO;
import com.bgeneral.ce.bc.clientecobis.handler.dto.ValorDireccionDTO;
import com.bgeneral.ce.bc.clientecobis.util.MockDummyRequest;
import com.bgeneral.ce.bc.clientecobis.util.MockDummyResponse;
import com.bgeneral.ce.bc.clientecobis.util.MockServer;

/**
 * @author jnieves
 *
 */
@SpringBootTest
@RunWith(MockitoJUnitRunner.class)
public class CrearDireccionClienteCobisAdapterTest extends MockServer {
	
	@Mock
	private IAmMapperJsonDTO<DireccionClienteDTO, ValorDireccionDTO, DireccionClienteCobisDTO, ValorDireccionCobisDTO> mapper;
	
	@InjectMocks
	private CrearDireccionClienteCobisAdapter sut;
	
	
	@Test
	public void shouldConnectToLegacy(){
		String jsonString = "{\"header\":{\"numeroUnico\":123456,\"oficina\":62,\"terminal\":\"twen\",\"usuario\":\"twen\",\"rolUsuario\":1,\"aplicacion\":\"1\",\"canal\":\"1\",\"fecha\":\"2018-10-22T21:12:52.655Z\",\"nodo\":1,\"sesionBg\":\"\",\"direccionIp\":\"1.9.7.6\",\"origenSolicitud\":\"BEL\",\"numeroReferencia\":\"\",\"horaInicio\":\"\",\"perfil\":{\"login\":\"\",\"numeroCliente\":104953,\"numeroBancaVirtual\":0,\"alias\":\"yonofui\",\"numeroPerfil\":3,\"numeroClienteUsuario\":1,\"vistaAsociada\":1}},\"body\":{\"codigoDireccion\":\"6999\"},\"status\":{\"returnStatus\":{\"returnCode\":\"U0000\",\"returnCodeDesc\":\"Se ha realizado la ejecucion del servicio correctamente.\"}}}"; 
		setUpMockResponse(jsonString);
		modifyPropertyHost();

		when(mapper.handlerRequestTOAdapterRequestDTO(Mockito.any())).thenReturn(getRequestMapperDTO());
		when(mapper.adapterResponseTOHandlerResponseDTO(Mockito.any())).thenReturn(getResponseMapperDTO());
		
		BCResponse<ValorDireccionDTO> bcResponse = sut.callRestService(getRequestInServices());

		Assert.assertNotNull(bcResponse);
		Assert.assertThat(bcResponse.getStatus().getReturnStatus().getReturnCode(), instanceOf(String.class));
	}
	
	@Test
	public void validateServicesWithResponseBodyNull(){
		String jsonString = "{\"header\":{\"numeroUnico\":123456,\"oficina\":62,\"terminal\":\"twen\",\"usuario\":\"twen\",\"rolUsuario\":1,\"aplicacion\":\"1\",\"canal\":\"1\",\"fecha\":\"2018-10-22T21:12:52.655Z\",\"nodo\":1,\"sesionBg\":\"\",\"direccionIp\":\"1.9.7.6\",\"origenSolicitud\":\"BEL\",\"numeroReferencia\":\"\",\"horaInicio\":\"\",\"perfil\":{\"login\":\"\",\"numeroCliente\":104953,\"numeroBancaVirtual\":0,\"alias\":\"yonofui\",\"numeroPerfil\":3,\"numeroClienteUsuario\":1,\"vistaAsociada\":1}},\"body\":{\"codigoDireccion\":\"6999\"},\"status\":{\"returnStatus\":{\"returnCode\":\"U0000\",\"returnCodeDesc\":\"Se ha realizado la ejecucion del servicio correctamente.\"}}}"; 
		setUpMockResponse(jsonString);
		modifyPropertyHost();

		when(mapper.handlerRequestTOAdapterRequestDTO(Mockito.any())).thenReturn(getRequestMapperDTO());
		 BCResponse<ValorDireccionDTO> response = getResponseMapperDTO();
		 response.setBody(null);
		
		when(mapper.adapterResponseTOHandlerResponseDTO(Mockito.any())).thenReturn(response);
		
		BCResponse<ValorDireccionDTO> bcResponse = sut.callRestService(getRequestInServices());

		Assert.assertNotNull(bcResponse);
		Assert.assertThat(bcResponse.getStatus().getReturnStatus().getReturnCode(), instanceOf(String.class));
	}
	
	@Test(expected = IllegalArgumentException.class)
	public void validateServicesWithResponseStatusNull(){
		String jsonString = "{\"header\":{\"numeroUnico\":123456,\"oficina\":62,\"terminal\":\"twen\",\"usuario\":\"twen\",\"rolUsuario\":1,\"aplicacion\":\"1\",\"canal\":\"1\",\"fecha\":\"2018-10-22T21:12:52.655Z\",\"nodo\":1,\"sesionBg\":\"\",\"direccionIp\":\"1.9.7.6\",\"origenSolicitud\":\"BEL\",\"numeroReferencia\":\"\",\"horaInicio\":\"\",\"perfil\":{\"login\":\"\",\"numeroCliente\":104953,\"numeroBancaVirtual\":0,\"alias\":\"yonofui\",\"numeroPerfil\":3,\"numeroClienteUsuario\":1,\"vistaAsociada\":1}},\"body\":{\"codigoDireccion\":\"6999\"},\"status\":{\"returnStatus\":{\"returnCode\":\"U0000\",\"returnCodeDesc\":\"Se ha realizado la ejecucion del servicio correctamente.\"}}}"; 
		setUpMockResponse(jsonString);
		modifyPropertyHost();

		when(mapper.handlerRequestTOAdapterRequestDTO(Mockito.any())).thenReturn(getRequestMapperDTO());
		 BCResponse<ValorDireccionDTO> response = getResponseMapperDTO();
		 response.getStatus().setReturnStatus(null);
		
		when(mapper.adapterResponseTOHandlerResponseDTO(Mockito.any())).thenReturn(response);
		
		sut.callRestService(getRequestInServices());

	}
	
	@Test(expected = IllegalArgumentException.class)
	public void validateServicesWithRequestBodyNull(){		
		BCRequest<DireccionClienteDTO> bcRequest = getRequestInServices();
		bcRequest.setBody(null);
		sut.callRestService(bcRequest);
	}
	
	
	/* Methods to load data in the DTOs */
	private BCRequest<DireccionClienteCobisDTO> getRequestMapperDTO() {
		DireccionClienteCobisDTO body = new DireccionClienteCobisDTO();
		
		Catalogo catalogo = new Catalogo();
		body.setCalle("Calle de Silent Hill");
		body.setCodigoCasa("CHAPATA");
		body.setDescripcion("Descripcion de Direccion");
		body.setEsPrincipal(true);
		
		catalogo.setCodigo("Barrio");
		catalogo.setDescripcion("Descripcion Barrio");
		body.setBarrio(catalogo);
		
		catalogo.setCodigo("Ciudad");
		catalogo.setDescripcion("Descripcion Ciudad");
		body.setCiudad(catalogo);

		catalogo.setCodigo("Corregimiento");
		catalogo.setDescripcion("Descripcion Corregimiento");
		body.setCorregimiento(catalogo);

		catalogo.setCodigo("Oficina");
		catalogo.setDescripcion("Descripcion Oficina");
		body.setOficina(catalogo);

		catalogo.setCodigo("Pais");
		catalogo.setDescripcion("Descripcion Pais");
		body.setPais(catalogo);

		catalogo.setCodigo("TipoDireccion");
		catalogo.setDescripcion("Descripcion TipoDireccion");
		body.setTipoDireccion(catalogo);
			
		BCRequest<DireccionClienteCobisDTO> bcRequest = new MockDummyRequest<DireccionClienteCobisDTO>().setBody(body).build();
		return bcRequest;
	}

	
	private BCResponse<ValorDireccionDTO> getResponseMapperDTO() {
		ValorDireccionDTO body = new ValorDireccionDTO();
		body.setCodigoDireccion("123456");
		
		ReturnStatus returnStatus = new ReturnStatus();
		returnStatus.setReturnCode("U0000");
		returnStatus.setReturnCodeDesc("Se ha realizado al ejecucion con exito");
		
		return new MockDummyResponse<ValorDireccionDTO>().setBody(body).setReturnStatus(returnStatus).build();
	}
	
	private BCRequest<DireccionClienteDTO> getRequestInServices() {
		return new MockDummyRequest<DireccionClienteDTO>().setBody(new DireccionClienteDTO()).build();
	}

}
