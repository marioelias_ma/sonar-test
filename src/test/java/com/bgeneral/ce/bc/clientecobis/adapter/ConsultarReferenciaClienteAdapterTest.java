package com.bgeneral.ce.bc.clientecobis.adapter;

import static org.hamcrest.CoreMatchers.instanceOf;
import static org.mockito.Mockito.when;

import java.util.Date;

import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.MockitoJUnitRunner;
import org.springframework.boot.test.context.SpringBootTest;

import com.bgeneral.ce.bc.clientecobis.adapter.dto.ReferenciaClienteAdapterDTO;
import com.bgeneral.ce.bc.clientecobis.adapter.mapper.IAmMapperJsonDTO;
import com.bgeneral.ce.bc.clientecobis.common.dto.BCRequest;
import com.bgeneral.ce.bc.clientecobis.common.dto.BCResponse;
import com.bgeneral.ce.bc.clientecobis.common.dto.EmptyBodyT;
import com.bgeneral.ce.bc.clientecobis.common.dto.Header;
import com.bgeneral.ce.bc.clientecobis.common.dto.Perfil;
import com.bgeneral.ce.bc.clientecobis.common.dto.ReturnStatus;
import com.bgeneral.ce.bc.clientecobis.common.dto.Status;
import com.bgeneral.ce.bc.clientecobis.common.util.catalogs.CodeCatalog;
import com.bgeneral.ce.bc.clientecobis.handler.dto.ReferenciaClienteDTO;
import com.bgeneral.ce.bc.clientecobis.util.MockServer;

/**
 * 
 * @author jolivero
 *
 */

@SpringBootTest
@RunWith(MockitoJUnitRunner.class)
public class ConsultarReferenciaClienteAdapterTest extends MockServer {

	@Mock
	private IAmMapperJsonDTO<EmptyBodyT, ReferenciaClienteDTO, EmptyBodyT, ReferenciaClienteAdapterDTO> mapper;

	@InjectMocks
	private ConsultarReferenciaClienteAdapter sut;

	@Test
	public void shouldConnectToLegacy() {
		String jsonString = "{\r\n" + "  \"header\": {\r\n" + "    \"numeroUnico\": 0,\r\n" + "    \"oficina\": 0,\r\n"
				+ "    \"terminal\": \"string\",\r\n" + "    \"usuario\": \"string\",\r\n"
				+ "    \"rolUsuario\": 0,\r\n" + "    \"aplicacion\": \"string\",\r\n"
				+ "    \"canal\": \"string\",\r\n" + "    \"fecha\": \"2019-01-22T17:25:58.623Z\",\r\n"
				+ "    \"nodo\": 0,\r\n" + "    \"sesionBg\": \"string\",\r\n" + "    \"direccionIp\": \"string\",\r\n"
				+ "    \"origenSolicitud\": \"string\",\r\n" + "    \"numeroReferencia\": \"string\",\r\n"
				+ "    \"horaInicio\": \"string\",\r\n" + "    \"perfil\": {\r\n" + "      \"login\": \"string\",\r\n"
				+ "      \"numeroCliente\": 0,\r\n" + "      \"numeroBancaVirtual\": 0,\r\n"
				+ "      \"alias\": \"string\",\r\n" + "      \"numeroPerfil\": 0,\r\n"
				+ "      \"numeroClienteUsuario\": 0,\r\n" + "      \"vistaAsociada\": 0\r\n" + "    }\r\n" + "  },\r\n"
				+ "  \"status\": {\r\n" + "    \"returnStatus\": {\r\n" + "      \"returnCode\": \"string\",\r\n"
				+ "      \"returnCodeDesc\": \"string\"\r\n" + "    }\r\n" + "  },\r\n" + "  \"body\": {\r\n"
				+ "    \"numeroRegistros\": 1,\r\n" + "    \"cliente\": [\r\n" + "      {\r\n"
				+ "        \"numeroCliente\": 0,\r\n" + "        \"nombreCompleto\": \"string\",\r\n"
				+ "        \"secuencia\": 0,\r\n" + "        \"referencia\": 0,\r\n"
				+ "        \"tiporeferencia\": {\r\n" + "          \"codigo\": \"string\",\r\n"
				+ "          \"descripcion\": \"string\"\r\n" + "        },\r\n" + "        \"motivo\": {\r\n"
				+ "          \"codigo\": \"string\",\r\n" + "          \"descripcion\": \"string\"\r\n"
				+ "        },\r\n" + "        \"observacion\": \"string\",\r\n" + "        \"calificacion\": {\r\n"
				+ "          \"codigo\": \"string\",\r\n" + "          \"descripcion\": \"string\"\r\n"
				+ "        },\r\n" + "        \"cuenta\": \"string\",\r\n" + "        \"fechaInicio\": \"string\",\r\n"
				+ "        \"fechaVencimiento\": \"string\",\r\n" + "        \"fechaCierre\": \"string\",\r\n"
				+ "        \"banco\": {\r\n" + "          \"codigo\": \"string\",\r\n"
				+ "          \"descripcion\": \"string\"\r\n" + "        },\r\n"
				+ "        \"fechaRegistro\": \"string\",\r\n" + "        \"fechaModificacion\": \"string\",\r\n"
				+ "        \"usuarioLogin\": \"string\",\r\n" + "        \"usuarioNombre\": \"string\"\r\n"
				+ "      }\r\n" + "    ]\r\n" + "  }\r\n" + "}";
		setUpMockResponse(jsonString);
		modifyPropertyHost();

		when(mapper.adapterResponseTOHandlerResponseDTO(Mockito.any())).thenReturn(getResponseMapperDTO());

		BCResponse<ReferenciaClienteDTO> bcResponse = sut.callRestService(getRequestInServices());

		Assert.assertNotNull(bcResponse);
		Assert.assertThat(bcResponse.getStatus().getReturnStatus().getReturnCode(), instanceOf(String.class));
		Assert.assertEquals(bcResponse.getBody().isTieneReferencia(), true);

	}

	@Test(expected = IllegalArgumentException.class)
	public void validateStatusReturnStatusNull() {
		String jsonString = "{\r\n" + "  \"header\": {\r\n" + "    \"numeroUnico\": 0,\r\n" + "    \"oficina\": 0,\r\n"
				+ "    \"terminal\": \"string\",\r\n" + "    \"usuario\": \"string\",\r\n"
				+ "    \"rolUsuario\": 0,\r\n" + "    \"aplicacion\": \"string\",\r\n"
				+ "    \"canal\": \"string\",\r\n" + "    \"fecha\": \"2019-01-22T17:25:58.623Z\",\r\n"
				+ "    \"nodo\": 0,\r\n" + "    \"sesionBg\": \"string\",\r\n" + "    \"direccionIp\": \"string\",\r\n"
				+ "    \"origenSolicitud\": \"string\",\r\n" + "    \"numeroReferencia\": \"string\",\r\n"
				+ "    \"horaInicio\": \"string\",\r\n" + "    \"perfil\": {\r\n" + "      \"login\": \"string\",\r\n"
				+ "      \"numeroCliente\": 0,\r\n" + "      \"numeroBancaVirtual\": 0,\r\n"
				+ "      \"alias\": \"string\",\r\n" + "      \"numeroPerfil\": 0,\r\n"
				+ "      \"numeroClienteUsuario\": 0,\r\n" + "      \"vistaAsociada\": 0\r\n" + "    }\r\n" + "  },\r\n"
				+ "  \"status\": {\r\n" + "    \"returnStatus\": {\r\n" + "      \"returnCode\": \"string\",\r\n"
				+ "      \"returnCodeDesc\": \"string\"\r\n" + "    }\r\n" + "  },\r\n" + "  \"body\": {\r\n"
				+ "    \"numeroRegistros\": 0,\r\n" + "    \"cliente\": [\r\n" + "      {\r\n"
				+ "        \"numeroCliente\": 0,\r\n" + "        \"nombreCompleto\": \"string\",\r\n"
				+ "        \"secuencia\": 0,\r\n" + "        \"referencia\": 0,\r\n"
				+ "        \"tiporeferencia\": {\r\n" + "          \"codigo\": \"string\",\r\n"
				+ "          \"descripcion\": \"string\"\r\n" + "        },\r\n" + "        \"motivo\": {\r\n"
				+ "          \"codigo\": \"string\",\r\n" + "          \"descripcion\": \"string\"\r\n"
				+ "        },\r\n" + "        \"observacion\": \"string\",\r\n" + "        \"calificacion\": {\r\n"
				+ "          \"codigo\": \"string\",\r\n" + "          \"descripcion\": \"string\"\r\n"
				+ "        },\r\n" + "        \"cuenta\": \"string\",\r\n" + "        \"fechaInicio\": \"string\",\r\n"
				+ "        \"fechaVencimiento\": \"string\",\r\n" + "        \"fechaCierre\": \"string\",\r\n"
				+ "        \"banco\": {\r\n" + "          \"codigo\": \"string\",\r\n"
				+ "          \"descripcion\": \"string\"\r\n" + "        },\r\n"
				+ "        \"fechaRegistro\": \"string\",\r\n" + "        \"fechaModificacion\": \"string\",\r\n"
				+ "        \"usuarioLogin\": \"string\",\r\n" + "        \"usuarioNombre\": \"string\"\r\n"
				+ "      }\r\n" + "    ]\r\n" + "  }\r\n" + "}";
		setUpMockResponse(jsonString);
		modifyPropertyHost();

		BCResponse<ReferenciaClienteDTO> mapperResponse = getResponseMapperDTO();
		mapperResponse.setStatus(null);
		when(mapper.adapterResponseTOHandlerResponseDTO(Mockito.any())).thenReturn(mapperResponse);
		sut.callRestService(getRequestInServices());
	}

	@Test(expected = IllegalArgumentException.class)
	public void validateStatusReturnNull() {
		String jsonString = "{\r\n" + "  \"header\": {\r\n" + "    \"numeroUnico\": 0,\r\n" + "    \"oficina\": 0,\r\n"
				+ "    \"terminal\": \"string\",\r\n" + "    \"usuario\": \"string\",\r\n"
				+ "    \"rolUsuario\": 0,\r\n" + "    \"aplicacion\": \"string\",\r\n"
				+ "    \"canal\": \"string\",\r\n" + "    \"fecha\": \"2019-01-22T17:25:58.623Z\",\r\n"
				+ "    \"nodo\": 0,\r\n" + "    \"sesionBg\": \"string\",\r\n" + "    \"direccionIp\": \"string\",\r\n"
				+ "    \"origenSolicitud\": \"string\",\r\n" + "    \"numeroReferencia\": \"string\",\r\n"
				+ "    \"horaInicio\": \"string\",\r\n" + "    \"perfil\": {\r\n" + "      \"login\": \"string\",\r\n"
				+ "      \"numeroCliente\": 0,\r\n" + "      \"numeroBancaVirtual\": 0,\r\n"
				+ "      \"alias\": \"string\",\r\n" + "      \"numeroPerfil\": 0,\r\n"
				+ "      \"numeroClienteUsuario\": 0,\r\n" + "      \"vistaAsociada\": 0\r\n" + "    }\r\n" + "  },\r\n"
				+ "  \"status\": {\r\n" + "    \"returnStatus\": {\r\n" + "      \"returnCode\": \"string\",\r\n"
				+ "      \"returnCodeDesc\": \"string\"\r\n" + "    }\r\n" + "  },\r\n" + "  \"body\": {\r\n"
				+ "    \"numeroRegistros\": 0,\r\n" + "    \"cliente\": [\r\n" + "      {\r\n"
				+ "        \"numeroCliente\": 0,\r\n" + "        \"nombreCompleto\": \"string\",\r\n"
				+ "        \"secuencia\": 0,\r\n" + "        \"referencia\": 0,\r\n"
				+ "        \"tiporeferencia\": {\r\n" + "          \"codigo\": \"string\",\r\n"
				+ "          \"descripcion\": \"string\"\r\n" + "        },\r\n" + "        \"motivo\": {\r\n"
				+ "          \"codigo\": \"string\",\r\n" + "          \"descripcion\": \"string\"\r\n"
				+ "        },\r\n" + "        \"observacion\": \"string\",\r\n" + "        \"calificacion\": {\r\n"
				+ "          \"codigo\": \"string\",\r\n" + "          \"descripcion\": \"string\"\r\n"
				+ "        },\r\n" + "        \"cuenta\": \"string\",\r\n" + "        \"fechaInicio\": \"string\",\r\n"
				+ "        \"fechaVencimiento\": \"string\",\r\n" + "        \"fechaCierre\": \"string\",\r\n"
				+ "        \"banco\": {\r\n" + "          \"codigo\": \"string\",\r\n"
				+ "          \"descripcion\": \"string\"\r\n" + "        },\r\n"
				+ "        \"fechaRegistro\": \"string\",\r\n" + "        \"fechaModificacion\": \"string\",\r\n"
				+ "        \"usuarioLogin\": \"string\",\r\n" + "        \"usuarioNombre\": \"string\"\r\n"
				+ "      }\r\n" + "    ]\r\n" + "  }\r\n" + "}";
		setUpMockResponse(jsonString);
		modifyPropertyHost();

		BCResponse<ReferenciaClienteDTO> mapperResponse = getResponseMapperDTO();
		mapperResponse.setStatus(null);
		when(mapper.adapterResponseTOHandlerResponseDTO(Mockito.any())).thenReturn(mapperResponse);
		sut.callRestService(getRequestInServices());
	}

	private BCRequest<EmptyBodyT> getRequestInServices() {
		BCRequest<EmptyBodyT> request = new BCRequest<>();
		request.setHeader(this.getHeader());
		return request;
	}

	private BCResponse<ReferenciaClienteDTO> getResponseMapperDTO() {
		BCResponse<ReferenciaClienteDTO> bcResponse = new BCResponse<>();
		bcResponse.setBody(loadBody());
		bcResponse.setHeader(this.getHeader());
		bcResponse.setStatus(this.getStatus());
		return bcResponse;
	}

	private ReferenciaClienteDTO loadBody() {
		ReferenciaClienteDTO body = new ReferenciaClienteDTO();
		body.setTieneReferencia(true);
		return body;
	}

	private Header getHeader() {
		Header headerRest = new Header();
		// Cabecera
		headerRest.setNumeroUnico(1);
		headerRest.setOficina(1);
		headerRest.setTerminal("1");
		headerRest.setUsuario("bccanales");
		headerRest.setRolUsuario(3);
		// CabeceraCanales
		headerRest.setAplicacion("1");
		headerRest.setCanal("1");
		headerRest.setFecha(new Date());
		headerRest.setNodo(1);
		headerRest.setSesionBg("1234");
		headerRest.setDireccionIp("");
		headerRest.setOrigenSolicitud("bel");
		headerRest.setNumeroReferencia("1234");
		headerRest.setHoraInicio("12:00:00");

		// Perfil
		Perfil perfil = new Perfil();
		perfil.setLogin("");
		perfil.setNumeroCliente(12);
		perfil.setNumeroBancaVirtual(0);
		perfil.setAlias("prueba");
		perfil.setNumeroPerfil(3);

		headerRest.setPerfil(perfil);
		return headerRest;
	}

	private Status getStatus() {
		Status status = new Status();
		ReturnStatus returnStatus = new ReturnStatus();
		returnStatus.setReturnCode(CodeCatalog.CODE_SUCCESS.codeS());
		returnStatus.setReturnCodeDesc(CodeCatalog.S_MSG_CODE_SUCCESS.codeS());
		status.setReturnStatus(returnStatus);
		return status;
	}

}
