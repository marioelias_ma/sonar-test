package com.bgeneral.ce.bc.clientecobis.adapter.mapper;

import static org.hamcrest.CoreMatchers.equalTo;
import static org.hamcrest.CoreMatchers.instanceOf;
import static org.junit.Assert.assertThat;

import java.util.Date;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.junit.MockitoJUnitRunner;

import com.bgeneral.ce.bc.clientecobis.adapter.dto.DatosClienteCobisDTO;
import com.bgeneral.ce.bc.clientecobis.adapter.dto.DatosDireccionClienteCobisDTO;
import com.bgeneral.ce.bc.clientecobis.adapter.mapper.impl.ConsultarDatosClienteCobisMapper;
import com.bgeneral.ce.bc.clientecobis.common.dto.BCResponse;
import com.bgeneral.ce.bc.clientecobis.common.dto.Catalogo;
import com.bgeneral.ce.bc.clientecobis.common.dto.Header;
import com.bgeneral.ce.bc.clientecobis.common.dto.Perfil;
import com.bgeneral.ce.bc.clientecobis.common.dto.ReturnStatus;
import com.bgeneral.ce.bc.clientecobis.common.dto.Status;
import com.bgeneral.ce.bc.clientecobis.common.util.catalogs.CodeCatalog;
import com.bgeneral.ce.bc.clientecobis.handler.dto.DatosClienteHandlerDTO;
import com.bgeneral.ce.bc.clientecobis.handler.dto.DatosDireccionClienteCobisHandlerDTO;

@RunWith(MockitoJUnitRunner.class)
public class ConsultarDatosClienteCobisMapperTest {

	// DatosDireccionClienteCobisHandlerDTO, DatosDireccionClienteCobisDTO
	@InjectMocks
	private ConsultarDatosClienteCobisMapper sut;

	private BCResponse<DatosClienteCobisDTO> validLoadResponse() {
		BCResponse<DatosClienteCobisDTO> bcResponse = new BCResponse<>();
		bcResponse.setHeader(this.getHeader());
		bcResponse.setStatus(this.getStatus());
		bcResponse.setBody(this.getBody());
		return bcResponse;
	}

	/*
	 * Testing Mapper to Response
	 * 
	 */
	@Test
	public void buildHandlerRequestTOAdapterResponse() {
		BCResponse<DatosClienteCobisDTO> bcResponse = validLoadResponse();
		sut.adapterResponseTOHandlerResponseDTO(bcResponse);
	}

	@Test
	public void validateValueResponseDataType() {
		BCResponse<DatosClienteCobisDTO> bcResponse = validLoadResponse();
		assertThat(sut.adapterResponseTOHandlerResponseDTO(bcResponse).getBody(),
				instanceOf(DatosClienteHandlerDTO.class));
	}

	@Test(expected = NullPointerException.class)
	public void validateStatusResponseNull() {
		BCResponse<DatosClienteCobisDTO> bcResponse = validLoadResponse();
		bcResponse.getStatus().setReturnStatus(null);
		sut.adapterResponseTOHandlerResponseDTO(bcResponse);
	}

	@Test
	public void validateValueBodyResponseNull() {
		BCResponse<DatosClienteCobisDTO> bcResponse = validLoadResponse();
		bcResponse.getStatus().getReturnStatus().setReturnCode("U0002");
		bcResponse.getStatus().getReturnStatus().setReturnCodeDesc("FALLO");
		BCResponse<DatosClienteHandlerDTO> response = sut.adapterResponseTOHandlerResponseDTO(bcResponse);

		assertThat(response.getBody(), equalTo(null));
	}

	@Test
	public void validateValueInHeaderResponse() {
		BCResponse<DatosClienteCobisDTO> bcResponse = validLoadResponse();
		BCResponse<DatosClienteHandlerDTO> response = sut.adapterResponseTOHandlerResponseDTO(bcResponse);

		assertThat(response.getHeader().getCanal(), instanceOf(String.class));
		assertThat(response.getHeader().getCanal(), equalTo("1"));
	}

	@Test
	public void validateValueInBodyResponse() {
		BCResponse<DatosClienteCobisDTO> bcResponse = validLoadResponse();
		BCResponse<DatosClienteHandlerDTO> response = sut.adapterResponseTOHandlerResponseDTO(bcResponse);

//		assertThat(response.getBody().getCalle(), instanceOf(String.class));
//		assertThat(response.getBody().getCalle(), equalTo("CALLE"));
	}

	private DatosClienteCobisDTO getBody() {
		DatosClienteCobisDTO body = new DatosClienteCobisDTO();

		body.setNombre("MARIA");
		body.setSegundonombre("EVA");
		body.setApellidoPaterno("CARABALLO");
		body.setApellidoMaterno("GONZALEZ");
		body.setApellidoCasada("GUDINO");
		body.setCedula("09  009600475");
		body.setEdad(62);
		body.setAutorizacionAPC(true);
		body.setCantidadCuentasPasivas(2);
		body.setCodigoMalaRef("00");
		body.setCodigoOficial(1035);
		body.setComentario("PRESTAMO Personal");
		body.setDescripcionOficial("Oficial de sucursal");
		body.setDifunto("S");
		body.setFechaExp(new Date());
		body.setFechaIngreso(new Date());
		body.setFechaInicioRelacion("01/01/2000");
		body.setFechaNacimiento(new Date());
		body.setFechaRegistro(new Date());
		body.setFechaUltimaModificacion(new Date());
		body.setFechaVerificacion(new Date());
		body.setGrupoRel("S");
		
		Catalogo catalogo = new Catalogo();

		catalogo.setCodigo("00");
		catalogo.setDescripcion("COSTURERA");
		body.setActividad(catalogo);		

		catalogo.setCodigo("00");
		catalogo.setDescripcion("COSTURERA");
		body.setClasificacion(catalogo);
		
		catalogo.setCodigo("00");
		catalogo.setDescripcion("COSTURERA");
		body.setClienteBanca(catalogo);
		
		catalogo.setCodigo("00");
		catalogo.setDescripcion("COSTURERA");
		body.setCodigoCalificacion(catalogo);
		
		catalogo.setCodigo("00");
		catalogo.setDescripcion("COSTURERA");
		body.setEducacion(catalogo);
		
		catalogo.setCodigo("00");
		catalogo.setDescripcion("COSTURERA");		
		body.setEstadoCivil(catalogo);

		catalogo.setCodigo("00");
		catalogo.setDescripcion("COSTURERA");		
		body.setEstatus(catalogo);

		catalogo.setCodigo("00");
		catalogo.setDescripcion("COSTURERA");		
		body.setEvaluacionCumplimiento(catalogo);
		
		catalogo.setCodigo("00");
		catalogo.setDescripcion("COSTURERA");		
		body.setGrupo(catalogo);

		catalogo.setCodigo("00");
		catalogo.setDescripcion("COSTURERA");		
		body.setOficialSector(catalogo);

		catalogo.setCodigo("00");
		catalogo.setDescripcion("COSTURERA");		
		body.setOficialSubSector(catalogo);

		catalogo.setCodigo("00");
		catalogo.setDescripcion("COSTURERA");		
		body.setOficialTipo(catalogo);

		catalogo.setCodigo("00");
		catalogo.setDescripcion("COSTURERA");		
		body.setPaisNacimiento(catalogo);

		catalogo.setCodigo("00");
		catalogo.setDescripcion("COSTURERA");		
		body.setPaisRiesgo(catalogo);

		catalogo.setCodigo("00");
		catalogo.setDescripcion("COSTURERA");
		body.setProfesion(catalogo);
		
		catalogo.setCodigo("00");
		catalogo.setDescripcion("COSTURERA");		
		body.setRelacion(catalogo);
		
		catalogo.setCodigo("00");
		catalogo.setDescripcion("COSTURERA");		
		body.setSexo(catalogo);
		
		catalogo.setCodigo("00");
		catalogo.setDescripcion("COSTURERA");		
		body.setSubClasificacion(catalogo);

		catalogo.setCodigo("00");
		catalogo.setDescripcion("COSTURERA");		
		body.setTipoPersona(catalogo);
		
		catalogo.setCodigo("00");
		catalogo.setDescripcion("COSTURERA");		
		body.setUsuario(catalogo);

		return body;
	}

	private Header getHeader() {
		Header headerRest = new Header();
		// Cabecera
		headerRest.setNumeroUnico(1);
		headerRest.setOficina(1);
		headerRest.setTerminal("1");
		headerRest.setUsuario("bccanales");
		headerRest.setRolUsuario(3);
		// CabeceraCanales
		headerRest.setAplicacion("1");
		headerRest.setCanal("1");
		headerRest.setFecha(new Date());
		headerRest.setNodo(1);
		headerRest.setSesionBg("1234");
		headerRest.setDireccionIp("");
		headerRest.setOrigenSolicitud("bel");
		headerRest.setNumeroReferencia("1234");
		headerRest.setHoraInicio("12:00:00");

		// Perfil
		Perfil perfil = new Perfil();
		perfil.setLogin("");
		perfil.setNumeroCliente(12);
		perfil.setNumeroBancaVirtual(0);
		perfil.setAlias("prueba");
		perfil.setNumeroPerfil(3);

		headerRest.setPerfil(perfil);
		return headerRest;
	}

	private Status getStatus() {
		Status status = new Status();
		ReturnStatus returnStatus = new ReturnStatus();
		returnStatus.setReturnCode(CodeCatalog.CODE_SUCCESS.codeS());
		returnStatus.setReturnCodeDesc(CodeCatalog.S_MSG_CODE_SUCCESS.codeS());
		status.setReturnStatus(returnStatus);
		return status;
	}

}

