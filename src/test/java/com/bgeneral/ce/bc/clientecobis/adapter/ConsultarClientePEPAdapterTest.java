package com.bgeneral.ce.bc.clientecobis.adapter;

import static org.hamcrest.CoreMatchers.instanceOf;
import static org.mockito.Mockito.when;

import java.util.Date;

import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.MockitoJUnitRunner;
import org.springframework.boot.test.context.SpringBootTest;

import com.bgeneral.ce.bc.clientecobis.adapter.dto.ConsultaClientePEP;
import com.bgeneral.ce.bc.clientecobis.adapter.mapper.IAmMapperJsonDTO;
import com.bgeneral.ce.bc.clientecobis.common.dto.BCRequest;
import com.bgeneral.ce.bc.clientecobis.common.dto.BCResponse;
import com.bgeneral.ce.bc.clientecobis.common.dto.EmptyBodyT;
import com.bgeneral.ce.bc.clientecobis.common.dto.Header;
import com.bgeneral.ce.bc.clientecobis.common.dto.Perfil;
import com.bgeneral.ce.bc.clientecobis.common.dto.ReturnStatus;
import com.bgeneral.ce.bc.clientecobis.common.dto.Status;
import com.bgeneral.ce.bc.clientecobis.common.util.catalogs.CodeCatalog;
import com.bgeneral.ce.bc.clientecobis.handler.dto.ClientePEPDTO;
import com.bgeneral.ce.bc.clientecobis.util.MockServer;

/**
 * 
 * @author anlugo
 *
 */

@SpringBootTest
@RunWith(MockitoJUnitRunner.class)
public class ConsultarClientePEPAdapterTest extends MockServer{
	
	@Mock
	private IAmMapperJsonDTO<EmptyBodyT, ClientePEPDTO, EmptyBodyT, ConsultaClientePEP> mapper;
	
	@InjectMocks
	private ConsultarClientePEPAdapter sut;
	
	@Test
	public void shouldConnectToLegacy(){
		String jsonString = "{\r\n" + 
				"	\"header \": {\r\n" + 
				"	\"numeroUnico \": 1,\r\n" + 
				"	\"oficina \": 1,\r\n" + 
				"	\"terminal \": \"LaBandera \",\r\n" + 
				"	\"usuario \": \"Chepa \",\r\n" + 
				"	\"rolUsuario \": 1,\r\n" + 
				"	\"aplicacion \": \"tintenegro \",\r\n" + 
				"	\"canal \": \"ESPN \",\r\n" + 
				"	\"fecha \": \"2019-01-10T21:21:06.967Z \",\r\n" + 
				"	\"nodo \": 1,\r\n" + 
				"	\"sesionBg \": \"SDFsdfgsdfGSERgsdfgSDfgsdfgSDF \",\r\n" + 
				"	\"direccionIp \": \"10.2.10.2 \",\r\n" + 
				"	\"origenSolicitud \": \"INICIO \",\r\n" + 
				"	\"numeroReferencia \": \"5555 \",\r\n" + 
				"	\"horaInicio \": \"10:10:10 \",\r\n" + 
				"	\"perfil \": {\r\n" + 
				"	\"login \": \"1114155 \",\r\n" + 
				"	\"numeroCliente \": 1114155,\r\n" + 
				"	\"numeroBancaVirtual \": 123456,\r\n" + 
				"	\"alias \": \"ERCARTELUO \",\r\n" + 
				"	\"numeroPerfil \": 2,\r\n" + 
				"	\"numeroClienteUsuario \": 3,\r\n" + 
				"	\"vistaAsociada \": 1\r\n" + 
				"	}\r\n" + 
				"	},\r\n" + 
				"	\"body \": {\r\n" + 
				"	\"esPEP\":true \r\n" + 
				" }\r\n" + 
				"}"; 
		setUpMockResponse(jsonString);
		modifyPropertyHost();

		when(mapper.adapterResponseTOHandlerResponseDTO(Mockito.any())).thenReturn(getResponseMapperDTO());
		
		BCResponse<ClientePEPDTO> bcResponse = sut.callRestService(getRequestInServices());

		Assert.assertNotNull(bcResponse);
		Assert.assertThat(bcResponse.getStatus().getReturnStatus().getReturnCode(), instanceOf(String.class));
	}

	@Test(expected = IllegalArgumentException.class)
	public void validateStatusReturnStatusNull(){
		String jsonString = "{\r\n" + 
				"	\"header \": {\r\n" + 
				"	\"numeroUnico \": 1,\r\n" + 
				"	\"oficina \": 1,\r\n" + 
				"	\"terminal \": \"LaBandera \",\r\n" + 
				"	\"usuario \": \"Chepa \",\r\n" + 
				"	\"rolUsuario \": 1,\r\n" + 
				"	\"aplicacion \": \"tintenegro \",\r\n" + 
				"	\"canal \": \"ESPN \",\r\n" + 
				"	\"fecha \": \"2019-01-10T21:21:06.967Z \",\r\n" + 
				"	\"nodo \": 1,\r\n" + 
				"	\"sesionBg \": \"SDFsdfgsdfGSERgsdfgSDfgsdfgSDF \",\r\n" + 
				"	\"direccionIp \": \"10.2.10.2 \",\r\n" + 
				"	\"origenSolicitud \": \"INICIO \",\r\n" + 
				"	\"numeroReferencia \": \"5555 \",\r\n" + 
				"	\"horaInicio \": \"10:10:10 \",\r\n" + 
				"	\"perfil \": {\r\n" + 
				"	\"login \": \"1114155 \",\r\n" + 
				"	\"numeroCliente \": 1114155,\r\n" + 
				"	\"numeroBancaVirtual \": 123456,\r\n" + 
				"	\"alias \": \"ERCARTELUO \",\r\n" + 
				"	\"numeroPerfil \": 2,\r\n" + 
				"	\"numeroClienteUsuario \": 3,\r\n" + 
				"	\"vistaAsociada \": 1\r\n" + 
				"	}\r\n" + 
				"	},\r\n" + 
				"	\"body \": {\r\n" + 
				"	\"esPEP\":true \r\n" + 
				" }\r\n" + 
				"}"; 
		setUpMockResponse(jsonString);
		modifyPropertyHost();
		
		BCResponse<ClientePEPDTO> mapperResponse = getResponseMapperDTO();
		mapperResponse.getStatus().setReturnStatus(null);
		when(mapper.adapterResponseTOHandlerResponseDTO(Mockito.any())).thenReturn(mapperResponse);
		sut.callRestService(getRequestInServices());
	}
	
	@Test(expected = NullPointerException.class)
	public void validateStatusReturnNull(){
		String jsonString = "{\r\n" + 
				"	\"header \": {\r\n" + 
				"	\"numeroUnico \": 1,\r\n" + 
				"	\"oficina \": 1,\r\n" + 
				"	\"terminal \": \"LaBandera \",\r\n" + 
				"	\"usuario \": \"Chepa \",\r\n" + 
				"	\"rolUsuario \": 1,\r\n" + 
				"	\"aplicacion \": \"tintenegro \",\r\n" + 
				"	\"canal \": \"ESPN \",\r\n" + 
				"	\"fecha \": \"2019-01-10T21:21:06.967Z \",\r\n" + 
				"	\"nodo \": 1,\r\n" + 
				"	\"sesionBg \": \"SDFsdfgsdfGSERgsdfgSDfgsdfgSDF \",\r\n" + 
				"	\"direccionIp \": \"10.2.10.2 \",\r\n" + 
				"	\"origenSolicitud \": \"INICIO \",\r\n" + 
				"	\"numeroReferencia \": \"5555 \",\r\n" + 
				"	\"horaInicio \": \"10:10:10 \",\r\n" + 
				"	\"perfil \": {\r\n" + 
				"	\"login \": \"1114155 \",\r\n" + 
				"	\"numeroCliente \": 1114155,\r\n" + 
				"	\"numeroBancaVirtual \": 123456,\r\n" + 
				"	\"alias \": \"ERCARTELUO \",\r\n" + 
				"	\"numeroPerfil \": 2,\r\n" + 
				"	\"numeroClienteUsuario \": 3,\r\n" + 
				"	\"vistaAsociada \": 1\r\n" + 
				"	}\r\n" + 
				"	},\r\n" + 
				"	\"body \": {\r\n" + 
				"	\"esPEP\":true \r\n" + 
				" }\r\n" + 
				"}"; 
		setUpMockResponse(jsonString);
		modifyPropertyHost();
		
		BCResponse<ClientePEPDTO> mapperResponse = getResponseMapperDTO();
		mapperResponse.setStatus(null);
		when(mapper.adapterResponseTOHandlerResponseDTO(Mockito.any())).thenReturn(mapperResponse);
		sut.callRestService(getRequestInServices());
	}
	
	private BCRequest<EmptyBodyT> getRequestInServices() {
		BCRequest<EmptyBodyT> request = new BCRequest<>();
		request.setHeader(this.getHeader());
		return request;
	}

	
	private BCResponse<ClientePEPDTO> getResponseMapperDTO() {
		BCResponse<ClientePEPDTO> bcResponse = new BCResponse<>();
		bcResponse.setBody(loadBody());
		bcResponse.setHeader(this.getHeader());
		bcResponse.setStatus(this.getStatus());
		return bcResponse;
	}
	
	private ClientePEPDTO loadBody() {
		ClientePEPDTO body = new ClientePEPDTO();
		body.setEsPEP(true);		
		return body;
	}
	
	private Header getHeader() {
		Header headerRest = new Header();
		// Cabecera
		headerRest.setNumeroUnico(1);
		headerRest.setOficina(1);
		headerRest.setTerminal("1");
		headerRest.setUsuario("bccanales");
		headerRest.setRolUsuario(3);
		// CabeceraCanales
		headerRest.setAplicacion("1");
		headerRest.setCanal("1");
		headerRest.setFecha(new Date());
		headerRest.setNodo(1);
		headerRest.setSesionBg("1234");
		headerRest.setDireccionIp("");
		headerRest.setOrigenSolicitud("bel");
		headerRest.setNumeroReferencia("1234");
		headerRest.setHoraInicio("12:00:00");

		// Perfil
		Perfil perfil = new Perfil();
		perfil.setLogin("");
		perfil.setNumeroCliente(12);
		perfil.setNumeroBancaVirtual(0);
		perfil.setAlias("prueba");
		perfil.setNumeroPerfil(3);

		headerRest.setPerfil(perfil);
		return headerRest;
	}

	private Status getStatus() {
		Status status = new Status();
		ReturnStatus returnStatus = new ReturnStatus();
		returnStatus.setReturnCode(CodeCatalog.CODE_SUCCESS.codeS());
		returnStatus.setReturnCodeDesc(CodeCatalog.S_MSG_CODE_SUCCESS.codeS());
		status.setReturnStatus(returnStatus);
		return status;
	}

}
