package com.bgeneral.ce.bc.clientecobis.adapter;

import static org.hamcrest.CoreMatchers.instanceOf;
import static org.mockito.Mockito.when;

import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.MockitoJUnitRunner;
import org.springframework.boot.test.context.SpringBootTest;

import com.bgeneral.ce.bc.clientecobis.adapter.dto.TelefonoClienteCobisDTO;
import com.bgeneral.ce.bc.clientecobis.adapter.dto.ValorTelefonoCobisDTO;
import com.bgeneral.ce.bc.clientecobis.adapter.mapper.IAmMapperJsonDTO;
import com.bgeneral.ce.bc.clientecobis.common.dto.BCRequest;
import com.bgeneral.ce.bc.clientecobis.common.dto.BCResponse;
import com.bgeneral.ce.bc.clientecobis.common.dto.Catalogo;
import com.bgeneral.ce.bc.clientecobis.common.dto.ReturnStatus;
import com.bgeneral.ce.bc.clientecobis.handler.dto.TelefonoClienteDTO;
import com.bgeneral.ce.bc.clientecobis.handler.dto.ValorTelefonoDTO;
import com.bgeneral.ce.bc.clientecobis.util.MockDummyRequest;
import com.bgeneral.ce.bc.clientecobis.util.MockDummyResponse;
import com.bgeneral.ce.bc.clientecobis.util.MockServer;

/**
 * @author jnieves
 *
 */
@SpringBootTest
@RunWith(MockitoJUnitRunner.class)
public class CrearTelefonoClienteCobisAdapterTest extends MockServer {
	
	@Mock
	private IAmMapperJsonDTO<TelefonoClienteDTO, ValorTelefonoDTO, TelefonoClienteCobisDTO, ValorTelefonoCobisDTO> mapper;
	
	@InjectMocks
	private CrearTelefonoClienteCobisAdapter sut;
	
	
	@Test
	public void shouldConnectToLegacy(){
		String jsonString = "{\"header\":{\"numeroUnico\":123456,\"oficina\":62,\"terminal\":\"twen\",\"usuario\":\"twen\",\"rolUsuario\":1,\"aplicacion\":\"1\",\"canal\":\"1\",\"fecha\":\"2018-10-22T21:12:52.655Z\",\"nodo\":1,\"sesionBg\":\"\",\"direccionIp\":\"1.9.7.6\",\"origenSolicitud\":\"BEL\",\"numeroReferencia\":\"\",\"horaInicio\":\"\",\"perfil\":{\"login\":\"\",\"numeroCliente\":104953,\"numeroBancaVirtual\":0,\"alias\":\"yonofui\",\"numeroPerfil\":3,\"numeroClienteUsuario\":1,\"vistaAsociada\":1}},\"body\":{\"codigoDireccion\":\"6999\"},\"status\":{\"returnStatus\":{\"returnCode\":\"U0000\",\"returnCodeDesc\":\"Se ha realizado la ejecucion del servicio correctamente.\"}}}"; 
		setUpMockResponse(jsonString);
		modifyPropertyHost();

		when(mapper.handlerRequestTOAdapterRequestDTO(Mockito.any())).thenReturn(getRequestMapperDTO());
		when(mapper.adapterResponseTOHandlerResponseDTO(Mockito.any())).thenReturn(getResponseMapperDTO());
		
		BCResponse<ValorTelefonoDTO> bcResponse = sut.callRestService(getRequestInServices());

		Assert.assertNotNull(bcResponse);
		Assert.assertThat(bcResponse.getStatus().getReturnStatus().getReturnCode(), instanceOf(String.class));
	}
	
	@Test
	public void validateServicesWithResponseBodyNull(){
		String jsonString = "{\"header\":{\"numeroUnico\":123456,\"oficina\":62,\"terminal\":\"twen\",\"usuario\":\"twen\",\"rolUsuario\":1,\"aplicacion\":\"1\",\"canal\":\"1\",\"fecha\":\"2018-10-22T21:12:52.655Z\",\"nodo\":1,\"sesionBg\":\"\",\"direccionIp\":\"1.9.7.6\",\"origenSolicitud\":\"BEL\",\"numeroReferencia\":\"\",\"horaInicio\":\"\",\"perfil\":{\"login\":\"\",\"numeroCliente\":104953,\"numeroBancaVirtual\":0,\"alias\":\"yonofui\",\"numeroPerfil\":3,\"numeroClienteUsuario\":1,\"vistaAsociada\":1}},\"body\":{\"codigoDireccion\":\"6999\"},\"status\":{\"returnStatus\":{\"returnCode\":\"U0000\",\"returnCodeDesc\":\"Se ha realizado la ejecucion del servicio correctamente.\"}}}"; 
		setUpMockResponse(jsonString);
		modifyPropertyHost();

		when(mapper.handlerRequestTOAdapterRequestDTO(Mockito.any())).thenReturn(getRequestMapperDTO());
		 BCResponse<ValorTelefonoDTO> response = getResponseMapperDTO();
		 response.setBody(null);
		
		when(mapper.adapterResponseTOHandlerResponseDTO(Mockito.any())).thenReturn(response);
		
		BCResponse<ValorTelefonoDTO> bcResponse = sut.callRestService(getRequestInServices());

		Assert.assertNotNull(bcResponse);
		Assert.assertThat(bcResponse.getStatus().getReturnStatus().getReturnCode(), instanceOf(String.class));
	}
	
	@Test(expected = IllegalArgumentException.class)
	public void validateServicesWithResponseStatusNull(){
		String jsonString = "{\"header\":{\"numeroUnico\":123456,\"oficina\":62,\"terminal\":\"twen\",\"usuario\":\"twen\",\"rolUsuario\":1,\"aplicacion\":\"1\",\"canal\":\"1\",\"fecha\":\"2018-10-22T21:12:52.655Z\",\"nodo\":1,\"sesionBg\":\"\",\"direccionIp\":\"1.9.7.6\",\"origenSolicitud\":\"BEL\",\"numeroReferencia\":\"\",\"horaInicio\":\"\",\"perfil\":{\"login\":\"\",\"numeroCliente\":104953,\"numeroBancaVirtual\":0,\"alias\":\"yonofui\",\"numeroPerfil\":3,\"numeroClienteUsuario\":1,\"vistaAsociada\":1}},\"body\":{\"codigoDireccion\":\"6999\"},\"status\":{\"returnStatus\":{\"returnCode\":\"U0000\",\"returnCodeDesc\":\"Se ha realizado la ejecucion del servicio correctamente.\"}}}"; 
		setUpMockResponse(jsonString);
		modifyPropertyHost();

		when(mapper.handlerRequestTOAdapterRequestDTO(Mockito.any())).thenReturn(getRequestMapperDTO());
		 BCResponse<ValorTelefonoDTO> response = getResponseMapperDTO();
		 response.getStatus().setReturnStatus(null);
		
		when(mapper.adapterResponseTOHandlerResponseDTO(Mockito.any())).thenReturn(response);
		
		sut.callRestService(getRequestInServices());

	}
	
	@Test(expected = IllegalArgumentException.class)
	public void validateServicesWithRequestBodyNull(){		
		BCRequest<TelefonoClienteDTO> bcRequest = getRequestInServices();
		bcRequest.setBody(null);
		sut.callRestService(bcRequest);
	}
	
	
	/* Methods to load data in the DTOs */
	private BCRequest<TelefonoClienteCobisDTO> getRequestMapperDTO() {
		TelefonoClienteCobisDTO body = new TelefonoClienteCobisDTO();
		
		body.setExtencion("+507");
		body.setNumeroTelefono("65981245");
		body.setTipoTelefono("MIO");
		
		Catalogo catalogo = new Catalogo();
		catalogo.setCodigo("Direccion");
		catalogo.setDescripcion("Descripcion Direccion");
		body.setDireccion(catalogo);

		catalogo.setCodigo("Pais");
		catalogo.setDescripcion("Descripcion Pais");
		body.setPais(catalogo);
			
		BCRequest<TelefonoClienteCobisDTO> bcRequest = new MockDummyRequest<TelefonoClienteCobisDTO>().setBody(body).build();
		return bcRequest;
	}

	
	private BCResponse<ValorTelefonoDTO> getResponseMapperDTO() {
		ValorTelefonoDTO body = new ValorTelefonoDTO();
		body.setCodigoTelefono("123456");
		
		ReturnStatus returnStatus = new ReturnStatus();
		returnStatus.setReturnCode("U0000");
		returnStatus.setReturnCodeDesc("Se ha realizado al ejecucion con exito");
		
		return new MockDummyResponse<ValorTelefonoDTO>().setBody(body).setReturnStatus(returnStatus).build();
	}
	
	private BCRequest<TelefonoClienteDTO> getRequestInServices() {
		return new MockDummyRequest<TelefonoClienteDTO>().setBody(new TelefonoClienteDTO()).build();
	}

}