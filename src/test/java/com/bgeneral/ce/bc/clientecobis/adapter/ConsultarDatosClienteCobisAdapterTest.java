package com.bgeneral.ce.bc.clientecobis.adapter;

import static org.hamcrest.CoreMatchers.instanceOf;
import static org.mockito.Mockito.when;

import java.util.Date;

import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.MockitoJUnitRunner;
import org.springframework.boot.test.context.SpringBootTest;

import com.bgeneral.ce.bc.clientecobis.adapter.dto.CredencialesClienteCobisDTO;
import com.bgeneral.ce.bc.clientecobis.adapter.dto.DatosClienteCobisDTO;
import com.bgeneral.ce.bc.clientecobis.adapter.mapper.IAmMapperJsonDTO;
import com.bgeneral.ce.bc.clientecobis.common.dto.BCRequest;
import com.bgeneral.ce.bc.clientecobis.common.dto.BCResponse;
import com.bgeneral.ce.bc.clientecobis.common.dto.Catalogo;
import com.bgeneral.ce.bc.clientecobis.common.dto.Header;
import com.bgeneral.ce.bc.clientecobis.common.dto.Perfil;
import com.bgeneral.ce.bc.clientecobis.common.dto.ReturnStatus;
import com.bgeneral.ce.bc.clientecobis.common.dto.Status;
import com.bgeneral.ce.bc.clientecobis.common.util.catalogs.CodeCatalog;
import com.bgeneral.ce.bc.clientecobis.controller.dto.CredencialesClienteDTO;
import com.bgeneral.ce.bc.clientecobis.handler.dto.DatosClienteHandlerDTO;
import com.bgeneral.ce.bc.clientecobis.util.MockServer;

@SpringBootTest
@RunWith(MockitoJUnitRunner.class)
public class ConsultarDatosClienteCobisAdapterTest extends MockServer{

	@Mock
	private IAmMapperJsonDTO<CredencialesClienteDTO, DatosClienteHandlerDTO, CredencialesClienteCobisDTO, DatosClienteCobisDTO> mapper;

	@InjectMocks
	private ConsultarDatosClienteCobisAdapter sut;
	
	@Test
	public void shouldConnectToLegacy(){
		String jsonString = "{\"header\":{\"numeroUnico\":1,\"oficina\":1,\"terminal\":\"term_info\",\"usuario\":\"bv_admin\",\"rolUsuario\":1,\"aplicacion\":\"1\",\"canal\":\"1\",\"fecha\":\"2019-01-15T15:30:13.536Z\",\"nodo\":1,\"sesionBg\":\"12x2144dfd4df\",\"direccionIp\":\"123.5.589\",\"origenSolicitud\":\"BEL\",\"numeroReferencia\":\"123465\",\"horaInicio\":\"10:10:10\",\"perfil\":{\"login\":\"1114155\",\"numeroCliente\":1114155,\"numeroBancaVirtual\":123456,\"alias\":\"estrellita123\",\"numeroPerfil\":3,\"numeroClienteUsuario\":1114155,\"vistaAsociada\":0}},\"status\":{\"returnStatus\":{\"returnCode\":\"U0000\",\"returnCodeDesc\":\"Seejecutóelserviciosinerrores\"}},\"body\":{\"numeroCliente\":123,\"apellidoParterno\":\"GUDINO\",\"apellidoMaterno\":\"CARABALLO\",\"apellidoCasada\":\"SERRANO\",\"nombre\":\"INGRID\",\"segundonombre\":\"THAMARA\",\"nombreCompleto\":\"INGRIDGUDINODESERRANO\",\"cedula\":\"08070100841\",\"pasaporte\":\"\",\"segurosocial\":\"0800070100841\",\"fechaNacimiento\":\"2019-01-15T15:30:13.536Z\",\"edad\":42,\"sexo\":{\"codigo\":\"F\",\"descripcion\":\"FEMENINO\"},\"pais\":71,\"nacionalidad\":\"PANAMEÑA\",\"pais_2\":71,\"nacionalidad_2\":\"PANAMEÑA\",\"educacion\":{\"codigo\":\"02\",\"descripcion\":\"UNIVERSITARIA\"},\"actividad\":{\"codigo\":\"00\",\"descripcion\":\"ANALISTA\"},\"estadoCivil\":{\"codigo\":\"C\",\"descripcion\":\"CASADA\"},\"tipoPersona\":{\"codigo\":\"2\",\"descripcion\":\"COLABORADOR\"},\"profesion\":{\"codigo\":\"01\",\"descripcion\":\"LICENCIATURA\"},\"paisRiesgo\":{\"codigo\":71,\"descripcion\":\"PANAMA\"},\"codigoOficial\":10,\"oficial\":\"10\",\"descripcionOficial\":\"OFICIAL\",\"usuario\":{\"codigo\":\"1035\",\"descripcion\":\"CAJERO\"},\"fechaRegistro\":\"2019-01-15T15:30:13.536Z\",\"fechaUltimaModificacion\":\"2019-01-15T15:30:13.536Z\",\"fechaIngreso\":\"2019-01-15T15:30:13.536Z\",\"fechaExp\":\"2019-01-15T15:30:13.536Z\",\"grupo\":{\"codigo\":0,\"descripcion\":\"\"},\"retencion\":\"00\",\"codigoMalaRef\":\"00\",\"codigoCalificacion\":{\"codigo\":\"00\",\"descripcion\":\"0000\"},\"comentario\":\"00\",\"grupoRel\":\"00\",\"estatus\":{\"codigo\":\"00\",\"descripcion\":\"xxxxxxxxxx\"},\"fechaVerificacion\":\"2019-01-15T15:30:13.536Z\",\"difunto\":\"00\",\"versionCsba\":0,\"autorizacionAPC\":true,\"necesitaRevision\":true,\"referenciaInterna\":\"00\",\"clasificacion\":{\"codigo\":\"00\",\"descripcion\":\"sssssxxxxxxxx\"},\"subClasificacion\":{\"codigo\":\"000\",\"descripcion\":\"xxxxxxxssddfdddd\"},\"relacion\":{\"codigo\":\"00000\",\"descripcion\":\"xxxxxxxxxs\"},\"fechaInicioRelacion\":\"2019-01-15T15:30:13.536Z\",\"oficialCodigo\":0,\"oficialSector\":{\"codigo\":\"00\",\"descripcion\":\"000xdfdd\"},\"oficialSubSector\":{\"codigo\":\"000\",\"descripcion\":\"dfdfdfdfdfg\"},\"oficialTipo\":{\"codigo\":\"000\",\"descripcion\":\"string\"},\"usuarioRed\":\"1035\",\"clienteBanca\":{\"codigo\":\"P\",\"descripcion\":\"string\"},\"paisNacimiento\":{\"codigo\":\"71\",\"descripcion\":\"PANAMA\"},\"cantidadCuentasPasivas\":1,\"evaluacionCumplimiento\":{\"codigo\":\"00\",\"descripcion\":\"string\"},\"clienteEsPep\":true,\"clienteTienePantallazo\":true}}"; 
		setUpMockResponse(jsonString);
		modifyPropertyHost();
		
		when(mapper.handlerRequestTOAdapterRequestDTO(Mockito.any())).thenReturn(getAdapterRequest());
when(mapper.adapterResponseTOHandlerResponseDTO(Mockito.any())).thenReturn(getResponseMapperDTO());
		
		BCResponse<DatosClienteHandlerDTO> bcResponse = sut.callRestService(getRequestInServices());

		Assert.assertNotNull(bcResponse);
		Assert.assertThat(bcResponse.getStatus().getReturnStatus().getReturnCode(), instanceOf(String.class));
	}
	
	

	@Test(expected = IllegalArgumentException.class)
	public void validateRequestNull(){
		String jsonString = "{\"header\":{\"numeroUnico\":1,\"oficina\":1,\"terminal\":\"term_info\",\"usuario\":\"bv_admin\",\"rolUsuario\":1,\"aplicacion\":\"1\",\"canal\":\"1\",\"fecha\":\"2019-01-15T15:30:13.536Z\",\"nodo\":1,\"sesionBg\":\"12x2144dfd4df\",\"direccionIp\":\"123.5.589\",\"origenSolicitud\":\"BEL\",\"numeroReferencia\":\"123465\",\"horaInicio\":\"10:10:10\",\"perfil\":{\"login\":\"1114155\",\"numeroCliente\":1114155,\"numeroBancaVirtual\":123456,\"alias\":\"estrellita123\",\"numeroPerfil\":3,\"numeroClienteUsuario\":1114155,\"vistaAsociada\":0}},\"status\":{\"returnStatus\":{\"returnCode\":\"U0000\",\"returnCodeDesc\":\"Seejecutóelserviciosinerrores\"}},\"body\":{\"numeroCliente\":123,\"apellidoParterno\":\"GUDINO\",\"apellidoMaterno\":\"CARABALLO\",\"apellidoCasada\":\"SERRANO\",\"nombre\":\"INGRID\",\"segundonombre\":\"THAMARA\",\"nombreCompleto\":\"INGRIDGUDINODESERRANO\",\"cedula\":\"08070100841\",\"pasaporte\":\"\",\"segurosocial\":\"0800070100841\",\"fechaNacimiento\":\"2019-01-15T15:30:13.536Z\",\"edad\":42,\"sexo\":{\"codigo\":\"F\",\"descripcion\":\"FEMENINO\"},\"pais\":71,\"nacionalidad\":\"PANAMEÑA\",\"pais_2\":71,\"nacionalidad_2\":\"PANAMEÑA\",\"educacion\":{\"codigo\":\"02\",\"descripcion\":\"UNIVERSITARIA\"},\"actividad\":{\"codigo\":\"00\",\"descripcion\":\"ANALISTA\"},\"estadoCivil\":{\"codigo\":\"C\",\"descripcion\":\"CASADA\"},\"tipoPersona\":{\"codigo\":\"2\",\"descripcion\":\"COLABORADOR\"},\"profesion\":{\"codigo\":\"01\",\"descripcion\":\"LICENCIATURA\"},\"paisRiesgo\":{\"codigo\":71,\"descripcion\":\"PANAMA\"},\"codigoOficial\":10,\"oficial\":\"10\",\"descripcionOficial\":\"OFICIAL\",\"usuario\":{\"codigo\":\"1035\",\"descripcion\":\"CAJERO\"},\"fechaRegistro\":\"2019-01-15T15:30:13.536Z\",\"fechaUltimaModificacion\":\"2019-01-15T15:30:13.536Z\",\"fechaIngreso\":\"2019-01-15T15:30:13.536Z\",\"fechaExp\":\"2019-01-15T15:30:13.536Z\",\"grupo\":{\"codigo\":0,\"descripcion\":\"\"},\"retencion\":\"00\",\"codigoMalaRef\":\"00\",\"codigoCalificacion\":{\"codigo\":\"00\",\"descripcion\":\"0000\"},\"comentario\":\"00\",\"grupoRel\":\"00\",\"estatus\":{\"codigo\":\"00\",\"descripcion\":\"xxxxxxxxxx\"},\"fechaVerificacion\":\"2019-01-15T15:30:13.536Z\",\"difunto\":\"00\",\"versionCsba\":0,\"autorizacionAPC\":true,\"necesitaRevision\":true,\"referenciaInterna\":\"00\",\"clasificacion\":{\"codigo\":\"00\",\"descripcion\":\"sssssxxxxxxxx\"},\"subClasificacion\":{\"codigo\":\"000\",\"descripcion\":\"xxxxxxxssddfdddd\"},\"relacion\":{\"codigo\":\"00000\",\"descripcion\":\"xxxxxxxxxs\"},\"fechaInicioRelacion\":\"2019-01-15T15:30:13.536Z\",\"oficialCodigo\":0,\"oficialSector\":{\"codigo\":\"00\",\"descripcion\":\"000xdfdd\"},\"oficialSubSector\":{\"codigo\":\"000\",\"descripcion\":\"dfdfdfdfdfg\"},\"oficialTipo\":{\"codigo\":\"000\",\"descripcion\":\"string\"},\"usuarioRed\":\"1035\",\"clienteBanca\":{\"codigo\":\"P\",\"descripcion\":\"string\"},\"paisNacimiento\":{\"codigo\":\"71\",\"descripcion\":\"PANAMA\"},\"cantidadCuentasPasivas\":1,\"evaluacionCumplimiento\":{\"codigo\":\"00\",\"descripcion\":\"string\"},\"clienteEsPep\":true,\"clienteTienePantallazo\":true}}"; 
		setUpMockResponse(jsonString);
		modifyPropertyHost();
		
		sut.callRestService(null);
	}
	
	@Test(expected = IllegalArgumentException.class)
	public void validateStatusReturnNull(){
		String jsonString = "{\"header\":{\"numeroUnico\":1,\"oficina\":1,\"terminal\":\"term_info\",\"usuario\":\"bv_admin\",\"rolUsuario\":1,\"aplicacion\":\"1\",\"canal\":\"1\",\"fecha\":\"2019-01-15T15:30:13.536Z\",\"nodo\":1,\"sesionBg\":\"12x2144dfd4df\",\"direccionIp\":\"123.5.589\",\"origenSolicitud\":\"BEL\",\"numeroReferencia\":\"123465\",\"horaInicio\":\"10:10:10\",\"perfil\":{\"login\":\"1114155\",\"numeroCliente\":1114155,\"numeroBancaVirtual\":123456,\"alias\":\"estrellita123\",\"numeroPerfil\":3,\"numeroClienteUsuario\":1114155,\"vistaAsociada\":0}},\"status\":{\"returnStatus\":{\"returnCode\":\"U0000\",\"returnCodeDesc\":\"Seejecutóelserviciosinerrores\"}},\"body\":{\"numeroCliente\":123,\"apellidoParterno\":\"GUDINO\",\"apellidoMaterno\":\"CARABALLO\",\"apellidoCasada\":\"SERRANO\",\"nombre\":\"INGRID\",\"segundonombre\":\"THAMARA\",\"nombreCompleto\":\"INGRIDGUDINODESERRANO\",\"cedula\":\"08070100841\",\"pasaporte\":\"\",\"segurosocial\":\"0800070100841\",\"fechaNacimiento\":\"2019-01-15T15:30:13.536Z\",\"edad\":42,\"sexo\":{\"codigo\":\"F\",\"descripcion\":\"FEMENINO\"},\"pais\":71,\"nacionalidad\":\"PANAMEÑA\",\"pais_2\":71,\"nacionalidad_2\":\"PANAMEÑA\",\"educacion\":{\"codigo\":\"02\",\"descripcion\":\"UNIVERSITARIA\"},\"actividad\":{\"codigo\":\"00\",\"descripcion\":\"ANALISTA\"},\"estadoCivil\":{\"codigo\":\"C\",\"descripcion\":\"CASADA\"},\"tipoPersona\":{\"codigo\":\"2\",\"descripcion\":\"COLABORADOR\"},\"profesion\":{\"codigo\":\"01\",\"descripcion\":\"LICENCIATURA\"},\"paisRiesgo\":{\"codigo\":71,\"descripcion\":\"PANAMA\"},\"codigoOficial\":10,\"oficial\":\"10\",\"descripcionOficial\":\"OFICIAL\",\"usuario\":{\"codigo\":\"1035\",\"descripcion\":\"CAJERO\"},\"fechaRegistro\":\"2019-01-15T15:30:13.536Z\",\"fechaUltimaModificacion\":\"2019-01-15T15:30:13.536Z\",\"fechaIngreso\":\"2019-01-15T15:30:13.536Z\",\"fechaExp\":\"2019-01-15T15:30:13.536Z\",\"grupo\":{\"codigo\":0,\"descripcion\":\"\"},\"retencion\":\"00\",\"codigoMalaRef\":\"00\",\"codigoCalificacion\":{\"codigo\":\"00\",\"descripcion\":\"0000\"},\"comentario\":\"00\",\"grupoRel\":\"00\",\"estatus\":{\"codigo\":\"00\",\"descripcion\":\"xxxxxxxxxx\"},\"fechaVerificacion\":\"2019-01-15T15:30:13.536Z\",\"difunto\":\"00\",\"versionCsba\":0,\"autorizacionAPC\":true,\"necesitaRevision\":true,\"referenciaInterna\":\"00\",\"clasificacion\":{\"codigo\":\"00\",\"descripcion\":\"sssssxxxxxxxx\"},\"subClasificacion\":{\"codigo\":\"000\",\"descripcion\":\"xxxxxxxssddfdddd\"},\"relacion\":{\"codigo\":\"00000\",\"descripcion\":\"xxxxxxxxxs\"},\"fechaInicioRelacion\":\"2019-01-15T15:30:13.536Z\",\"oficialCodigo\":0,\"oficialSector\":{\"codigo\":\"00\",\"descripcion\":\"000xdfdd\"},\"oficialSubSector\":{\"codigo\":\"000\",\"descripcion\":\"dfdfdfdfdfg\"},\"oficialTipo\":{\"codigo\":\"000\",\"descripcion\":\"string\"},\"usuarioRed\":\"1035\",\"clienteBanca\":{\"codigo\":\"P\",\"descripcion\":\"string\"},\"paisNacimiento\":{\"codigo\":\"71\",\"descripcion\":\"PANAMA\"},\"cantidadCuentasPasivas\":1,\"evaluacionCumplimiento\":{\"codigo\":\"00\",\"descripcion\":\"string\"},\"clienteEsPep\":true,\"clienteTienePantallazo\":true}}";
		setUpMockResponse(jsonString);
		modifyPropertyHost();
		
		BCResponse<DatosClienteHandlerDTO> mapperResponse = getResponseMapperDTO();
		mapperResponse.setStatus(null);
		
		sut.callRestService(getRequestInServices());
	}
		
	private BCRequest<CredencialesClienteDTO> getRequestInServices() {
		BCRequest<CredencialesClienteDTO> request = new BCRequest<>();
		request.setHeader(this.getHeader());
		CredencialesClienteDTO credencialesClienteDTO = new CredencialesClienteDTO();
		credencialesClienteDTO.setCedula("123-123-123-123");
		credencialesClienteDTO.setFormato(1);
		credencialesClienteDTO.setNumeroRegistros(1);
		credencialesClienteDTO.setOnboarding(true);
		request.setBody(credencialesClienteDTO);
		return request;
	}

	private BCResponse<DatosClienteHandlerDTO> getResponseMapperDTO() {
		BCResponse<DatosClienteHandlerDTO> bcResponse = new BCResponse<>();
		bcResponse.setBody(loadBody());
		bcResponse.setHeader(this.getHeader());
		bcResponse.setStatus(this.getStatus());
		return bcResponse;		
	}
	
	private BCRequest<CredencialesClienteCobisDTO> getAdapterRequest() {
		BCRequest<CredencialesClienteCobisDTO> request = new BCRequest<>();
		request.setHeader(this.getHeader());
		CredencialesClienteCobisDTO credencialesClienteCobisDTO = new CredencialesClienteCobisDTO();
		credencialesClienteCobisDTO.setCedula("123-123-123-123");
		credencialesClienteCobisDTO.setFormato(1);
		credencialesClienteCobisDTO.setNumeroRegistros(1);
		credencialesClienteCobisDTO.setOnboarding(true);
		request.setBody(credencialesClienteCobisDTO);
		return request;
	}
	
	private DatosClienteHandlerDTO loadBody() {
		DatosClienteHandlerDTO body = new DatosClienteHandlerDTO();

		body.setNombre("MARIA");
		body.setSegundonombre("EVA");
		body.setApellidoPaterno("CARABALLO");
		body.setApellidoMaterno("GONZALEZ");
		body.setApellidoCasada("GUDINO");
		body.setCedula("09  009600475");
		body.setEdad(62);
		body.setAutorizacionAPC(true);
		body.setCantidadCuentasPasivas(2);
		body.setClienteEsPep(false);
		body.setClienteTienePantallazo(false);
		body.setCodigoMalaRef("00");
		body.setCodigoOficial(1035);
		body.setComentario("PRESTAMO Personal");
		body.setDescripcionOficial("Oficial de sucursal");
		body.setDifunto("S");
		body.setFechaExp(new Date());
		body.setFechaIngreso(new Date());
		body.setFechaInicioRelacion("01/01/2000");
		body.setFechaNacimiento(new Date());
		body.setFechaRegistro(new Date());
		body.setFechaUltimaModificacion(new Date());
		body.setFechaVerificacion(new Date());
		body.setGrupoRel("S");
		
		Catalogo catalogo = new Catalogo();

		catalogo.setCodigo("00");
		catalogo.setDescripcion("COSTURERA");
		body.setActividad(catalogo);		

		catalogo.setCodigo("00");
		catalogo.setDescripcion("COSTURERA");
		body.setClasificacion(catalogo);
		
		catalogo.setCodigo("00");
		catalogo.setDescripcion("COSTURERA");
		body.setClienteBanca(catalogo);
		
		catalogo.setCodigo("00");
		catalogo.setDescripcion("COSTURERA");
		body.setCodigoCalificacion(catalogo);
		
		catalogo.setCodigo("00");
		catalogo.setDescripcion("COSTURERA");
		body.setEducacion(catalogo);
		
		catalogo.setCodigo("00");
		catalogo.setDescripcion("COSTURERA");		
		body.setEstadoCivil(catalogo);

		catalogo.setCodigo("00");
		catalogo.setDescripcion("COSTURERA");		
		body.setEstatus(catalogo);

		catalogo.setCodigo("00");
		catalogo.setDescripcion("COSTURERA");		
		body.setEvaluacionCumplimiento(catalogo);
		
		catalogo.setCodigo("00");
		catalogo.setDescripcion("COSTURERA");		
		body.setGrupo(catalogo);

		catalogo.setCodigo("00");
		catalogo.setDescripcion("COSTURERA");		
		body.setOficialSector(catalogo);

		catalogo.setCodigo("00");
		catalogo.setDescripcion("COSTURERA");		
		body.setOficialSubSector(catalogo);

		catalogo.setCodigo("00");
		catalogo.setDescripcion("COSTURERA");		
		body.setOficialTipo(catalogo);

		catalogo.setCodigo("00");
		catalogo.setDescripcion("COSTURERA");		
		body.setPaisNacimiento(catalogo);

		catalogo.setCodigo("00");
		catalogo.setDescripcion("COSTURERA");		
		body.setPaisRiesgo(catalogo);

		catalogo.setCodigo("00");
		catalogo.setDescripcion("COSTURERA");
		body.setProfesion(catalogo);
		
		catalogo.setCodigo("00");
		catalogo.setDescripcion("COSTURERA");		
		body.setRelacion(catalogo);
		
		catalogo.setCodigo("00");
		catalogo.setDescripcion("COSTURERA");		
		body.setSexo(catalogo);
		
		catalogo.setCodigo("00");
		catalogo.setDescripcion("COSTURERA");		
		body.setSubClasificacion(catalogo);

		catalogo.setCodigo("00");
		catalogo.setDescripcion("COSTURERA");		
		body.setTipoPersona(catalogo);
		
		catalogo.setCodigo("00");
		catalogo.setDescripcion("COSTURERA");		
		body.setUsuario(catalogo);
	
		
		return body;
	}		

	private Header getHeader() {
		Header headerRest = new Header();
		// Cabecera
		headerRest.setNumeroUnico(1);
		headerRest.setOficina(1);
		headerRest.setTerminal("1");
		headerRest.setUsuario("bccanales");
		headerRest.setRolUsuario(3);
		// CabeceraCanales
		headerRest.setAplicacion("1");
		headerRest.setCanal("1");
		headerRest.setFecha(new Date());
		headerRest.setNodo(1);
		headerRest.setSesionBg("1234");
		headerRest.setDireccionIp("");
		headerRest.setOrigenSolicitud("bel");
		headerRest.setNumeroReferencia("1234");
		headerRest.setHoraInicio("12:00:00");

		// Perfil
		Perfil perfil = new Perfil();
		perfil.setLogin("");
		perfil.setNumeroCliente(12);
		perfil.setNumeroBancaVirtual(0);
		perfil.setAlias("prueba");
		perfil.setNumeroPerfil(3);

		headerRest.setPerfil(perfil);
		return headerRest;
	}

	private Status getStatus() {
		Status status = new Status();
		ReturnStatus returnStatus = new ReturnStatus();
		returnStatus.setReturnCode(CodeCatalog.CODE_SUCCESS.codeS());
		returnStatus.setReturnCodeDesc(CodeCatalog.S_MSG_CODE_SUCCESS.codeS());
		status.setReturnStatus(returnStatus);
		return status;
	}	
}