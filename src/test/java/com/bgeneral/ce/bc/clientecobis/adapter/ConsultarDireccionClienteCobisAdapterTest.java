package com.bgeneral.ce.bc.clientecobis.adapter;

import static org.hamcrest.CoreMatchers.instanceOf;
import static org.mockito.Mockito.when;

import java.util.Date;

import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.MockitoJUnitRunner;
import org.springframework.boot.test.context.SpringBootTest;

import com.bgeneral.ce.bc.clientecobis.adapter.dto.DatosDireccionClienteCobisDTO;
import com.bgeneral.ce.bc.clientecobis.adapter.mapper.IAmMapperJsonDTO;
import com.bgeneral.ce.bc.clientecobis.common.dto.BCRequest;
import com.bgeneral.ce.bc.clientecobis.common.dto.BCResponse;
import com.bgeneral.ce.bc.clientecobis.common.dto.Catalogo;
import com.bgeneral.ce.bc.clientecobis.common.dto.EmptyBodyT;
import com.bgeneral.ce.bc.clientecobis.common.dto.Header;
import com.bgeneral.ce.bc.clientecobis.common.dto.Perfil;
import com.bgeneral.ce.bc.clientecobis.common.dto.ReturnStatus;
import com.bgeneral.ce.bc.clientecobis.common.dto.Status;
import com.bgeneral.ce.bc.clientecobis.common.util.catalogs.CodeCatalog;
import com.bgeneral.ce.bc.clientecobis.handler.dto.DatosDireccionClienteCobisHandlerDTO;
import com.bgeneral.ce.bc.clientecobis.util.MockServer;

@SpringBootTest
@RunWith(MockitoJUnitRunner.class)
public class ConsultarDireccionClienteCobisAdapterTest extends MockServer{

	@Mock
	private IAmMapperJsonDTO<EmptyBodyT, DatosDireccionClienteCobisHandlerDTO, EmptyBodyT, DatosDireccionClienteCobisDTO> mapper;

	@InjectMocks
	private ConsultarDireccionClienteCobisAdapter sut;
	
	@Test
	public void shouldConnectToLegacy(){
		String jsonString = "{\"header\":{\"numeroUnico\":1,\"oficina\":1,\"terminal\":\"LaBandera\",\"usuario\":\"Chepa\",\"rolUsuario\":1,\"aplicacion\":\"tintenegro\",\"canal\":\"ESPN\",\"fecha\":\"2019-01-10T21:21:06.967Z\",\"nodo\":1,\"sesionBg\":\"SDFsdfgsdfGSERgsdfgSDfgsdfgSDF\",\"direccionIp\":\"10.2.10.2\",\"origenSolicitud\":\"INICIO\",\"numeroReferencia\":\"5555\",\"horaInicio\":\"10:10:10\",\"perfil\":{\"login\":\"1114155\",\"numeroCliente\":1114155,\"numeroBancaVirtual\":123456,\"alias\":\"ERCARTELUO\",\"numeroPerfil\":2,\"numeroClienteUsuario\":3,\"vistaAsociada\":1}},\"body\":{\"direccion\":{\"codigo\":\"DIRECCION_COD\",\"descripcion\":\"direcciondescripcion\"},\"tipoDireccion\":\"tipounico\",\"corregimiento\":{\"codigo\":\"CORREGIMIENTO_COD\",\"descripcion\":\"corregimientodescripcion\"},\"ciudad\":{\"codigo\":\"CIUDAD_COD\",\"descripcion\":\"ciudaddescripcion\"},\"oficina\":{\"codigo\":\"OFICINA_COD\",\"descripcion\":\"oficinadescripcion\"},\"pais\":{\"codigo\":\"PAIS_COD\",\"descripcion\":\"paisdescripcion\"},\"barrio\":{\"codigo\":\"BARRIO_COD\",\"descripcion\":\"barriodescripcion\"},\"sector\":{\"codigo\":\"SECTOR_COD\",\"descripcion\":\"sectordescripcion\"},\"zona\":{\"codigo\":\"ZONA_COD\",\"descripcion\":\"zonadescripcion\"},\"calle\":\"NOTECALLE\",\"edificioCasa\":\"CASAEDIFICIO\",\"fechaRegistro\":\"2019-01-10T21:21:06.967Z\",\"fechaModificacion\":\"2019-01-10T21:21:06.967Z\",\"fechaVerificacion\":\"2019-01-10T21:21:06.967Z\",\"esVigente\":true,\"esVerificado\":false,\"esPrincipal\":false,\"funcionario\":\"LAPOLI\"},\"status\":{\"returnStatus\":{\"returnCode\":\"U0000\",\"returnCodeDesc\":\"TODOBIENAMIGO\"}}}"; 
		setUpMockResponse(jsonString);
		modifyPropertyHost();

		when(mapper.adapterResponseTOHandlerResponseDTO(Mockito.any())).thenReturn(getResponseMapperDTO());
		
		BCResponse<DatosDireccionClienteCobisHandlerDTO> bcResponse = sut.callRestService(getRequestInServices());

		Assert.assertNotNull(bcResponse);
		Assert.assertThat(bcResponse.getStatus().getReturnStatus().getReturnCode(), instanceOf(String.class));
	}
	
	@Test(expected = IllegalArgumentException.class)
	public void validateStatusReturnStatusNull(){
		String jsonString = "{\"header\":{\"numeroUnico\":1,\"oficina\":1,\"terminal\":\"LaBandera\",\"usuario\":\"Chepa\",\"rolUsuario\":1,\"aplicacion\":\"tintenegro\",\"canal\":\"ESPN\",\"fecha\":\"2019-01-10T21:21:06.967Z\",\"nodo\":1,\"sesionBg\":\"SDFsdfgsdfGSERgsdfgSDfgsdfgSDF\",\"direccionIp\":\"10.2.10.2\",\"origenSolicitud\":\"INICIO\",\"numeroReferencia\":\"5555\",\"horaInicio\":\"10:10:10\",\"perfil\":{\"login\":\"1114155\",\"numeroCliente\":1114155,\"numeroBancaVirtual\":123456,\"alias\":\"ERCARTELUO\",\"numeroPerfil\":2,\"numeroClienteUsuario\":3,\"vistaAsociada\":1}},\"body\":{\"direccion\":{\"codigo\":\"DIRECCION_COD\",\"descripcion\":\"direcciondescripcion\"},\"tipoDireccion\":\"tipounico\",\"corregimiento\":{\"codigo\":\"CORREGIMIENTO_COD\",\"descripcion\":\"corregimientodescripcion\"},\"ciudad\":{\"codigo\":\"CIUDAD_COD\",\"descripcion\":\"ciudaddescripcion\"},\"oficina\":{\"codigo\":\"OFICINA_COD\",\"descripcion\":\"oficinadescripcion\"},\"pais\":{\"codigo\":\"PAIS_COD\",\"descripcion\":\"paisdescripcion\"},\"barrio\":{\"codigo\":\"BARRIO_COD\",\"descripcion\":\"barriodescripcion\"},\"sector\":{\"codigo\":\"SECTOR_COD\",\"descripcion\":\"sectordescripcion\"},\"zona\":{\"codigo\":\"ZONA_COD\",\"descripcion\":\"zonadescripcion\"},\"calle\":\"NOTECALLE\",\"edificioCasa\":\"CASAEDIFICIO\",\"fechaRegistro\":\"2019-01-10T21:21:06.967Z\",\"fechaModificacion\":\"2019-01-10T21:21:06.967Z\",\"fechaVerificacion\":\"2019-01-10T21:21:06.967Z\",\"esVigente\":true,\"esVerificado\":false,\"esPrincipal\":false,\"funcionario\":\"LAPOLI\"},\"status\":{\"returnStatus\":{\"returnCode\":\"U0000\",\"returnCodeDesc\":\"TODOBIENAMIGO\"}}}"; 
		setUpMockResponse(jsonString);
		modifyPropertyHost();
		BCResponse<DatosDireccionClienteCobisHandlerDTO> mapperResponse = getResponseMapperDTO();
		mapperResponse.getStatus().setReturnStatus(null);
		when(mapper.adapterResponseTOHandlerResponseDTO(Mockito.any())).thenReturn(mapperResponse);
		sut.callRestService(getRequestInServices());
	}
	
	@Test(expected = NullPointerException.class)
	public void validateStatusReturnNull(){
		String jsonString = "{\"header\":{\"numeroUnico\":1,\"oficina\":1,\"terminal\":\"LaBandera\",\"usuario\":\"Chepa\",\"rolUsuario\":1,\"aplicacion\":\"tintenegro\",\"canal\":\"ESPN\",\"fecha\":\"2019-01-10T21:21:06.967Z\",\"nodo\":1,\"sesionBg\":\"SDFsdfgsdfGSERgsdfgSDfgsdfgSDF\",\"direccionIp\":\"10.2.10.2\",\"origenSolicitud\":\"INICIO\",\"numeroReferencia\":\"5555\",\"horaInicio\":\"10:10:10\",\"perfil\":{\"login\":\"1114155\",\"numeroCliente\":1114155,\"numeroBancaVirtual\":123456,\"alias\":\"ERCARTELUO\",\"numeroPerfil\":2,\"numeroClienteUsuario\":3,\"vistaAsociada\":1}},\"body\":{\"direccion\":{\"codigo\":\"DIRECCION_COD\",\"descripcion\":\"direcciondescripcion\"},\"tipoDireccion\":\"tipounico\",\"corregimiento\":{\"codigo\":\"CORREGIMIENTO_COD\",\"descripcion\":\"corregimientodescripcion\"},\"ciudad\":{\"codigo\":\"CIUDAD_COD\",\"descripcion\":\"ciudaddescripcion\"},\"oficina\":{\"codigo\":\"OFICINA_COD\",\"descripcion\":\"oficinadescripcion\"},\"pais\":{\"codigo\":\"PAIS_COD\",\"descripcion\":\"paisdescripcion\"},\"barrio\":{\"codigo\":\"BARRIO_COD\",\"descripcion\":\"barriodescripcion\"},\"sector\":{\"codigo\":\"SECTOR_COD\",\"descripcion\":\"sectordescripcion\"},\"zona\":{\"codigo\":\"ZONA_COD\",\"descripcion\":\"zonadescripcion\"},\"calle\":\"NOTECALLE\",\"edificioCasa\":\"CASAEDIFICIO\",\"fechaRegistro\":\"2019-01-10T21:21:06.967Z\",\"fechaModificacion\":\"2019-01-10T21:21:06.967Z\",\"fechaVerificacion\":\"2019-01-10T21:21:06.967Z\",\"esVigente\":true,\"esVerificado\":false,\"esPrincipal\":false,\"funcionario\":\"LAPOLI\"},\"status\":{\"returnStatus\":{\"returnCode\":\"U0000\",\"returnCodeDesc\":\"TODOBIENAMIGO\"}}}"; 
		setUpMockResponse(jsonString);
		modifyPropertyHost();
		BCResponse<DatosDireccionClienteCobisHandlerDTO> mapperResponse = getResponseMapperDTO();
		mapperResponse.setStatus(null);
		when(mapper.adapterResponseTOHandlerResponseDTO(Mockito.any())).thenReturn(mapperResponse);
		sut.callRestService(getRequestInServices());
	}
	
	private BCRequest<EmptyBodyT> getRequestInServices() {
		BCRequest<EmptyBodyT> request = new BCRequest<>();
		request.setHeader(this.getHeader());
		return request;
	}

	private BCResponse<DatosDireccionClienteCobisHandlerDTO> getResponseMapperDTO() {
		BCResponse<DatosDireccionClienteCobisHandlerDTO> bcResponse = new BCResponse<>();
		bcResponse.setBody(loadBody());
		bcResponse.setHeader(this.getHeader());
		bcResponse.setStatus(this.getStatus());
		return bcResponse;
	}

	private DatosDireccionClienteCobisHandlerDTO loadBody() {
		DatosDireccionClienteCobisHandlerDTO body = new DatosDireccionClienteCobisHandlerDTO();
		
		body.setCalle("CALLE");
		body.setEdificioCasa("EDIFICIO CASA");
		body.setFuncionario("FUNCIONARIO");
		body.setTipoDireccion("TIPO DIRECCION");

		body.setEsPrincipal(true);
		body.setEsVerificado(false);
		body.setEsVigente(false);

		body.setFechaModificacion(new Date());
		body.setFechaRegistro(new Date());
		body.setFechaVerificacion(new Date());

		Catalogo catalogo = new Catalogo();

		catalogo.setCodigo("BARRIO_COD");
		catalogo.setDescripcion("Barrio Descripcion");
		body.setBarrio(catalogo);

		catalogo.setCodigo("CIUDAD_COD");
		catalogo.setDescripcion("Ciudad Descripcion");
		body.setCiudad(catalogo);

		catalogo.setCodigo("CORREGIMIENTO_COD");
		catalogo.setDescripcion("Corregimiento Descripcion");
		body.setCorregimiento(catalogo);

		catalogo.setCodigo("DIRECCION_COD");
		catalogo.setDescripcion("Direccion Descripcion");
		body.setDireccion(catalogo);

		catalogo.setCodigo("OFICINA_COD");
		catalogo.setDescripcion("Oficina Descripcion");
		body.setOficina(catalogo);

		catalogo.setCodigo("PAIS_COD");
		catalogo.setDescripcion("Pais Descripcion");
		body.setPais(catalogo);

		catalogo.setCodigo("SECTOR_COD");
		catalogo.setDescripcion("Sector Descripcion");
		body.setSector(catalogo);

		catalogo.setCodigo("ZONA_COD");
		catalogo.setDescripcion("Zona Descripcion");
		body.setZona(catalogo);

		return body;
	}

	private Header getHeader() {
		Header headerRest = new Header();
		// Cabecera
		headerRest.setNumeroUnico(1);
		headerRest.setOficina(1);
		headerRest.setTerminal("1");
		headerRest.setUsuario("bccanales");
		headerRest.setRolUsuario(3);
		// CabeceraCanales
		headerRest.setAplicacion("1");
		headerRest.setCanal("1");
		headerRest.setFecha(new Date());
		headerRest.setNodo(1);
		headerRest.setSesionBg("1234");
		headerRest.setDireccionIp("");
		headerRest.setOrigenSolicitud("bel");
		headerRest.setNumeroReferencia("1234");
		headerRest.setHoraInicio("12:00:00");

		// Perfil
		Perfil perfil = new Perfil();
		perfil.setLogin("");
		perfil.setNumeroCliente(12);
		perfil.setNumeroBancaVirtual(0);
		perfil.setAlias("prueba");
		perfil.setNumeroPerfil(3);

		headerRest.setPerfil(perfil);
		return headerRest;
	}

	private Status getStatus() {
		Status status = new Status();
		ReturnStatus returnStatus = new ReturnStatus();
		returnStatus.setReturnCode(CodeCatalog.CODE_SUCCESS.codeS());
		returnStatus.setReturnCodeDesc(CodeCatalog.S_MSG_CODE_SUCCESS.codeS());
		status.setReturnStatus(returnStatus);
		return status;
	}

}
