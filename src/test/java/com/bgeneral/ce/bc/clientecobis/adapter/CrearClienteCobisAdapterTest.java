package com.bgeneral.ce.bc.clientecobis.adapter;

import static com.bgeneral.ce.bc.clientecobis.common.util.UtilBG.FormatDocumentIDLegacy;
import static org.hamcrest.CoreMatchers.instanceOf;
import static org.mockito.Mockito.when;

import java.util.Date;

import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.MockitoJUnitRunner;
import org.springframework.boot.test.context.SpringBootTest;

import com.bgeneral.ce.bc.clientecobis.adapter.dto.ClienteCobisDTO;
import com.bgeneral.ce.bc.clientecobis.adapter.dto.ValorClienteCobisDTO;
import com.bgeneral.ce.bc.clientecobis.adapter.mapper.IAmMapperJsonDTO;
import com.bgeneral.ce.bc.clientecobis.common.dto.BCRequest;
import com.bgeneral.ce.bc.clientecobis.common.dto.BCResponse;
import com.bgeneral.ce.bc.clientecobis.common.dto.Catalogo;
import com.bgeneral.ce.bc.clientecobis.common.dto.ReturnStatus;
import com.bgeneral.ce.bc.clientecobis.handler.dto.EnteClienteDTO;
import com.bgeneral.ce.bc.clientecobis.handler.dto.InformacionClienteDTO;
import com.bgeneral.ce.bc.clientecobis.util.MockDummyRequest;
import com.bgeneral.ce.bc.clientecobis.util.MockDummyResponse;
import com.bgeneral.ce.bc.clientecobis.util.MockServer;

/*
 * @author: F. Castillo
 * 
 */
@SpringBootTest
@RunWith(MockitoJUnitRunner.class)
public class CrearClienteCobisAdapterTest extends MockServer{
	
	@Mock
	private	IAmMapperJsonDTO<InformacionClienteDTO, EnteClienteDTO, ClienteCobisDTO, ValorClienteCobisDTO> mapper;

	@InjectMocks
	private CrearClienteCobisAdapter sut;
	
	@Test
	public void shouldConnectToLegacy(){
		String jsonString = "{\"header\":{\"numeroUnico\":123456,\"oficina\":62,\"terminal\":\"twen\",\"usuario\":\"twen\",\"rolUsuario\":1,\"aplicacion\":\"1\",\"canal\":\"1\",\"fecha\":\"2018-10-22T21:12:52.655Z\",\"nodo\":1,\"sesionBg\":\"\",\"direccionIp\":\"1.9.7.6\",\"origenSolicitud\":\"BEL\",\"numeroReferencia\":\"\",\"horaInicio\":\"\",\"perfil\":{\"login\":\"\",\"numeroCliente\":104953,\"numeroBancaVirtual\":0,\"alias\":\"yonofui\",\"numeroPerfil\":3,\"numeroClienteUsuario\":1,\"vistaAsociada\":1}},\"body\":{\"enteMis\":\"6999\"},\"status\":{\"returnStatus\":{\"returnCode\":\"U0000\",\"returnCodeDesc\":\"SeharealizadolaejecuciÃ³ndelserviciocorrectamente.\"}}}"; 
		setUpMockResponse(jsonString);
		modifyPropertyHost();

		when(mapper.handlerRequestTOAdapterRequestDTO(Mockito.any())).thenReturn(getRequestMapperDTO());
		when(mapper.adapterResponseTOHandlerResponseDTO(Mockito.any())).thenReturn(getResponseMapperDTO());
		
		BCResponse<EnteClienteDTO> bcResponse = sut.callRestService(getRequestInServices());

		Assert.assertNotNull(bcResponse);
		Assert.assertThat(bcResponse.getStatus().getReturnStatus().getReturnCode(), instanceOf(String.class));
	}
	
	@Test(expected = IllegalArgumentException.class)
	public void validateServicesWithRequestBodyNull(){		
		BCRequest<InformacionClienteDTO> bcRequest = getRequestInServices();
		bcRequest.setBody(null);
		sut.callRestService(bcRequest);
	}
	
	@Test
	public void validateResponseMapperWithNumberRegistryZero(){
		String jsonString = "{\"header\":{\"numeroUnico\":123456,\"oficina\":62,\"terminal\":\"twen\",\"usuario\":\"twen\",\"rolUsuario\":1,\"aplicacion\":\"1\",\"canal\":\"1\",\"fecha\":\"2018-10-22T21:12:52.655Z\",\"nodo\":1,\"sesionBg\":\"\",\"direccionIp\":\"1.9.7.6\",\"origenSolicitud\":\"BEL\",\"numeroReferencia\":\"\",\"horaInicio\":\"\",\"perfil\":{\"login\":\"\",\"numeroCliente\":104953,\"numeroBancaVirtual\":0,\"alias\":\"yonofui\",\"numeroPerfil\":3,\"numeroClienteUsuario\":1,\"vistaAsociada\":1}},\"body\":{\"enteMis\":\"6999\"},\"status\":{\"returnStatus\":{\"returnCode\":\"U0000\",\"returnCodeDesc\":\"SeharealizadolaejecuciÃ³ndelserviciocorrectamente.\"}}}"; 
		setUpMockResponse(jsonString);
		modifyPropertyHost();

		when(mapper.handlerRequestTOAdapterRequestDTO(Mockito.any())).thenReturn(getRequestMapperDTO());
		when(mapper.adapterResponseTOHandlerResponseDTO(Mockito.any())).thenReturn(getResponseMapperDTOWithNumberRegistryZero());
		BCResponse<EnteClienteDTO> bcResponse = sut.callRestService(getRequestInServices());

		Assert.assertNotNull(bcResponse);
		Assert.assertNull(bcResponse.getBody());
	}
	
	
	/* Methods to load data in the DTOs */
	private BCRequest<ClienteCobisDTO> getRequestMapperDTO() {
		ClienteCobisDTO body = new ClienteCobisDTO();

		body.setPrimerNombre("LUCAS");
		body.setPrimerApellido("TROTACIELOS");
		body.setCedula(FormatDocumentIDLegacy("8-906-2217"));
		body.setFechaNacimiento(new Date());
		
		Catalogo catalogoSexo = new Catalogo();
		catalogoSexo.setCodigo("M");
		body.setSexo(catalogoSexo);
		
		Catalogo catalogoEstado = new Catalogo();
		catalogoEstado.setCodigo("S");
		body.setEstadoCivil(catalogoEstado);
		
		Catalogo catalogoPaisNacimiento = new Catalogo();
		catalogoPaisNacimiento.setCodigo("61");
		body.setPaisNacimiento(catalogoPaisNacimiento);
		
		Catalogo catalogoPaisNacionalidad = new Catalogo();
		catalogoPaisNacionalidad.setCodigo("61");
		body.setPaisNacionalidad(catalogoPaisNacionalidad);
		
		
		Catalogo catalogoPaisRiesgo = new Catalogo();
		catalogoPaisRiesgo.setCodigo("61");
		body.setPaisRiesgo(catalogoPaisRiesgo);
			
		BCRequest<ClienteCobisDTO> bcRequest = new MockDummyRequest<ClienteCobisDTO>().setBody(body).build();
		return bcRequest;
	}

	
	private BCResponse<EnteClienteDTO> getResponseMapperDTO() {
		EnteClienteDTO body = new EnteClienteDTO();
		body.setEnteMis("6999");
		
		ReturnStatus returnStatus = new ReturnStatus();
		returnStatus.setReturnCode("U0000");
		returnStatus.setReturnCodeDesc("Se ha realizado al ejecucion con exito");
		
		return new MockDummyResponse<EnteClienteDTO>().setBody(body).setReturnStatus(returnStatus).build();
	}
	
	private BCRequest<InformacionClienteDTO> getRequestInServices() {
		return new MockDummyRequest<InformacionClienteDTO>().setBody(new InformacionClienteDTO()).build();
	}
	
	private BCResponse<EnteClienteDTO> getResponseMapperDTOWithNumberRegistryZero(){
		return new MockDummyResponse<EnteClienteDTO>().setBody(null).build();
	}

	
	private BCResponse<EnteClienteDTO> getResponseMapperDTOWithReturnStatusNull(){
		return new MockDummyResponse<EnteClienteDTO>().build();
	}
}
