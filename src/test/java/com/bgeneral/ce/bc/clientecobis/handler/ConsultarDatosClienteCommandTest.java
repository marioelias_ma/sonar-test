package com.bgeneral.ce.bc.clientecobis.handler;

import static org.hamcrest.CoreMatchers.equalTo;
import static org.hamcrest.CoreMatchers.instanceOf;
import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.CoreMatchers.notNullValue;
import static org.junit.Assert.assertThat;
import static org.mockito.Mockito.when;
import static org.mockito.Mockito.eq;

import java.util.Date;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.MockitoJUnitRunner;
import org.modelmapper.ModelMapper;

import com.bgeneral.ce.bc.clientecobis.common.dto.BCRequest;
import com.bgeneral.ce.bc.clientecobis.common.dto.BCResponse;
import com.bgeneral.ce.bc.clientecobis.common.dto.Catalogo;
import com.bgeneral.ce.bc.clientecobis.common.dto.EmptyBodyT;
import com.bgeneral.ce.bc.clientecobis.common.dto.Header;
import com.bgeneral.ce.bc.clientecobis.common.dto.Perfil;
import com.bgeneral.ce.bc.clientecobis.common.dto.ReturnStatus;
import com.bgeneral.ce.bc.clientecobis.common.dto.Status;
import com.bgeneral.ce.bc.clientecobis.common.util.catalogs.CodeCatalog;
import com.bgeneral.ce.bc.clientecobis.controller.dto.CredencialesClienteDTO;
import com.bgeneral.ce.bc.clientecobis.controller.dto.DatosClienteDTO;
import com.bgeneral.ce.bc.clientecobis.handler.command.ConsultarDatosClienteCommand;
import com.bgeneral.ce.bc.clientecobis.handler.command.impl.ConsultarDatosClienteCommandHandler;
import com.bgeneral.ce.bc.clientecobis.handler.dto.ClientePEPDTO;
import com.bgeneral.ce.bc.clientecobis.handler.dto.DatosClienteHandlerDTO;
import com.bgeneral.ce.bc.clientecobis.handler.dto.ReferenciaClienteDTO;
import com.bgeneral.ce.bc.clientecobis.handler.interfaces.IAmRestAdapter;

@RunWith(MockitoJUnitRunner.class)
public class ConsultarDatosClienteCommandTest {

	@Mock
	private IAmRestAdapter<EmptyBodyT, DatosClienteHandlerDTO> consultarDatosClienteCobisAdapter;

	@Mock
	private IAmRestAdapter<EmptyBodyT, ClientePEPDTO> consultarClientePEPAdapter;

	@Mock
	private IAmRestAdapter<EmptyBodyT, ReferenciaClienteDTO> consultarReferenciaClienteAdapter;

	@InjectMocks
	private ConsultarDatosClienteCommandHandler sut;

	@Test
	public void shouldReturnSuccessBusinessExecution() {

		when(consultarDatosClienteCobisAdapter.callRestService(Mockito.any()))
				.thenReturn(getResponseCallRestServices());
		when(consultarClientePEPAdapter.callRestService(Mockito.any()))
				.thenReturn(getResponseCallRestServicesPEPAdapter());
		when(consultarReferenciaClienteAdapter.callRestService(Mockito.any()))
				.thenReturn(getResponseCallRestServicesReferenciaAdapter());
		
		BCRequest<CredencialesClienteDTO> request = new BCRequest<>();
		request.setHeader(this.getHeader());
		request.setBody(this.getBody());

		ConsultarDatosClienteCommand clienteCommand = new ConsultarDatosClienteCommand(request);
		BCResponse<DatosClienteDTO> bcResponse = sut.handle(clienteCommand);

		assertThat(bcResponse, instanceOf(BCResponse.class));
		// Si es nulo
		assertThat(bcResponse.getStatus().getReturnStatus().getReturnCode(), is(notNullValue()));
		// Si es <> U0000
		assertThat(bcResponse.getStatus().getReturnStatus().getReturnCode(), equalTo("U0000"));
	}

	private BCResponse<ReferenciaClienteDTO> getResponseCallRestServicesReferenciaAdapter() {
		BCResponse<ReferenciaClienteDTO> bcResponse = new BCResponse<>();
		bcResponse.setHeader(getHeader());
		bcResponse.setBody(new ReferenciaClienteDTO());
		bcResponse.getBody().setTieneReferencia(true);
		return bcResponse;
	}

	private BCResponse<ClientePEPDTO> getResponseCallRestServicesPEPAdapter() {
		BCResponse<ClientePEPDTO> bcResponse = new BCResponse<>();
		bcResponse.setHeader(getHeader());
		bcResponse.setBody(new ClientePEPDTO());
		bcResponse.getBody().setEsPEP(true);
		return bcResponse;
	}

	@Test(expected = NullPointerException.class)
	public void shouldHandleNullRequest() {

		sut.handle(null);

	}

	private Header getHeader() {
		Header headerRest = new Header();
		// Cabecera
		headerRest.setNumeroUnico(1);
		headerRest.setOficina(1);
		headerRest.setTerminal("1");
		headerRest.setUsuario("bccanales");
		headerRest.setRolUsuario(3);
		// CabeceraCanales
		headerRest.setAplicacion("1");
		headerRest.setCanal("1");
		headerRest.setFecha(new Date());
		headerRest.setNodo(1);
		headerRest.setSesionBg("1234");
		headerRest.setDireccionIp("");
		headerRest.setOrigenSolicitud("bel");
		headerRest.setNumeroReferencia("1234");
		headerRest.setHoraInicio("12:00:00");

		// Perfil
		Perfil perfil = new Perfil();
		perfil.setLogin("");
		perfil.setNumeroCliente(12);
		perfil.setNumeroBancaVirtual(0);
		perfil.setAlias("prueba");
		perfil.setNumeroPerfil(3);

		headerRest.setPerfil(perfil);
		return headerRest;
	}

	private Status getStatus() {
		Status status = new Status();
		ReturnStatus returnStatus = new ReturnStatus();
		returnStatus.setReturnCode(CodeCatalog.CODE_SUCCESS.codeS());
		returnStatus.setReturnCodeDesc(CodeCatalog.S_MSG_CODE_SUCCESS.codeS());
		status.setReturnStatus(returnStatus);
		return status;
	}

	private BCResponse<DatosClienteHandlerDTO> getResponseCallRestServices() {
		BCResponse<DatosClienteHandlerDTO> bcResponse = new BCResponse<>();
		bcResponse.setBody(loadBody());
		bcResponse.setHeader(this.getHeader());
		bcResponse.setStatus(this.getStatus());
		return bcResponse;
	}

	private CredencialesClienteDTO getBody() {
		CredencialesClienteDTO credencialesClienteDTO = new CredencialesClienteDTO();
		credencialesClienteDTO.setCedula("123-123-123-123");
		credencialesClienteDTO.setFormato(1);
		credencialesClienteDTO.setNumeroRegistros(1);
		credencialesClienteDTO.setOnboarding(true);
		return credencialesClienteDTO;

	}

	private DatosClienteHandlerDTO loadBody() {
		DatosClienteHandlerDTO body = new DatosClienteHandlerDTO();

		body.setNombre("MARIA");
		body.setSegundonombre("EVA");
		body.setApellidoPaterno("CARABALLO");
		body.setApellidoMaterno("GONZALEZ");
		body.setApellidoCasada("GUDINO");
		body.setCedula("09  009600475");
		body.setEdad(62);
		body.setAutorizacionAPC(true);
		body.setCantidadCuentasPasivas(2);
		body.setClienteEsPep(false);
		body.setClienteTienePantallazo(false);
		body.setCodigoMalaRef("00");
		body.setCodigoOficial(1035);
		body.setComentario("PRESTAMO Personal");
		body.setDescripcionOficial("Oficial de sucursal");
		body.setDifunto("S");
		body.setFechaExp(new Date());
		body.setFechaIngreso(new Date());
		body.setFechaInicioRelacion("01/01/2000");
		body.setFechaNacimiento(new Date());
		body.setFechaRegistro(new Date());
		body.setFechaUltimaModificacion(new Date());
		body.setFechaVerificacion(new Date());
		body.setGrupoRel("S");

		Catalogo catalogo = new Catalogo();

		catalogo.setCodigo("00");
		catalogo.setDescripcion("COSTURERA");
		body.setActividad(catalogo);

		catalogo.setCodigo("00");
		catalogo.setDescripcion("COSTURERA");
		body.setClasificacion(catalogo);

		catalogo.setCodigo("00");
		catalogo.setDescripcion("COSTURERA");
		body.setClienteBanca(catalogo);

		catalogo.setCodigo("00");
		catalogo.setDescripcion("COSTURERA");
		body.setCodigoCalificacion(catalogo);

		catalogo.setCodigo("00");
		catalogo.setDescripcion("COSTURERA");
		body.setEducacion(catalogo);

		catalogo.setCodigo("00");
		catalogo.setDescripcion("COSTURERA");
		body.setEstadoCivil(catalogo);

		catalogo.setCodigo("00");
		catalogo.setDescripcion("COSTURERA");
		body.setEstatus(catalogo);

		catalogo.setCodigo("00");
		catalogo.setDescripcion("COSTURERA");
		body.setEvaluacionCumplimiento(catalogo);

		catalogo.setCodigo("00");
		catalogo.setDescripcion("COSTURERA");
		body.setGrupo(catalogo);

		catalogo.setCodigo("00");
		catalogo.setDescripcion("COSTURERA");
		body.setOficialSector(catalogo);

		catalogo.setCodigo("00");
		catalogo.setDescripcion("COSTURERA");
		body.setOficialSubSector(catalogo);

		catalogo.setCodigo("00");
		catalogo.setDescripcion("COSTURERA");
		body.setOficialTipo(catalogo);

		catalogo.setCodigo("00");
		catalogo.setDescripcion("COSTURERA");
		body.setPaisNacimiento(catalogo);

		catalogo.setCodigo("00");
		catalogo.setDescripcion("COSTURERA");
		body.setPaisRiesgo(catalogo);

		catalogo.setCodigo("00");
		catalogo.setDescripcion("COSTURERA");
		body.setProfesion(catalogo);

		catalogo.setCodigo("00");
		catalogo.setDescripcion("COSTURERA");
		body.setRelacion(catalogo);

		catalogo.setCodigo("00");
		catalogo.setDescripcion("COSTURERA");
		body.setSexo(catalogo);

		catalogo.setCodigo("00");
		catalogo.setDescripcion("COSTURERA");
		body.setSubClasificacion(catalogo);

		catalogo.setCodigo("00");
		catalogo.setDescripcion("COSTURERA");
		body.setTipoPersona(catalogo);

		catalogo.setCodigo("00");
		catalogo.setDescripcion("COSTURERA");
		body.setUsuario(catalogo);

		return body;
	}
}
