package com.bgeneral.ce.bc.clientecobis.handler;

import static org.hamcrest.CoreMatchers.equalTo;
import static org.hamcrest.CoreMatchers.instanceOf;
import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.CoreMatchers.notNullValue;
import static org.junit.Assert.assertThat;
import static org.mockito.Mockito.when;

import java.util.Date;

import javax.xml.datatype.DatatypeConfigurationException;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.MockitoJUnitRunner;

import com.bgeneral.ce.bc.clientecobis.common.dto.BCRequest;
import com.bgeneral.ce.bc.clientecobis.common.dto.BCResponse;
import com.bgeneral.ce.bc.clientecobis.common.dto.Catalogo;
import com.bgeneral.ce.bc.clientecobis.common.dto.Header;
import com.bgeneral.ce.bc.clientecobis.common.dto.Perfil;
import com.bgeneral.ce.bc.clientecobis.common.dto.ReturnStatus;
import com.bgeneral.ce.bc.clientecobis.common.dto.Status;
import com.bgeneral.ce.bc.clientecobis.common.exceptions.BussinesCapabilityException;
import com.bgeneral.ce.bc.clientecobis.controller.dto.CodigosRegistroClienteDTO;
import com.bgeneral.ce.bc.clientecobis.controller.dto.CorreoElectronicoCanalesDTO;
import com.bgeneral.ce.bc.clientecobis.controller.dto.DatosClientesCanalesDTO;
import com.bgeneral.ce.bc.clientecobis.controller.dto.DireccionClienteCanalesDTO;
import com.bgeneral.ce.bc.clientecobis.controller.dto.TelefonoClienteCanalesDTO;
import com.bgeneral.ce.bc.clientecobis.handler.command.CrearClienteCommand;
import com.bgeneral.ce.bc.clientecobis.handler.command.impl.CrearClienteCommandHandler;
import com.bgeneral.ce.bc.clientecobis.handler.dto.CuentaCorreaClienteDTO;
import com.bgeneral.ce.bc.clientecobis.handler.dto.DireccionClienteDTO;
import com.bgeneral.ce.bc.clientecobis.handler.dto.EnteClienteDTO;
import com.bgeneral.ce.bc.clientecobis.handler.dto.InformacionClienteDTO;
import com.bgeneral.ce.bc.clientecobis.handler.dto.TelefonoClienteDTO;
import com.bgeneral.ce.bc.clientecobis.handler.dto.ValorCorreoDTO;
import com.bgeneral.ce.bc.clientecobis.handler.dto.ValorDireccionDTO;
import com.bgeneral.ce.bc.clientecobis.handler.dto.ValorTelefonoDTO;
import com.bgeneral.ce.bc.clientecobis.handler.interfaces.IAmRestAdapter;

@RunWith(MockitoJUnitRunner.class)
public class CrearClienteHandlerTest {
	
	@Mock
	private IAmRestAdapter<InformacionClienteDTO, EnteClienteDTO> crearClienteCobisAdapter;
	
	@Mock
	private IAmRestAdapter<CuentaCorreaClienteDTO, ValorCorreoDTO> crearCorreoClienteCobisAdapter;

	@Mock
	private IAmRestAdapter<DireccionClienteDTO, ValorDireccionDTO> crearDireccionClienteCobisAdapter;

	@Mock
	private IAmRestAdapter<TelefonoClienteDTO, ValorTelefonoDTO> crearTelefonoClienteCobisAdapter;
	
	@InjectMocks
	private CrearClienteCommandHandler sut;
	
	@Test
	public void shouldReturnSuccessBusinessExecution() throws BussinesCapabilityException, Exception {

		when(crearClienteCobisAdapter.callRestService(Mockito.any())).thenReturn(getResponseClienteCallRestServices());
		when(crearCorreoClienteCobisAdapter.callRestService(Mockito.any())).thenReturn(getResponseCorreoClienteCallRestServices());
		when(crearDireccionClienteCobisAdapter.callRestService(Mockito.any())).thenReturn(getResponseDireccionClienteCallRestServices());
		when(crearTelefonoClienteCobisAdapter.callRestService(Mockito.any())).thenReturn(getResponseTelefonoClienteCallRestServices());

		CrearClienteCommand crearClienteCommand = new CrearClienteCommand(getRequestCommand());
		
				
		BCResponse<CodigosRegistroClienteDTO> bcResponse = sut.handle(crearClienteCommand);
		
		assertThat(bcResponse, instanceOf(BCResponse.class));
		// Si es nulo
		assertThat(bcResponse.getStatus().getReturnStatus().getReturnCode(), is(notNullValue()));
		// Si es <> U0000
		assertThat(bcResponse.getStatus().getReturnStatus().getReturnCode(), equalTo("U0000"));

	}
		
	private BCResponse<ValorTelefonoDTO> getResponseTelefonoClienteCallRestServices() {
		BCResponse<ValorTelefonoDTO> bcResponse = new BCResponse<>();
		bcResponse.setHeader(getHeader());
		bcResponse.setStatus(getStatus("U0000", "Exito"));
		return bcResponse;
	}

	private BCResponse<ValorDireccionDTO> getResponseDireccionClienteCallRestServices() {
		BCResponse<ValorDireccionDTO> bcResponse = new BCResponse<>();
		bcResponse.setBody(new ValorDireccionDTO());
		bcResponse.getBody().setCodigoDireccion("CODIGO");
		bcResponse.setHeader(getHeader());
		bcResponse.setStatus(getStatus("U0000", "Exito"));
		return bcResponse;
	}

	private BCResponse<ValorCorreoDTO> getResponseCorreoClienteCallRestServices() {
		BCResponse<ValorCorreoDTO> bcResponse = new BCResponse<>();
		bcResponse.setHeader(getHeader());
		bcResponse.setStatus(getStatus("U0000", "Exito"));
		return bcResponse;
	}

	// Body Rest Service
	private BCRequest<DatosClientesCanalesDTO> getRequestCommand() throws DatatypeConfigurationException {
		BCRequest<DatosClientesCanalesDTO> bcRequest = new BCRequest<>();
		bcRequest.setHeader(getHeader());
		
		Catalogo catalogo = new Catalogo();
		
		
		DatosClientesCanalesDTO body = new DatosClientesCanalesDTO();
		body.setPrimerNombre("LUCAS");
		body.setPrimerApellido("TROTACIELOS");
		body.setCedula("8-906-2217");
		body.setFechaNacimiento(new Date());

		TelefonoClienteCanalesDTO telefonoDTO = new TelefonoClienteCanalesDTO();
		telefonoDTO.setExtencion("1230");
		telefonoDTO.setNumeroTelefono("32549865");
		telefonoDTO.setTipoTelefono("P");
		body.setTelefono(telefonoDTO);
		
		CorreoElectronicoCanalesDTO correoDTO = new CorreoElectronicoCanalesDTO();
		correoDTO.setCodigo("0120");
		correoDTO.setDireccionEmail("direccion@email.com");
		correoDTO.setTipoMail("P");
		body.setCorreoElectronico(correoDTO);
		
		DireccionClienteCanalesDTO direccion = new DireccionClienteCanalesDTO();
		direccion.setCalle("CALLE");
		
		catalogo.setCodigo("BARRIO");
		direccion.setBarrio(catalogo);
		
		catalogo.setCodigo("CIUDAD");
		direccion.setCiudad(catalogo);
		
		direccion.setDescripcion("descripcionDireccion");
		direccion.setCodigoCasa("0102");		

		catalogo.setCodigo("corregimiento");
		direccion.setCorregimiento(catalogo);
		
		direccion.setEsPrincipal(true);
		
		catalogo.setCodigo("oficina");
		direccion.setOficina(catalogo);

		catalogo.setCodigo("paisdireccion");
		direccion.setPais(catalogo);

		catalogo.setCodigo("tipoDireccion");
		direccion.setTipoDireccion(catalogo);
		body.setDireccion(direccion );

		catalogo.setCodigo("M");
		body.setSexo(catalogo);
		
		catalogo.setCodigo("S");
		body.setEstadoCivil(catalogo);
		
		catalogo.setCodigo("61");
		body.setPaisNacimiento(catalogo);
		
		catalogo.setCodigo("61");
		body.setPaisNacionalidad(catalogo);
		
		catalogo.setCodigo("61");
		body.setPaisRiesgo(catalogo);
		
		bcRequest.setBody(body);
		return bcRequest;
	}

	private BCResponse<EnteClienteDTO> getResponseClienteCallRestServices() throws DatatypeConfigurationException  {
		BCResponse<EnteClienteDTO> bcResponse = new BCResponse<>();
		bcResponse.setHeader(getHeader());
		bcResponse.setStatus(getStatus("U0000", "Exito"));
		bcResponse.setBody(getBodyRestService());
		return bcResponse;
	}

	private EnteClienteDTO getBodyRestService() {
		EnteClienteDTO bodyRest = new EnteClienteDTO();
		bodyRest.setEnteMis("1234");
		return bodyRest;
	}
	
	private Status getStatus(String returnCode, String returnDesc) {
		Status status = new Status();
		ReturnStatus returnStatus = new ReturnStatus();
		returnStatus.setReturnCode(returnCode);
		returnStatus.setReturnCodeDesc(returnDesc);
		status.setReturnStatus(returnStatus);
		return status;
	}

	
	private Header getHeader(){
		Header headerRest = new Header();
		// Cabecera
		headerRest.setNumeroUnico(1);
		headerRest.setOficina(1);
		headerRest.setTerminal("1");
		headerRest.setUsuario("bc-canales");
		headerRest.setRolUsuario(3);
		// CabeceraCanales
		headerRest.setAplicacion("1");
		headerRest.setCanal("1");

		headerRest.setFecha(new Date());
		headerRest.setNodo(1);
		headerRest.setSesionBg("1234");
		headerRest.setDireccionIp("");
		headerRest.setOrigenSolicitud("bel");
		headerRest.setNumeroReferencia("1234");
		headerRest.setHoraInicio("10:12:10");

		// Perfil
		Perfil perfil = new Perfil();
		perfil.setLogin("");
		perfil.setNumeroCliente(12);
		perfil.setNumeroBancaVirtual(0);
		perfil.setAlias("bc-canales");
		perfil.setNumeroPerfil(3);

		headerRest.setPerfil(perfil);
		return headerRest;
	}
}
