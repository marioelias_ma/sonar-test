package com.bgeneral.ce.bc.clientecobis.handler;

import static org.hamcrest.CoreMatchers.equalTo;
import static org.hamcrest.CoreMatchers.instanceOf;
import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.CoreMatchers.notNullValue;
import static org.junit.Assert.assertThat;
import static org.mockito.Mockito.when;

import java.util.Date;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.MockitoJUnitRunner;

import com.bgeneral.ce.bc.clientecobis.common.dto.BCRequest;
import com.bgeneral.ce.bc.clientecobis.common.dto.BCResponse;
import com.bgeneral.ce.bc.clientecobis.common.dto.Catalogo;
import com.bgeneral.ce.bc.clientecobis.common.dto.EmptyBodyT;
import com.bgeneral.ce.bc.clientecobis.common.dto.Header;
import com.bgeneral.ce.bc.clientecobis.common.dto.Perfil;
import com.bgeneral.ce.bc.clientecobis.common.dto.ReturnStatus;
import com.bgeneral.ce.bc.clientecobis.common.dto.Status;
import com.bgeneral.ce.bc.clientecobis.common.util.catalogs.CodeCatalog;
import com.bgeneral.ce.bc.clientecobis.controller.dto.DatosDireccionClienteCobisDTO;
import com.bgeneral.ce.bc.clientecobis.handler.command.ConsultarDireccionClienteCommand;
import com.bgeneral.ce.bc.clientecobis.handler.command.impl.ConsultarDireccionClienteCommandHandler;
import com.bgeneral.ce.bc.clientecobis.handler.dto.DatosDireccionClienteCobisHandlerDTO;
import com.bgeneral.ce.bc.clientecobis.handler.interfaces.IAmRestAdapter;

@RunWith(MockitoJUnitRunner.class)
public class ConsultarDireccionClienteCommandHandlerTest {
	
	@Mock
	private IAmRestAdapter<EmptyBodyT, DatosDireccionClienteCobisHandlerDTO> consultarDireccionClienteCobisAdapter;
	
	@InjectMocks
	private ConsultarDireccionClienteCommandHandler sut;
	
	@Test
	public void shouldReturnSuccessBusinessExecution(){
		
		when(consultarDireccionClienteCobisAdapter.callRestService(Mockito.any())).thenReturn(getResponseCallRestServices());
		
		BCRequest<EmptyBodyT> request = new BCRequest<>();
		request.setHeader(this.getHeader());
		
		ConsultarDireccionClienteCommand clienteCommand = new ConsultarDireccionClienteCommand(request);
		BCResponse<DatosDireccionClienteCobisDTO> bcResponse = sut.handle(clienteCommand);
		
		assertThat(bcResponse, instanceOf(BCResponse.class));
		// Si es nulo
		assertThat(bcResponse.getStatus().getReturnStatus().getReturnCode(), is(notNullValue()));
		// Si es <> U0000
		assertThat(bcResponse.getStatus().getReturnStatus().getReturnCode(), equalTo("U0000"));
		
	}
	
	private BCResponse<DatosDireccionClienteCobisHandlerDTO> getResponseCallRestServices() {
		BCResponse<DatosDireccionClienteCobisHandlerDTO> bcResponse = new BCResponse<>();
		bcResponse.setBody(loadBody());
		bcResponse.setHeader(this.getHeader());
		bcResponse.setStatus(this.getStatus());
		return bcResponse;
	}

	private DatosDireccionClienteCobisHandlerDTO loadBody() {
		DatosDireccionClienteCobisHandlerDTO body = new DatosDireccionClienteCobisHandlerDTO();
		
		body.setCalle("CALLE");
		body.setEdificioCasa("EDIFICIO CASA");
		body.setFuncionario("FUNCIONARIO");
		body.setTipoDireccion("TIPO DIRECCION");

		body.setEsPrincipal(true);
		body.setEsVerificado(false);
		body.setEsVigente(false);

		body.setFechaModificacion(new Date());
		body.setFechaRegistro(new Date());
		body.setFechaVerificacion(new Date());

		Catalogo catalogo = new Catalogo();

		catalogo.setCodigo("BARRIO_COD");
		catalogo.setDescripcion("Barrio Descripcion");
		body.setBarrio(catalogo);

		catalogo.setCodigo("CIUDAD_COD");
		catalogo.setDescripcion("Ciudad Descripcion");
		body.setCiudad(catalogo);

		catalogo.setCodigo("CORREGIMIENTO_COD");
		catalogo.setDescripcion("Corregimiento Descripcion");
		body.setCorregimiento(catalogo);

		catalogo.setCodigo("DIRECCION_COD");
		catalogo.setDescripcion("Direccion Descripcion");
		body.setDireccion(catalogo);

		catalogo.setCodigo("OFICINA_COD");
		catalogo.setDescripcion("Oficina Descripcion");
		body.setOficina(catalogo);

		catalogo.setCodigo("PAIS_COD");
		catalogo.setDescripcion("Pais Descripcion");
		body.setPais(catalogo);

		catalogo.setCodigo("SECTOR_COD");
		catalogo.setDescripcion("Sector Descripcion");
		body.setSector(catalogo);

		catalogo.setCodigo("ZONA_COD");
		catalogo.setDescripcion("Zona Descripcion");
		body.setZona(catalogo);

		return body;
	}

	private Header getHeader() {
		Header headerRest = new Header();
		// Cabecera
		headerRest.setNumeroUnico(1);
		headerRest.setOficina(1);
		headerRest.setTerminal("1");
		headerRest.setUsuario("bccanales");
		headerRest.setRolUsuario(3);
		// CabeceraCanales
		headerRest.setAplicacion("1");
		headerRest.setCanal("1");
		headerRest.setFecha(new Date());
		headerRest.setNodo(1);
		headerRest.setSesionBg("1234");
		headerRest.setDireccionIp("");
		headerRest.setOrigenSolicitud("bel");
		headerRest.setNumeroReferencia("1234");
		headerRest.setHoraInicio("12:00:00");

		// Perfil
		Perfil perfil = new Perfil();
		perfil.setLogin("");
		perfil.setNumeroCliente(12);
		perfil.setNumeroBancaVirtual(0);
		perfil.setAlias("prueba");
		perfil.setNumeroPerfil(3);

		headerRest.setPerfil(perfil);
		return headerRest;
	}

	private Status getStatus() {
		Status status = new Status();
		ReturnStatus returnStatus = new ReturnStatus();
		returnStatus.setReturnCode(CodeCatalog.CODE_SUCCESS.codeS());
		returnStatus.setReturnCodeDesc(CodeCatalog.S_MSG_CODE_SUCCESS.codeS());
		status.setReturnStatus(returnStatus);
		return status;
	}

}
