package com.bgeneral.ce.bc.clientecobis.util;

import static org.mockito.Mockito.when;

import java.io.IOException;

import org.junit.After;
import org.junit.Before;
import org.mockito.Mock;
import org.springframework.boot.test.context.SpringBootTest;

import com.bgeneral.ce.bc.clientecobis.common.util.configuration.ConfigurationAppProperties;

import okhttp3.mockwebserver.MockResponse;
import okhttp3.mockwebserver.MockWebServer;

@SpringBootTest
public class MockServer {
	
	protected static MockWebServer mockWebServer;
	protected static String host = "http://localhost:";
	
	@Mock
	protected ConfigurationAppProperties configurationAppProperties;

	@Mock
	protected ConfigurationAppProperties.ClientConf clientConf;
		
	@Before
	public void startProperties() throws IOException {
		mockWebServer = new MockWebServer();
		mockWebServer.start();
	}

	@After
	public void stopServer() throws IOException {
		mockWebServer.shutdown();
	}
	
	protected static void setUpMockResponse(String messageBody) {
		MockResponse response = new MockResponse()
				.setResponseCode(200)
			    .addHeader("Content-Type", "application/json; charset=utf-8")
			    .addHeader("Cache-Control", "no-cache")
			    .setBody(messageBody);
        mockWebServer.enqueue(response);
        
	}
	
	protected static void setUpMockGatewayTimeOut() {
		MockResponse response = new MockResponse()
				.setResponseCode(504)
	    		.setStatus("Gateway Timeout");
        mockWebServer.enqueue(response);
	}
	
	protected static void setUpMockResponseWithFailure(int codeErrorHttp) {
		MockResponse response = new MockResponse()
				.setResponseCode(codeErrorHttp)
	    		.setStatus("HTTP/1.1 " + codeErrorHttp + " Internal Server Error")
			    .addHeader("Content-Type", "application/json; charset=utf-8")
			    .addHeader("Cache-Control", "no-cache");
        mockWebServer.enqueue(response);
	}
	
	protected static void setUpMockResponseWithContenType(String messageBody, String contentType) {
		MockResponse response = new MockResponse()
				.setResponseCode(200)
			    .addHeader("Content-Type", contentType)
			    .addHeader("Cache-Control", "no-cache")
			    .setBody(messageBody);
        mockWebServer.enqueue(response);
	}
	
	protected static void setUpMockResponseWithHttpCode(String messageBody, int httpCode) {
		MockResponse response = new MockResponse()
				.setResponseCode(httpCode)
			    .addHeader("Content-Type", "application/json; charset=utf-8")
			    .addHeader("Cache-Control", "no-cache")
			    .setBody(messageBody);
        mockWebServer.enqueue(response);
	}
	
	protected static void setUpMockResponseWithContenTypeAndHttpCode(String messageBody, String contentType, int httpCode) {
		MockResponse response = new MockResponse()
				.setResponseCode(httpCode)
			    .addHeader("Content-Type", contentType)
			    .addHeader("Cache-Control", "no-cache")
			    .setBody(messageBody);
        mockWebServer.enqueue(response);
	}
	
	protected void modifyPropertyHost() {
		when(configurationAppProperties.getRestEndpoint()).thenReturn(clientConf);
		when(clientConf.getHost()).thenReturn( host + mockWebServer.getPort());
	}
	
	protected void modifyPropertyHost(String host) {
		when(configurationAppProperties.getRestEndpoint()).thenReturn(clientConf);
		when(clientConf.getHost()).thenReturn( host + mockWebServer.getPort());
	}
	
	protected void modifyPropertyPort(String port) {
		when(configurationAppProperties.getRestEndpoint()).thenReturn(clientConf);
		when(clientConf.getHost()).thenReturn( host + port);
	}
	
	protected void modifyPropertyHostAndPort(String host, String port) {
		when(configurationAppProperties.getRestEndpoint()).thenReturn(clientConf);
		when(clientConf.getHost()).thenReturn( host + port);
	}

}
