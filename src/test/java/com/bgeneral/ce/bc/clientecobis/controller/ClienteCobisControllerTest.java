//package com.bgeneral.ce.bc.clientecobis.controller;
//
//import static org.hamcrest.CoreMatchers.equalTo;
//import static org.hamcrest.CoreMatchers.not;
//import static org.junit.Assert.assertThat;
//import static org.mockito.ArgumentMatchers.any;
//import static org.mockito.Mockito.when;
//import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
//import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
//import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;
//
//import java.util.Date;
//
//import org.junit.Before;
//import org.junit.Test;
//import org.junit.runner.RunWith;
//import org.mockito.Mock;
//import org.springframework.beans.factory.annotation.Autowired;
//import org.springframework.boot.test.context.SpringBootTest;
//import org.springframework.http.MediaType;
//import org.springframework.test.context.junit4.SpringRunner;
//import org.springframework.test.web.servlet.MockMvc;
//import org.springframework.test.web.servlet.MvcResult;
//import org.springframework.test.web.servlet.setup.MockMvcBuilders;
//import org.springframework.web.context.WebApplicationContext;
//
//import com.bgeneral.ce.bc.clientecobis.common.dto.BCResponse;
//import com.bgeneral.ce.bc.clientecobis.common.dto.EmptyBodyT;
//import com.bgeneral.ce.bc.clientecobis.common.dto.Header;
//import com.bgeneral.ce.bc.clientecobis.common.dto.Perfil;
//import com.bgeneral.ce.bc.clientecobis.common.dto.ReturnStatus;
//import com.bgeneral.ce.bc.clientecobis.common.dto.Status;
//import com.bgeneral.ce.bc.clientecobis.common.util.catalogs.CodeCatalog;
//import com.bgeneral.ce.bc.clientecobis.controller.dto.CodigosRegistroClienteDTO;
//import com.bgeneral.ce.bc.clientecobis.controller.dto.DatosClienteDTO;
//import com.bgeneral.ce.bc.clientecobis.controller.dto.DatosClientesCanalesDTO;
//import com.bgeneral.ce.bc.clientecobis.controller.interfaces.IAmCommandHandler;
//import com.bgeneral.ce.bc.clientecobis.handler.command.ConsultarDatosClienteCommand;
//import com.bgeneral.ce.bc.clientecobis.handler.command.CrearClienteCommand;
//import com.fasterxml.jackson.databind.ObjectMapper;
//
///*
// * @author: F. Castillo
// * 
// */
//
//@SpringBootTest
//@RunWith(SpringRunner.class)
//public class ClienteCobisControllerTest {
//	
//	private MockMvc mockMvc;
//	
//	@Autowired
//	private WebApplicationContext context;
//
//	@Mock
//	private IAmCommandHandler<CrearClienteCommand, DatosClientesCanalesDTO, CodigosRegistroClienteDTO> logDecoratorHandler;
//	
////	@Mock
////	private IAmCommandHandler<ConsultarDatosClienteCommand, EmptyBodyT, DatosClienteDTO> logDecoratorHandler;
//	
//	private ObjectMapper om = new ObjectMapper();
//
//	@Before
//	public void setUp() {
//		mockMvc = MockMvcBuilders.webAppContextSetup(context).build();
//	}
//	
//	@Test
//	public void validateHealthCheck() throws Exception {
//
//		mockMvc.perform(
//					post("/api/ce/bc/cliente/healthcheck")
//					.contentType(MediaType.APPLICATION_JSON))
//				.andExpect(status().isOk())
//				.andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE));
//	}
//	
////	@Test
////	public void validateCrearCliente() throws Exception {
////		BCResponse<CodigosRegistroClienteDTO> bcResponse = new BCResponse<>();
////		bcResponse.setBody(getBody());
////		bcResponse.setHeader(getHeader());
////		bcResponse.setStatus(getStatus());
////
////		when(logDecoratorHandler.handle(any())).thenReturn(bcResponse);
////
////		MvcResult mvcResult = mockMvc.perform(
////					post("/api/ce/bc/cliente/crearcliente")
////					.content(om.writeValueAsString(bcResponse))
////					.contentType(MediaType.APPLICATION_JSON))
////				.andExpect(status().isOk())
////				.andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
////				.andReturn();
////		String resultContent = mvcResult.getResponse().getContentAsString();
////		BCResponse<CodigosRegistroClienteDTO> response = om.readValue(resultContent, BCResponse.class);
////		assertThat(response.getStatus().getReturnStatus().getReturnCode(), not(equalTo("U0000")));
////
////	}
//	
//	
//	
////	@Test
////	public void validateConsultarCliente() throws Exception {
////		BCResponse<DatosClienteDTO> bcResponse = new BCResponse<>();
////		bcResponse.setHeader(getHeader());
////		bcResponse.setStatus(getStatus());
////
////		when(logDecoratorHandler.handle(any())).thenReturn(bcResponse);
////
////		MvcResult mvcResult = mockMvc.perform(
////					post("/api/ce/bc/cliente/consultar")
////					.content(om.writeValueAsString(bcResponse))
////					.contentType(MediaType.APPLICATION_JSON))
////				.andExpect(status().isOk())
////				
////				.andReturn();
////		String resultContent = mvcResult.getResponse().getContentAsString();
////		BCResponse<DatosClienteDTO> response = om.readValue(resultContent, BCResponse.class);
////		assertThat(response.getStatus().getReturnStatus().getReturnCode(), not(equalTo("U0000")));
////
////	}
//	
//	
//	private CodigosRegistroClienteDTO getBody() {
//		CodigosRegistroClienteDTO body = new CodigosRegistroClienteDTO();
//		body.setEnteMis(6999);
//		return body;
//	}
//	
//	private DatosClienteDTO getBodyDatosClienteDTO() {
//		DatosClienteDTO body = new DatosClienteDTO();
//		body.setApellidoCasada("Test");;
//		return body;
//	}
//	
//	private Header getHeader() {
//		Header headerRest = new Header();
//		// Cabecera
//		headerRest.setNumeroUnico(1);
//		headerRest.setOficina(1);
//		headerRest.setTerminal("1");
//		headerRest.setUsuario("bccanales");
//		headerRest.setRolUsuario(3);
//		// CabeceraCanales
//		headerRest.setAplicacion("1");
//		headerRest.setCanal("1");
//
//		headerRest.setFecha(new Date());
//		headerRest.setNodo(1);
//		headerRest.setSesionBg("1234");
//		headerRest.setDireccionIp("");
//		headerRest.setOrigenSolicitud("bel");
//		headerRest.setNumeroReferencia("1234");
//		headerRest.setHoraInicio("");
//
//		// Perfil
//		Perfil perfil = new Perfil();
//		perfil.setLogin("");
//		perfil.setNumeroCliente(12);
//		perfil.setNumeroBancaVirtual(0);
//		perfil.setAlias("prueba");
//		perfil.setNumeroPerfil(3);
//
//		headerRest.setPerfil(perfil);
//		return headerRest;
//	}
//	
//	private Status getStatus() {
//		Status status = new Status();
//		ReturnStatus returnStatus = new ReturnStatus();
//		returnStatus.setReturnCode(CodeCatalog.CODE_SUCCESS.codeS());
//		returnStatus.setReturnCodeDesc(CodeCatalog.S_MSG_CODE_SUCCESS.codeS());
//		status.setReturnStatus(returnStatus);
//		return status;
//	}
//}
