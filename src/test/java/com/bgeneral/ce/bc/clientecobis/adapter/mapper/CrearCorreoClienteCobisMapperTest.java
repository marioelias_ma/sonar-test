package com.bgeneral.ce.bc.clientecobis.adapter.mapper;

import static org.hamcrest.CoreMatchers.equalTo;
import static org.junit.Assert.assertThat;

import java.util.Date;

import javax.xml.datatype.DatatypeConfigurationException;

import org.hamcrest.CoreMatchers;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.junit.MockitoJUnitRunner;

import com.bgeneral.ce.bc.clientecobis.adapter.dto.CrearCorreoClienteCobisRespDTO;
import com.bgeneral.ce.bc.clientecobis.adapter.dto.CrearCorreoClienteCobisRqDTO;
import com.bgeneral.ce.bc.clientecobis.adapter.mapper.impl.CrearCorreoClienteCobisMapper;
import com.bgeneral.ce.bc.clientecobis.common.dto.BCRequest;
import com.bgeneral.ce.bc.clientecobis.common.dto.BCResponse;
import com.bgeneral.ce.bc.clientecobis.common.dto.Header;
import com.bgeneral.ce.bc.clientecobis.common.dto.Perfil;
import com.bgeneral.ce.bc.clientecobis.common.dto.ReturnStatus;
import com.bgeneral.ce.bc.clientecobis.common.dto.Status;
import com.bgeneral.ce.bc.clientecobis.handler.dto.CuentaCorreaClienteDTO;
import com.bgeneral.ce.bc.clientecobis.handler.dto.ValorCorreoDTO;


@RunWith(MockitoJUnitRunner.class)
public class CrearCorreoClienteCobisMapperTest {

	@InjectMocks
	private CrearCorreoClienteCobisMapper sut; 

	private Header getHeader() {
		Header headerRest = new Header();
		// Cabecera
		headerRest.setNumeroUnico(1);
		headerRest.setOficina(1);
		headerRest.setTerminal("1");
		headerRest.setUsuario("twen");
		headerRest.setRolUsuario(3);
		// CabeceraCanales
		headerRest.setAplicacion("1");
		headerRest.setCanal("1");	
	
		headerRest.setFecha(new Date());
		headerRest.setNodo(1);
		headerRest.setSesionBg("1234");
		headerRest.setDireccionIp("");
		headerRest.setOrigenSolicitud("bel");
		headerRest.setNumeroReferencia("1234");
		headerRest.setHoraInicio("sd");
		
		// Perfil
		Perfil perfil = new Perfil();
		perfil.setLogin("");
		perfil.setNumeroCliente(12);
		perfil.setNumeroBancaVirtual(12345);
		perfil.setAlias("prueba");
		perfil.setNumeroPerfil(3);

		headerRest.setPerfil(perfil);
		return headerRest;
	}
	
	private BCRequest<CuentaCorreaClienteDTO> getRequestHandler() {
		CuentaCorreaClienteDTO body = new CuentaCorreaClienteDTO();
		body.setDireccion("jdsajk@hot");  
		body.setTipoMail("p");
		body.setTipoUso("M");
		
		BCRequest<CuentaCorreaClienteDTO> bcRequest = new BCRequest<>();
		bcRequest.setBody(body);
		bcRequest.setHeader(getHeader());
		
		return bcRequest;
	}
	


	private BCResponse<CrearCorreoClienteCobisRespDTO> getResponseAdapter() {
		BCResponse<CrearCorreoClienteCobisRespDTO> response = new BCResponse<>();
		CrearCorreoClienteCobisRespDTO body = new CrearCorreoClienteCobisRespDTO();
		body.setCodigoCorreo(123);
		response.setBody(body);
		
		response.setStatus(new Status());
		ReturnStatus returnStatus = new ReturnStatus();
		returnStatus.setReturnCode("U0000");
		response.getStatus().setReturnStatus(returnStatus );

		response.setHeader(getHeader());
		return response;
	}
	
	
	/*
	 *  Testing Mapper to Request  
	 * 
	 */
	@Test
	public void validateRequestToAdapterDireccion() throws DatatypeConfigurationException {
		BCRequest<CrearCorreoClienteCobisRqDTO> bcRequest = sut.handlerRequestTOAdapterRequestDTO(getRequestHandler());
		assertThat(bcRequest.getBody().getDireccion(), CoreMatchers.containsString("jdsajk@hot"));
	}
	
	@Test(expected=IllegalArgumentException.class)
	public void validateRequestToAdapterDireccionNull() throws DatatypeConfigurationException {
		BCRequest<CuentaCorreaClienteDTO> request= getRequestHandler();
		request.setBody(null);
		sut.handlerRequestTOAdapterRequestDTO(request);
	}
	
	/*
	 *  Testing Mapper to Response
	 * 
	 */
	@Test
	public void validateResponseToHandler() throws DatatypeConfigurationException {
		BCResponse<ValorCorreoDTO> response =  sut.adapterResponseTOHandlerResponseDTO(getResponseAdapter());
		assertThat(response.getBody().getCodigoCorreo(), equalTo(123));
	}

	@Test(expected=NullPointerException.class)
	public void validateResponseToHandlerNull() throws DatatypeConfigurationException {
		BCResponse<CrearCorreoClienteCobisRespDTO> bcResponse = getResponseAdapter();
		bcResponse.setStatus(null);
		sut.adapterResponseTOHandlerResponseDTO(bcResponse);
	}
	
}
