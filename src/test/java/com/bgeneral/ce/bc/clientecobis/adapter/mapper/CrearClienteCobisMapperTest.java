package com.bgeneral.ce.bc.clientecobis.adapter.mapper;

import static org.hamcrest.CoreMatchers.equalTo;
import static org.hamcrest.CoreMatchers.instanceOf;
import static org.junit.Assert.assertThat;

import java.util.Date;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.junit.MockitoJUnitRunner;

import com.bgeneral.ce.bc.clientecobis.adapter.dto.ClienteCobisDTO;
import com.bgeneral.ce.bc.clientecobis.adapter.mapper.impl.CrearClienteCobisMapper;
import com.bgeneral.ce.bc.clientecobis.common.dto.BCRequest;
import com.bgeneral.ce.bc.clientecobis.common.dto.BCResponse;
import com.bgeneral.ce.bc.clientecobis.common.dto.Catalogo;
import com.bgeneral.ce.bc.clientecobis.common.dto.EmptyBodyT;
import com.bgeneral.ce.bc.clientecobis.common.dto.Header;
import com.bgeneral.ce.bc.clientecobis.common.dto.Perfil;
import com.bgeneral.ce.bc.clientecobis.common.dto.ReturnStatus;
import com.bgeneral.ce.bc.clientecobis.common.dto.Status;
import com.bgeneral.ce.bc.clientecobis.common.util.catalogs.CodeCatalog;
import com.bgeneral.ce.bc.clientecobis.handler.dto.InformacionClienteDTO;
/**
 * 
 * @author jolivero
 *
 */
@RunWith(MockitoJUnitRunner.class)
public class CrearClienteCobisMapperTest {
	
	@InjectMocks
	private CrearClienteCobisMapper sut; 
	
	private BCRequest<InformacionClienteDTO> validLoadRequest(){
		BCRequest<InformacionClienteDTO> bcRequest = new BCRequest<>();
		bcRequest.setHeader(getHeader());
		bcRequest.setBody(getBodyResponse());
        return bcRequest;
	}
	
	/*
	 *  Testing Mapper to Request  
	 * 
	 */
	
	@Test
	public void buildHandlerRequestTOAdapterRequest(){
		BCRequest<InformacionClienteDTO> bcRequest = validLoadRequest();
		sut.handlerRequestTOAdapterRequestDTO(bcRequest);
	}

	@Test
	public void validateOutputBCRequest(){
		BCRequest<InformacionClienteDTO> bcRequest = validLoadRequest();
		BCRequest<ClienteCobisDTO> request = sut.handlerRequestTOAdapterRequestDTO(bcRequest);
		assertThat(request.getHeader().getCanal(), equalTo("1"));
	}
	
	@Test
	public void validateOutputDateBCRequest(){
		BCRequest<InformacionClienteDTO> bcRequest = validLoadRequest();
		BCRequest<ClienteCobisDTO> request = sut.handlerRequestTOAdapterRequestDTO(bcRequest);
		assertThat(request.getBody().getFechaNacimiento(), instanceOf(Date.class));
	}
	
	/* BEGIN: Validating all cases for missing parameters */
	private BCResponse<EmptyBodyT> validLoadResponse(){
		BCResponse<EmptyBodyT> bcResponse = new BCResponse<>();
		bcResponse.setStatus(getStatus());
		bcResponse.setHeader(getHeader());
        return bcResponse;
    }
	
	private InformacionClienteDTO getBodyResponse() {
		InformacionClienteDTO body = new InformacionClienteDTO();
		body.setPrimerNombre("Naruto");
		body.setPrimerApellido("Uzumaki");
		body.setCedula("08  1061000250");
	
		body.setFechaNacimiento(new Date());
		Catalogo catalogo = new Catalogo();
		
		catalogo.setCodigo("M");
		body.setSexo(catalogo);

		catalogo.setCodigo("S");
		body.setEstadoCivil(catalogo);
		
		catalogo.setCodigo("61");		
		body.setPaisNacimiento(catalogo);

		catalogo.setCodigo("61");
		body.setPaisNacionalidad(catalogo);
		
		catalogo.setCodigo("61");
		body.setPaisRiesgo(catalogo);
		
		return body;
	}
	

	
	private Header getHeader(){
		Header headerRest = new Header();
		// Cabecera
		headerRest.setNumeroUnico(1);
		headerRest.setOficina(1);
		headerRest.setTerminal("1");
		headerRest.setUsuario("bccanales");
		headerRest.setRolUsuario(3);
		// CabeceraCanales
		headerRest.setAplicacion("1");
		headerRest.setCanal("1");
		headerRest.setFecha(new Date());
		headerRest.setNodo(1);
		headerRest.setSesionBg("1234");
		headerRest.setDireccionIp("");
		headerRest.setOrigenSolicitud("bel");
		headerRest.setNumeroReferencia("1234");
		headerRest.setHoraInicio("12:00:00");
		
		// Perfil
		Perfil perfil = new Perfil();
		perfil.setLogin("");
		perfil.setNumeroCliente(12);
		perfil.setNumeroBancaVirtual(0);
		perfil.setAlias("prueba");
		perfil.setNumeroPerfil(3);

		headerRest.setPerfil(perfil);
		return headerRest;
	}
	
	private Status getStatus() {
		Status status = new Status();
		ReturnStatus returnStatus = new ReturnStatus();
		returnStatus.setReturnCode(CodeCatalog.CODE_SUCCESS.codeS());
		returnStatus.setReturnCodeDesc(CodeCatalog.S_MSG_CODE_SUCCESS.codeS());
		status.setReturnStatus(returnStatus);
		return status;
	}
}
