package com.bgeneral.ce.bc.clientecobis.adapter;

import static org.hamcrest.CoreMatchers.instanceOf;
import static org.mockito.Mockito.when;

import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.MockitoJUnitRunner;
import org.springframework.boot.test.context.SpringBootTest;

import com.bgeneral.ce.bc.clientecobis.adapter.dto.CrearCorreoClienteCobisRespDTO;
import com.bgeneral.ce.bc.clientecobis.adapter.dto.CrearCorreoClienteCobisRqDTO;
import com.bgeneral.ce.bc.clientecobis.adapter.mapper.IAmMapperJsonDTO;
import com.bgeneral.ce.bc.clientecobis.common.dto.BCRequest;
import com.bgeneral.ce.bc.clientecobis.common.dto.BCResponse;
import com.bgeneral.ce.bc.clientecobis.common.dto.ReturnStatus;
import com.bgeneral.ce.bc.clientecobis.common.dto.Status;
import com.bgeneral.ce.bc.clientecobis.handler.dto.CuentaCorreaClienteDTO;
import com.bgeneral.ce.bc.clientecobis.handler.dto.ValorCorreoDTO;
import com.bgeneral.ce.bc.clientecobis.util.MockDummyRequest;
import com.bgeneral.ce.bc.clientecobis.util.MockServer;

@SpringBootTest
@RunWith(MockitoJUnitRunner.class)
public class CrearCorreoClienteCobisAdapterTest extends MockServer{

	@Mock
	private IAmMapperJsonDTO<CuentaCorreaClienteDTO, ValorCorreoDTO,CrearCorreoClienteCobisRqDTO,CrearCorreoClienteCobisRespDTO> mapperDTO;

	@InjectMocks
	private CrearCorreoClienteCobisAdapter sut;
	
	@Test
	public void shouldConnectToAdapter() throws Exception {
		String jsonString = "{\"header\":{\"numeroUnico\":123456,\"oficina\":62,\"terminal\":\"junitest\",\"usuario\":\"junitest\",\"rolUsuario\":1,\"aplicacion\":\"1\",\"canal\":\"1\",\"fecha\":\"2018-10-22T21:12:52.655Z\",\"nodo\":1,\"sesionBg\":\"\",\"direccionIp\":\"1.9.7.6\",\"origenSolicitud\":\"BEL\",\"numeroReferencia\":\"\",\"horaInicio\":\"\",\"perfil\":{\"login\":\"\",\"numeroCliente\":229469,\"numeroBancaVirtual\":0,\"alias\":\"yonofui\",\"numeroPerfil\":3,\"numeroClienteUsuario\":1,\"vistaAsociada\":1}},\"body\":{\"numeroBancaVirtual\":12345,\"login\":\"54321\"},\"status\":{\"returnStatus\":{\"returnCode\":\"U0000\",\"returnCodeDesc\":\"SeharealizadolaejecuciÃ³ndelserviciocorrectamente.\"}}}";
		setUpMockResponse(jsonString);
		modifyPropertyHost();
		when(mapperDTO.handlerRequestTOAdapterRequestDTO(Mockito.any())).thenReturn(CorreoClienteCobisRqDTO());
		

		when(mapperDTO.adapterResponseTOHandlerResponseDTO(Mockito.any())).thenReturn(CrearCorreoClienteRespDTO());
		
		BCResponse<ValorCorreoDTO> bcResponse = sut.callRestService(getRequestInServices());
		Assert.assertNotNull(bcResponse);
		System.out.println(bcResponse.getStatus().getReturnStatus().getReturnCode());
		Assert.assertThat(bcResponse.getStatus().getReturnStatus().getReturnCode(), instanceOf(String.class));
		Assert.assertNotNull(bcResponse.getBody().getCodigoCorreo());		
	}
	
	@Test(expected = IllegalArgumentException.class)
	public void shouldValidateServicesWithResponseReturnStatusNull() throws Exception {
		String jsonString = "{\"header\":{\"numeroUnico\":123456,\"oficina\":62,\"terminal\":\"junitest\",\"usuario\":\"junitest\",\"rolUsuario\":1,\"aplicacion\":\"1\",\"canal\":\"1\",\"fecha\":\"2018-10-22T21:12:52.655Z\",\"nodo\":1,\"sesionBg\":\"\",\"direccionIp\":\"1.9.7.6\",\"origenSolicitud\":\"BEL\",\"numeroReferencia\":\"\",\"horaInicio\":\"\",\"perfil\":{\"login\":\"\",\"numeroCliente\":229469,\"numeroBancaVirtual\":0,\"alias\":\"yonofui\",\"numeroPerfil\":3,\"numeroClienteUsuario\":1,\"vistaAsociada\":1}},\"status\":{\"returnStatus\":{\"returnCode\":null,\"returnCodeDesc\":\"SeharealizadolaejecuciÃ³ndelserviciocorrectamente.\"}}}";
		setUpMockResponse(jsonString);
		modifyPropertyHost();
		when(mapperDTO.handlerRequestTOAdapterRequestDTO(Mockito.any())).thenReturn(new BCRequest <CrearCorreoClienteCobisRqDTO>());
		when(mapperDTO.adapterResponseTOHandlerResponseDTO(Mockito.any())).thenReturn(new BCResponse<ValorCorreoDTO>());
		
		
		BCResponse<ValorCorreoDTO> bcResponse = sut.callRestService(getRequestInServices());
	}
	
	@Test(expected = IllegalArgumentException.class)
	public void shouldValidateServicesWithRequestNull() throws Exception {
		String jsonString = "{\"header\":{\"numeroUnico\":123456,\"oficina\":62,\"terminal\":\"junitest\",\"usuario\":\"junitest\",\"rolUsuario\":1,\"aplicacion\":\"1\",\"canal\":\"1\",\"fecha\":\"2018-10-22T21:12:52.655Z\",\"nodo\":1,\"sesionBg\":\"\",\"direccionIp\":\"1.9.7.6\",\"origenSolicitud\":\"BEL\",\"numeroReferencia\":\"\",\"horaInicio\":\"\",\"perfil\":{\"login\":\"\",\"numeroCliente\":229469,\"numeroBancaVirtual\":0,\"alias\":\"yonofui\",\"numeroPerfil\":3,\"numeroClienteUsuario\":1,\"vistaAsociada\":1}},\"status\":{\"returnStatus\":{\"returnCode\":null,\"returnCodeDesc\":\"SeharealizadolaejecuciÃ³ndelserviciocorrectamente.\"}}}";
		setUpMockResponse(jsonString);
		modifyPropertyHost();
		BCResponse<ValorCorreoDTO> bcResponse = sut.callRestService(null);
	}

	private BCRequest<CrearCorreoClienteCobisRqDTO> CorreoClienteCobisRqDTO() {
		CrearCorreoClienteCobisRqDTO crearCorreoDTO = new CrearCorreoClienteCobisRqDTO();
		crearCorreoDTO.setDireccion("andrea@bgeneral.com");
		crearCorreoDTO.setTipoMail("M");
		crearCorreoDTO.setTipoUso("P");
		return new MockDummyRequest<CrearCorreoClienteCobisRqDTO>().setBody(crearCorreoDTO).build();
	}
	
	private BCResponse<ValorCorreoDTO> CrearCorreoClienteRespDTO() {
		BCResponse<ValorCorreoDTO> test = new BCResponse<ValorCorreoDTO>();
		test.setStatus(new Status());
		test.getStatus().setReturnStatus(new ReturnStatus());
		test.getStatus().getReturnStatus().setReturnCode("U0000");
		test.setBody(new ValorCorreoDTO());
		test.getBody().setCodigoCorreo(1);
		return test;
	}
	private BCRequest<CuentaCorreaClienteDTO> ClienteCobisRqDTO() {
		CuentaCorreaClienteDTO crearCorreoDTO = new CuentaCorreaClienteDTO();
		crearCorreoDTO.setDireccion("andrea@bgeneral.com");
		crearCorreoDTO.setTipoMail("M");
		crearCorreoDTO.setTipoUso("P");
		return new MockDummyRequest<CuentaCorreaClienteDTO>().setBody(crearCorreoDTO).build();
	}
	
	private BCRequest<CuentaCorreaClienteDTO> getRequestInServices() {
		CuentaCorreaClienteDTO body = new CuentaCorreaClienteDTO();
		return new MockDummyRequest<CuentaCorreaClienteDTO>().setBody(body).build();
	}
}
