package com.bgeneral.ce.bc.clientecobis.adapter.mapper;

import static org.hamcrest.CoreMatchers.equalTo;
import static org.hamcrest.CoreMatchers.instanceOf;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertThat;

import java.util.Date;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.junit.MockitoJUnitRunner;

import com.bgeneral.ce.bc.clientecobis.adapter.dto.ReferenciaClienteAdapterDTO;
import com.bgeneral.ce.bc.clientecobis.adapter.mapper.impl.ConsultarReferenciaClienteMapper;
import com.bgeneral.ce.bc.clientecobis.common.dto.BCRequest;
import com.bgeneral.ce.bc.clientecobis.common.dto.BCResponse;
import com.bgeneral.ce.bc.clientecobis.common.dto.EmptyBodyT;
import com.bgeneral.ce.bc.clientecobis.common.dto.Header;
import com.bgeneral.ce.bc.clientecobis.common.dto.Perfil;
import com.bgeneral.ce.bc.clientecobis.common.dto.ReturnStatus;
import com.bgeneral.ce.bc.clientecobis.common.dto.Status;
import com.bgeneral.ce.bc.clientecobis.common.util.catalogs.CodeCatalog;
import com.bgeneral.ce.bc.clientecobis.handler.dto.ReferenciaClienteDTO;

/**
 * 
 * @author jolivero
 *
 */

@RunWith(MockitoJUnitRunner.class)
public class ConsultarReferenciaClienteMapperTest {
	
	@InjectMocks
	private ConsultarReferenciaClienteMapper sut;
	
	private BCRequest<EmptyBodyT> validLoadRequest() {
		BCRequest<EmptyBodyT> bcRequest = new BCRequest<>();
		bcRequest.setHeader(getHeader());
		return bcRequest;
	}
	
	/*
	 *  Testing Mapper to Request  
	 * 
	 */
	
	@Test
	public void buildHandlerRequestTOAdapterRequest(){
		BCRequest<EmptyBodyT> bcRequest = validLoadRequest();
		sut.handlerRequestTOAdapterRequestDTO(bcRequest);
	}
	
	
	/*
	 * Testing Mapper to Response
	 * 
	 */
	@Test
	public void buildHandlerRequestTOAdapterResponse() {
		BCResponse<ReferenciaClienteAdapterDTO> bcResponse = validLoadResponse();
		sut.adapterResponseTOHandlerResponseDTO(bcResponse);
	}
	
	@Test(expected = NullPointerException.class)
	public void validateStatusResponseNull() {
		BCResponse<ReferenciaClienteAdapterDTO> bcResponse = validLoadResponse();
		bcResponse.getStatus().setReturnStatus(null);
		sut.adapterResponseTOHandlerResponseDTO(bcResponse);
	}
	
	@Test
	public void validateValueInBodyResponse() {
		BCResponse<ReferenciaClienteAdapterDTO> bcResponse = validLoadResponse();
		BCResponse<ReferenciaClienteDTO> response = sut.adapterResponseTOHandlerResponseDTO(bcResponse);

		assertThat(response.getBody().isTieneReferencia(), instanceOf(Boolean.class));
		assertEquals(response.getBody().isTieneReferencia(), true);
	}
	
	private BCResponse<ReferenciaClienteAdapterDTO> validLoadResponse(){
		BCResponse<ReferenciaClienteAdapterDTO> bcResponse = new BCResponse<>();
		bcResponse.setHeader(this.getHeader());
		bcResponse.setStatus(this.getStatus());
		bcResponse.setBody(this.getBody());
        return bcResponse;
    }
	
	@Test
	public void validateValueInNullBodyResponse() {
		BCResponse<ReferenciaClienteAdapterDTO> bcResponse = validLoadResponse();
		bcResponse.getStatus().getReturnStatus().setReturnCode("U0002");
		bcResponse.getStatus().getReturnStatus().setReturnCodeDesc("FALLO");
		BCResponse<ReferenciaClienteDTO> response = sut.adapterResponseTOHandlerResponseDTO(bcResponse);

		assertThat(response.getBody(), equalTo(null));
	}

	private ReferenciaClienteAdapterDTO getBody() {
		ReferenciaClienteAdapterDTO body = new ReferenciaClienteAdapterDTO();
		body.setNumeroRegistros(1);
		return body;
	}
	
	private Header getHeader(){
		Header headerRest = new Header();
		// Cabecera
		headerRest.setNumeroUnico(1);
		headerRest.setOficina(1);
		headerRest.setTerminal("1");
		headerRest.setUsuario("bccanales");
		headerRest.setRolUsuario(3);
		// CabeceraCanales
		headerRest.setAplicacion("1");
		headerRest.setCanal("1");
		headerRest.setFecha(new Date());
		headerRest.setNodo(1);
		headerRest.setSesionBg("1234");
		headerRest.setDireccionIp("");
		headerRest.setOrigenSolicitud("bel");
		headerRest.setNumeroReferencia("1234");
		headerRest.setHoraInicio("12:00:00");
		
		// Perfil
		Perfil perfil = new Perfil();
		perfil.setLogin("");
		perfil.setNumeroCliente(12);
		perfil.setNumeroBancaVirtual(0);
		perfil.setAlias("prueba");
		perfil.setNumeroPerfil(3);

		headerRest.setPerfil(perfil);
		return headerRest;
	}
	
	private Status getStatus() {
		Status status = new Status();
		ReturnStatus returnStatus = new ReturnStatus();
		returnStatus.setReturnCode(CodeCatalog.CODE_SUCCESS.codeS());
		returnStatus.setReturnCodeDesc(CodeCatalog.S_MSG_CODE_SUCCESS.codeS());
		status.setReturnStatus(returnStatus);
		return status;
	}
}
