package com.bgeneral.ce.bc.clientecobis.util;

import java.io.Serializable;
import java.util.Date;
import java.util.GregorianCalendar;

import javax.xml.datatype.DatatypeConfigurationException;
import javax.xml.datatype.DatatypeFactory;
import javax.xml.datatype.XMLGregorianCalendar;

import com.bgeneral.ce.bc.clientecobis.common.dto.BCResponse;
import com.bgeneral.ce.bc.clientecobis.common.dto.Header;
import com.bgeneral.ce.bc.clientecobis.common.dto.Perfil;
import com.bgeneral.ce.bc.clientecobis.common.dto.ReturnStatus;
import com.bgeneral.ce.bc.clientecobis.common.dto.Status;


public class MockDummyResponse<TBody extends Serializable> implements Serializable {
	
	private Perfil perfil;
	private Header header;
	private TBody body;
	private ReturnStatus returnStatus;
	private BCResponse<TBody> bcResponse;
	
	public MockDummyResponse<TBody> setReturnStatus(ReturnStatus returnStatus){
		this.returnStatus = returnStatus;
		return this;
	}

	public MockDummyResponse<TBody> setPerfil(Perfil perfil){
		this.perfil = perfil;
		return this;
	}
	
	public MockDummyResponse<TBody> setHeader(Header header){
		this.header = header;
		return this;
	}
	
	public MockDummyResponse<TBody> setBody(TBody body){
		this.body = body;
		return this;
	}
	
	public BCResponse<TBody> build(){
		this.bcResponse = new BCResponse<>();
		if(this.header==null) {
			this.getHeader();
		}
		
		this.bcResponse.setHeader(this.header);
		this.bcResponse.setStatus(new Status());
		this.bcResponse.getStatus().setReturnStatus(returnStatus);
		this.bcResponse.setBody(this.body);
		return this.bcResponse;
	}
	

	private void getHeader(){
		try {
			
			this.header = new Header();
			GregorianCalendar c = new GregorianCalendar();
			c.setTime(new Date());
			XMLGregorianCalendar date2 = DatatypeFactory.newInstance().newXMLGregorianCalendar(c);
			
			// Cabecera
			this.header.setNumeroUnico(1);
			this.header.setOficina(1);
			this.header.setTerminal("1");
			this.header.setUsuario("twen");
			this.header.setRolUsuario(3);
			
			// CabeceraCanales
			this.header.setAplicacion("1");
			this.header.setCanal("1");
			this.header.setFecha(date2.toGregorianCalendar().getTime());
			this.header.setNodo(1);
			this.header.setSesionBg("D16845BA18F38001A67FCB01FF97A0BC939AEACFF2C3BB9D5FD5D5830A81625C");
			this.header.setDireccionIp("10.90.5.106");
			this.header.setOrigenSolicitud("BEL");
			this.header.setHoraInicio(date2.toString());
			
			// Perfil
			if(this.perfil!=null) {
				this.header.setPerfil(this.perfil);
			}else {
				Perfil perfil = new Perfil();
				perfil.setLogin("43127");
				perfil.setNumeroCliente(43127);
				perfil.setNumeroBancaVirtual(24459);
				perfil.setAlias("yonofui");
				perfil.setNumeroPerfil(3);
				this.header.setPerfil(perfil);
			}

			
		} catch (DatatypeConfigurationException e) {
			e.printStackTrace();
		}
	}	

}
