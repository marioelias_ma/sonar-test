FROM registry.bgeneral.com:48082/openjdk:8-jre-alpine
RUN apk add --update --no-cache tzdata
ADD /target/*.jar /opt/api/servicios_rest/app.jar
RUN echo 'java -jar -Djava.security.egd=file:/dev/./urandom -Djavax.net.ssl.trustStore=${trustoreFile} -Djavax.net.ssl.trustStorePassword=${trustorePwd} -Djavax.net.ssl.keyStore=${keystoreFile} -Djavax.net.ssl.keyStorePassword=${keystorePwd} /opt/api/servicios_rest/app.jar' > /runapp.sh && \
    chmod +x /runapp.sh
VOLUME ["/tmp", "/opt/api/servicios_rest/config", "/opt/api/servicios_rest/log"]
ENTRYPOINT ["/bin/sh", "/runapp.sh"]
